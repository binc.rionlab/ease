		<!--==============================footer=================================-->
    <hr class="hr-footer">
    <div id="footer">
      <section id="footer_ui">
        <ul>
			<script>
			console.log('test-single')
			</script>
<?php
$next = get_adjacent_post(false, '', false); //$nextに次の記事を代入
$prev = get_adjacent_post(false, '', true); //$prevに前の記事を代入
?>
<?php if (get_previous_post()):?>
          <li><a class="page-link" href="<?php echo get_permalink($prev->ID) //前の記事URL ?>"><img class="footer-button" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/fig/inertia-fig-prev.png" alt="backbutton"></a></li>
<?php endif; ?>
          <li><a class="page-link" href="#content-container"><img class="footer-button" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/fig/inertia-fig-top.png" alt="backbutton"></a></li>
			
<?php if (get_next_post()):?>
          <li><a class="page-link" href="<?php echo get_permalink($next->ID) //次の記事URL ?>"><img class="footer-button" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/fig/inertia-fig-next.png" alt="backbutton"></a></li>
<?php endif; ?>
        </ul>
      </section>
    </div>
  <section id="footer_copyright">
    <div style="text-align: center; margin-bottom: 10px; padding-top: 5px;"><a href="http://inertiaart.io/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>//assets/img/INERTIA_LOGO.svg" style="height: 15px; margin: 0 10px;"/><a href="https://www.facebook.com/easel-103182437986338/" target="_blank"><i class="fab fa-facebook-square" style="height: 15px; margin: 0 2px;"></i></a><a href="https://twitter.com/easel_inertia" target="_blank"><i class="fab fa-twitter-square" style="height: 15px; margin: 0 2px;"></i></a><a href="https://www.instagram.com/easel.inertia/" target="_blank"><i class="fab fa-instagram" style="height: 15px; margin: 0 2px;"></i></a></a>
    </div>
    <div style="text-align: center"><a href="<?php echo esc_url( home_url( '/' ) ); ?>privacy"><span class="footer--text footer--text__small">隐私权政策</span></a><span style="margin: 3px;"></span><a href="<?php echo esc_url( home_url( '/' ) ); ?>faq"><span class="footer--text footer--text__small">常见问题</span></a><span style="margin: 3px;"></span><a href="<?php echo esc_url( home_url( '/' ) ); ?>transaction"><span class="footer--text footer--text__small">基于特定商户交易法的公示信息</span></a></div><div style="text-align: center"><a href="<?php echo esc_url( home_url( '/' ) ); ?>trouble-shooting"><span class="footer--text footer--text__small">故障排除</span></a><span style="margin: 3px;"></span><span style="margin: 3px;"></span><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact"><span class="footer--text footer--text__small">问询</span></a></div>
    <p class="footer--text footer--text__small">&copy; <?php echo date('Y'); ?> 株式会社INERTIA All Rights Reserved.</p>
  </section>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
  <script>
    Pace.on('done',function() {
      var sketch, playButton;
      $('#content-container').fadeIn();
      $('#footer_copyright').fadeIn();

    });
    

  </script>
			<?php else : ?>
  <script>
    Pace.on('done',function() {
      var sketch, playButton;
      $('#content-container').fadeIn();
      $('#footer_copyright').fadeIn();
      $('#scrolldown').fadeIn();
      sketch = document.getElementsByClassName("demo--sketch");
      playButton = sketch[0].contentWindow.document.getElementById('playIcon');
      playButton.click();$('.modal').modal();
      setTimeout(function() {
        $('#scrolldown').fadeOut();
        }, 5000);
    });
    
    function showInfo() {$('#showPlanInfo').modal('open');return false;}
  </script>
			<?php endif; ?>
<?php else : ?>
  <script>
    Pace.on('done',function() {
      var sketch, playButton;
      $('#content-container').fadeIn();
      $('#footer_copyright').fadeIn();

    });
    

  </script>
<?php endif; ?>
 
<?php wp_footer(); ?>
</body>
</html>
