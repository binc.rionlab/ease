<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */


get_header('single'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>
<a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;"></a>
<section class="section--scrolldown" id="scrolldown" style="display: none;"><a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;"><span></span>スクロールしてください</a></section>
<div id="content-container" style="display: block;">
<section id="header-title" style="margin-bottom: 0px;">
<h2 id="header_title">SketchBook</h2>
</section>
<section class="swpm-pw-reset-widget-form">
    <div class="sketch_new"><a href="<?php echo esc_url( home_url( '/' ) ); ?>wp-admin/post-new.php?post_type=sketchbook"><span class="new_icon"></span>新的创作</a></div>
    <div class="sketch_edit"><a href="<?php echo esc_url( home_url( '/' ) ); ?>wp-admin/edit.php?post_type=sketchbook"><span class="edit_icon"></span>修改作品</a></div>
</section>
</div>


<?php get_footer(); ?>