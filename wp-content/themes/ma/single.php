<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header('single'); ?>
<?php get_template_part('inc/category-nav'); ?>

 <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>

		<!--==============================content=================================-->

				<?php while ( have_posts() ) : the_post(); ?>

				<?php 
the_content(); 
?>

			<?php endwhile; // end of the loop. ?>


 <?php 
get_footer('single'); 
// get_footer(); 
?>
