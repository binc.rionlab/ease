<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header('sec'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>
  <section id="list-container">
    <ul id="list-ul01">
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section01/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image01.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Start</h2>
              <h3 class="list-jpTitle">はじめよう</h3>
              <hr class="center-line">
              <p class="progress-no">01</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section01/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image01.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Start</h2>
              <h3 class="list-jpTitle">はじめよう</h3>
              <hr class="center-line">
              <p class="progress-no">01</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section01/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image01.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Start</h2>
              <h3 class="list-jpTitle">はじめよう</h3>
              <hr class="center-line">
              <p class="progress-no">01</p>
              
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
<?php endif; ?>


<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section02/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image02.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Form</h2>
              <h3 class="list-jpTitle">図形</h3>
              <hr class="center-line">
              <p class="progress-no">02-09</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section02/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image02.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Form</h2>
              <h3 class="list-jpTitle">図形</h3>
              <hr class="center-line">
              <p class="progress-no">02-09</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section02/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image02.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Form</h2>
              <h3 class="list-jpTitle">図形</h3>
              <hr class="center-line">
              <p class="progress-no">02-09</p>
              
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
<?php endif; ?>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section03/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image03.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Color</h2>
              <h3 class="list-jpTitle">色</h3>
              <hr class="center-line">
              <p class="progress-no">10-21</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section03/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image03.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Color</h2>
              <h3 class="list-jpTitle">色</h3>
              <hr class="center-line">
              <p class="progress-no">10-21</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section03/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image03.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Color</h2>
              <h3 class="list-jpTitle">色</h3>
              <hr class="center-line">
              <p class="progress-no">10-21</p>
              
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
<?php endif; ?>

<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section04/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image04.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Variable</h2>
              <h3 class="list-jpTitle">変数</h3>
              <hr class="center-line">
              <p class="progress-no">22-24</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section04/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image04.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Variable</h2>
              <h3 class="list-jpTitle">変数</h3>
              <hr class="center-line">
              <p class="progress-no">22-24</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section04/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image04.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Variable</h2>
              <h3 class="list-jpTitle">変数</h3>
              <hr class="center-line">
              <p class="progress-no">22-24</p>
              
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
<?php endif; ?>

<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section05/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image05.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Math</h2>
              <h3 class="list-jpTitle">算数</h3>
              <hr class="center-line">
              <p class="progress-no">25-28</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section05/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image05.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Math</h2>
              <h3 class="list-jpTitle">算数</h3>
              <hr class="center-line">
              <p class="progress-no">25-28</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section05/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image05.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Math</h2>
              <h3 class="list-jpTitle">算数</h3>
              <hr class="center-line">
              <p class="progress-no">25-28</p>
              
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
<?php endif; ?>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section06/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image06.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Animation</h2>
              <h3 class="list-jpTitle">アニメーション</h3>
              <hr class="center-line">
              <p class="progress-no">29-31</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section06/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image06.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Animation</h2>
              <h3 class="list-jpTitle">アニメーション</h3>
              <hr class="center-line">
              <p class="progress-no">29-31</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image06.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Animation</h2>
              <h3 class="list-jpTitle">アニメーション</h3>
              <hr class="center-line">
              <p class="progress-no">29-31</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>



<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section07/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image07.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Condition</h2>
              <h3 class="list-jpTitle">条件文</h3>
              <hr class="center-line">
              <p class="progress-no">32-38</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section07/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image07.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Condition</h2>
              <h3 class="list-jpTitle">条件文</h3>
              <hr class="center-line">
              <p class="progress-no">32-38</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image07.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Condition</h2>
              <h3 class="list-jpTitle">条件文</h3>
              <hr class="center-line">
              <p class="progress-no">32-38</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>

<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section08/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image08.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Repeat</h2>
              <h3 class="list-jpTitle">繰り返し</h3>
              <hr class="center-line">
              <p class="progress-no">39-41</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section08/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image08.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Repeat</h2>
              <h3 class="list-jpTitle">繰り返し</h3>
              <hr class="center-line">
              <p class="progress-no">39-41</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image08.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Repeat</h2>
              <h3 class="list-jpTitle">繰り返し</h3>
              <hr class="center-line">
              <p class="progress-no">39-41</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section09/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image09.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Array</h2>
              <h3 class="list-jpTitle">配列</h3>
              <hr class="center-line">
              <p class="progress-no">42-44</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section09/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image09.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Array</h2>
              <h3 class="list-jpTitle">配列</h3>
              <hr class="center-line">
              <p class="progress-no">42-44</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image09.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Array</h2>
              <h3 class="list-jpTitle">配列</h3>
              <hr class="center-line">
              <p class="progress-no">42-44</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>

<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section10/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image10.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Function</h2>
              <h3 class="list-jpTitle">関数</h3>
              <hr class="center-line">
              <p class="progress-no">45-49</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section10/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image10.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Function</h2>
              <h3 class="list-jpTitle">関数</h3>
              <hr class="center-line">
              <p class="progress-no">45-49</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image10.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Function</h2>
              <h3 class="list-jpTitle">関数</h3>
              <hr class="center-line">
              <p class="progress-no">45-49</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section11/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image11.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Text</h2>
              <h3 class="list-jpTitle">テキスト</h3>
              <hr class="center-line">
              <p class="progress-no">50-54</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section11/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image11.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Text</h2>
              <h3 class="list-jpTitle">テキスト</h3>
              <hr class="center-line">
              <p class="progress-no">50-54</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image11.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Text</h2>
              <h3 class="list-jpTitle">テキスト</h3>
              <hr class="center-line">
              <p class="progress-no">50-54</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section12/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image12.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Image</h2>
              <h3 class="list-jpTitle">イメージ</h3>
              <hr class="center-line">
              <p class="progress-no">55-58</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section12/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image12.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Image</h2>
              <h3 class="list-jpTitle">イメージ</h3>
              <hr class="center-line">
              <p class="progress-no">55-58</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image12.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Image</h2>
              <h3 class="list-jpTitle">イメージ</h3>
              <hr class="center-line">
              <p class="progress-no">55-58</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section13/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image13.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">GUI</h2>
              <h3 class="list-jpTitle">インタフェース</h3>
              <hr class="center-line">
              <p class="progress-no">59-60</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section13/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image13.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">GUI</h2>
              <h3 class="list-jpTitle">インタフェース</h3>
              <hr class="center-line">
              <p class="progress-no">59-60</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image13.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">GUI</h2>
              <h3 class="list-jpTitle">インタフェース</h3>
              <hr class="center-line">
              <p class="progress-no">59-60</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section14/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image14.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Sound</h2>
              <h3 class="list-jpTitle">サウンド</h3>
              <hr class="center-line">
              <p class="progress-no">61-63</p>
            </div>
              <div class="list-ribbon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg"></div>
          </div>
        </li>
      </a>
			<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/section14/">
        <li>
          <div id="section01">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image14.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Sound</h2>
              <h3 class="list-jpTitle">サウンド</h3>
              <hr class="center-line">
              <p class="progress-no">61-63</p>
            </div>
          </div>
        </li>
      </a>
			<?php endif; ?>
<?php else : ?>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
        <li>
          <div id="section01" style="background: rgba(0,0,0,.6); color: #C6C6C6;">
            <div class="list-fig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image14.gif"></div>
            <div class="list-blocks">
              <h2 class="list-enTitle">Sound</h2>
              <h3 class="list-jpTitle">サウンド</h3>
              <hr class="center-line">
              <p class="progress-no">61-63</p>
            </div>
          </div>
        </li>
        </a>
<?php endif; ?>

    </ul>
  </section>
  <section id="list-underBlock"></section>
  <section id="footer-menu">
    <ul id="navi-ul">
      <li class="navi-list navi-active" id="">
        <div class="navi-blocks">
          <p class="navi-jpTitle">学习</p>
          <p class="navi-enTitle">Entry</p>
        </div>
      </li>
      <li class="navi-list" id="">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/">
          <div class="navi-blocks">
            <p class="navi-jpTitle">模仿</p>
            <p class="navi-enTitle">Make</p>
          </div>
        </a>
      </li>
    </ul>
  </section>
  <script>
    Pace.on('done',function(){$('#list-container').fadeIn();$('#box-container').fadeIn();$('#footer_copyright').fadeIn();$('.modal').modal();});
    function showInfo() {$('#showPlanInfo').modal('open');return false;}
  </script>
<?php get_footer(); ?>
