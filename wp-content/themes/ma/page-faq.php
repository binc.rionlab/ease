<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */


get_header('faq'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){
		caches.keys().then(function(cacheNames) {
    cacheNames.forEach(function(cacheName) {
      caches.delete(cacheName);
    });
  });
		$('.sidenav').sidenav();});
  </script>
<a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;"></a>

<section id="header-title" style="margin-bottom: 0px;">
  <h2 id="header_title">常见问题</h2>
</section>
<div style="height: 20px;"></div>
<section class="content_blocks">
  <p class="content_text">easel APについて</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group = SCF::get( 'faq_wrap', 581 );
    foreach ( $repeat_group as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>


<section class="content_blocks">
  <p class="content_text">問い合わせ・メールについて</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group2122 = SCF::get( 'faq_wrap', 2122 );
    foreach ( $repeat_group2122 as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>

<section class="content_blocks">
  <p class="content_text">有料プランについて</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group2126 = SCF::get( 'faq_wrap', 2126 );
    foreach ( $repeat_group2126 as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>

<section class="content_blocks">
  <p class="content_text">決済・クーポンの利用について</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group2129 = SCF::get( 'faq_wrap', 2129 );
    foreach ( $repeat_group2129 as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>

<section class="content_blocks">
  <p class="content_text">返金・解約について</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group2132 = SCF::get( 'faq_wrap', 2132 );
    foreach ( $repeat_group2132 as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>

<section class="content_blocks">
  <p class="content_text">ID、パスワードについて</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group2136 = SCF::get( 'faq_wrap', 2136 );
    foreach ( $repeat_group2136 as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>

<section class="content_blocks">
  <p class="content_text">動作環境について</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group2139 = SCF::get( 'faq_wrap', 2139 );
    foreach ( $repeat_group2139 as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>


<section class="content_blocks">
  <p class="content_text">Sketchbookの操作について</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group2143 = SCF::get( 'faq_wrap', 2143 );
    foreach ( $repeat_group2143 as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>


<section class="content_blocks">
  <p class="content_text">その他</p>
</section>
<section class="content_blocks">

  <dl class="qa">
<?php
    //グループ名を設定しループスタート
    $repeat_group2147 = SCF::get( 'faq_wrap', 2147 );
    foreach ( $repeat_group2147 as $fields ) { ?>
	  <dt>Q,<?php echo nl2br( $fields['faq_q'] ); ?></dt>
	  <dd>A,<?php echo nl2br( $fields['faq_a'] ); ?></dd>
<?php } ?>
  </dl>
</section>



<?php get_footer(); ?>
