<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<link rel="shortcut icon" href="">
	<link rel="apple-touch-icon-precomposed" href="">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/asset/css/common.css">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/asset/css/animate.css">

	<link rel="stylesheet" media="screen and (max-width:480px)" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/asset/css/smart.css" />
	<link rel="stylesheet" media="screen and (min-width:481px) and (max-width: 1024px)" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/asset/css/tablet.css" /> 
    <link rel="stylesheet" media="screen and (min-width: 1025px)" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/asset/css/style.css" />
	
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
	
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<meta property="og:title" content="<?php wp_title(); ?>"/>
<meta property="og:type" content="article"/>
<?php if(has_post_thumbnail()) { ?>
<meta property="og:image" content="<?php get_featured_image_url(); ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php wp_title(); ?>"/>

<title><?php wp_title( '|', true, 'right' ); ?></title>

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/image/favicon.ico">

<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/respond.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/selectivizr-min.js" type="text/javascript"></script>
<![endif]-->

<?php wp_head(); ?>

	
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/asset/js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/asset/js/tab.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/asset/js/movie.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/asset/js/jquery.inview.min.js"></script>
	<script>
		$(function () {
			var $body = $('body');
			//開閉用ボタンをクリックでクラスの切替え
			$('#js__btn').on('click', function () {
				$body.toggleClass('open');
			});

			//メニュー名以外の部分をクリックで閉じる
			$('#js__nav').on('click', function () {
				$body.removeClass('open');
			});
		});
		$(function() {
		    var topMenu = $('.fix-menu');    
		    topMenu.hide();
		    $(this).scroll(function () {
		        if ($(this).scrollTop() > 350) {
		            topMenu.fadeIn();
		        } else {
		            topMenu.fadeOut();
		        }
		    });
		});
		$(function(){
			$('a[href^="#"]').click(function() {
		      var speed = 400;
		      var href= $(this).attr("href");
		      var target = $(href == "#" || href == "" ? 'html' : href);
		      var position = target.offset().top;
		      $('body,html').animate({scrollTop:position}, speed, 'swing');
		      return false;
		   });
		});
	</script>
</head>
<body>
	<div id="wrapper">
        <span class="bg_line1"></span>
        <span class="bg_line2"></span>
        <span class="bg_line3"></span>
        <span class="bg_line4"></span>
        <span class="bg_line5"></span>
		<!--==============================header=================================-->

				
