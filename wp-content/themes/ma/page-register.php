<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */


get_header('page'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>
<a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;"></a>

<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>

<section id="header-title" style="margin-bottom: 0px;"><h2 id="header_title">有料会員登録</h2></section>
	<div style="height: 20px;"></div>
<section class="swpm-login-widget-form register_contents">
<p class="index">有料会員登録をしていただくと、より多くの魅⼒的なコンテンツのご利⽤が可能となります。</p><br><br>

<div class="register_wrap">
	<div class="l_box">
			<h3 class="">免费会员套餐</h3>
			<p class="number">18</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編14、まねぶ編4コンテンツ</p>
	</div>
	<div class="c_box">
			<h3 class="">收费会员套餐</h3>
			<p class="number">122</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編63、まねぶ編59コンテンツ</p>
	</div>
	<div class="r_box">
			<h3 class="">非会員</h3>
			<p class="number">7</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編5、まねぶ編2コンテンツ</p>
	</div>

</div>

</section>
<hr class="register_hr">
<section class="swpm-login-widget-form register_contents">


	<h4 class="">有料会員登録</h4>
	<p class="index">有料会員登録をしていただくと、すべてのコンテンツのご利⽤が可能となります。</p>
	<p class="index">※) 「申込⽇」を基準⽇として、30⽇毎に⾃動更新され、料⾦の請求が⽣じます。</p>
	<p class="index">※) ご利⽤は、⾃動更新となります。ご登録後、マイページで解約⼿続きを⾏うことで、更新を停⽌することが可能です。</p>
    
    
<div class="fee_wrp">
    <h3 class="">１ヶ月</h3>
    <p class="">990円（税込）/月額</p>
	<a class="fee_register_btn" href="https://ch.easelart.io/asp-payment-box/?product_id=1323">有料会員登録</a>

</div>
</section>
			<?php else : ?>

<section id="header-title" style="margin-bottom: 0px;"><h2 id="header_title">会員プラン変更</h2></section>
	<div style="height: 20px;"></div>
<section class="swpm-login-widget-form register_contents">
<p class="index">会員登録をしていただくと、より多くの魅⼒的なコンテンツのご利⽤が可能となります。テスト表示です</p><br><br>

<!--
<div class="register_wrap">
	<div class="l_box">
			<h3 class="">無料会員</h3>
			<p class="number">18</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編14、まねぶ編4コンテンツ</p>
	</div>
	<div class="c_box">
			<h3 class="">有料会員</h3>
			<p class="number">122</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編63、まねぶ編59コンテンツ</p>
	</div>
	<div class="r_box">
			<h3 class="">非会員</h3>
			<p class="number">7</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編5、まねぶ編2コンテンツ</p>
	</div>

</div>

	<h4 class="">無料会員登録</h4>
	<p class="index">無料会員登録をしていただくと、18コンテンツのご利⽤が可能となります。</p>
	<a href="/free" class="free_register_btn">無料会員登録</a>
</section>
<hr class="register_hr">
-->
<section class="swpm-login-widget-form register_contents">


	<h4 class="">有料会員登録</h4>
	<p class="index">有料会員登録をしていただくと、すべてのコンテンツのご利⽤が可能となります。</p>
	<p class="index">※) 「申込⽇」を基準⽇として、30⽇毎に⾃動更新され、料⾦の請求が⽣じます。</p>
	<p class="index">※) ご利⽤は、⾃動更新となります。ご登録後、マイページで解約⼿続きを⾏うことで、更新を停⽌することが可能です。</p>
    
    
<div class="fee_wrp">
    <h3 class="">１ヶ月</h3>
    <p class="">990円（税込）/月額</p>
	<a class="fee_register_btn" href="https://ch.easelart.io/asp-payment-box/?product_id=1323">有料会員登録</a>

</div>
</section>
			<?php endif; ?>
<?php else : ?>

<section id="header-title" style="margin-bottom: 0px;"><h2 id="header_title">会員登録</h2></section>
	<div style="height: 20px;"></div>
<section class="swpm-login-widget-form register_contents">
<p class="index">会員登録をしていただくと、より多くの魅⼒的なコンテンツのご利⽤が可能となります。</p><br><br>

<div class="register_wrap">
	<div class="l_box">
			<h3 class="">免费会员套餐</h3>
			<p class="number">18</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編14、まねぶ編4コンテンツ</p>
	</div>
	<div class="c_box">
			<h3 class="">收费会员套餐</h3>
			<p class="number">122</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編63、まねぶ編59コンテンツ</p>
	</div>
	<div class="r_box">
			<h3 class="">非会員</h3>
			<p class="number">7</p>
			<p class="contents">コンテンツ</p>
			<p class="word">まなぶ編5、まねぶ編2コンテンツ</p>
	</div>

</div>

	<h4 class="">無料会員登録</h4>
	<p class="index">無料会員登録をしていただくと、18コンテンツのご利⽤が可能となります。</p>
	<a href="/free" class="free_register_btn">無料会員登録</a>
</section>
<hr class="register_hr">
<section class="swpm-login-widget-form register_contents">


	<h4 class="">有料会員登録</h4>
	<p class="index">有料会員登録をしていただくと、すべてのコンテンツのご利⽤が可能となります。</p>
	<p class="index">※) 「申込⽇」を基準⽇として、30⽇毎に⾃動更新され、料⾦の請求が⽣じます。</p>
	<p class="index">※) ご利⽤は、⾃動更新となります。ご登録後、マイページで解約⼿続きを⾏うことで、更新を停⽌することが可能です。</p>
    
    
<div class="fee_wrp">
    <h3 class="">１ヶ月</h3>
    <p class="">990円（税込）/月額</p>
	<a class="fee_register_btn" href="https://ch.easelart.io/asp-payment-box/?product_id=1323">有料会員登録</a>

</div>
</section>
<?php endif; ?>











<?php get_footer(); ?>
