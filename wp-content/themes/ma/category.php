<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header('sec'); ?>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a class="brand-logo" href="../">
          <h1>ESQUISSE</h1>
        </a><a class="sidenav-trigger" href="#" data-target="mobile-demo"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>">主页</a></li>
          <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>01/">入門編</a></li>
          <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>02/">つくる編</a></li>
        </ul>
      </div>
    </nav>
  </div>
  <ul class="sidenav" id="mobile-demo">
    <li>
      <div class="user-view">
        <div class="background"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/mainimage.png" style="height: 300px;" /></div><a href="http://inertiaart.io/"><span class="white-text name" style="font-family: DINNextLTPro-Bold,sans-serif; font-size:23px;">ESQUISSE</span></a><span class="white-text email" style="font-family: DINNextLTPro-Bold,sans-serif; mix-blend-mode: soft-light;">COMPLETE VERSION</span></div>
    </li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>"><i class="material-icons">home</i>主页</a></li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>01/"><i class="material-icons">tag_faces</i>入門編</a></li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>02/"><i class="material-icons">brush</i>つくる編</a></li>
    <li>
      <div class="divider"></div>
    </li>
    <li><a class="waves-effect" href="http://inertiaart.io/"><i class="material-icons">launch</i>ポータルサイトINERTIA</a></li>
  </ul>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>



  <section id="list-container">
    <ul id="section-list">
      <li class="section-Title" id="sec01_00">
        <div class="section-topFig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image01.gif"></div>
        <div class="seclist-blocks">
          <h3 class="section-enTitle">Start</h3>
          <h2 class="section-jpTitle">はじめよう</h2>
        </div>
      </li>
<?php query_posts("showposts=30"); ?>
<?php while (have_posts()) : the_post(); ?>

<?php
  $category = get_the_category();
  $cat_id   = $category[0]->cat_ID;
  $cat_name = $category[0]->cat_name;
  $cat_slug = $category[0]->category_nicename;
?>
      <li class="section-contents" id="sec02_01">
        <a href="<?php the_permalink(); ?>">
          <div class="seclist-blocks">
            <hr class="contentlist-hr">
            <div class="number-blocks">
		<?php if ( has_post_thumbnail() ):?>
		<?php the_post_thumbnail("topinfosize"); ?>
		<?php else : ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/image/news_sample.png" alt="INERTIA NEWS" />
		<?php endif; ?>
              <p class="number_sec01">01</p>
            </div>
            <div class="seclist-title">
              <h2><?php the_title(); ?></h2>
            </div>
          </div>
        </a>
      </li>
</li><?php endwhile; ?>


    </ul>
    <hr class="contentlist-hr">
  </section>














<?php get_footer(); ?>
