<?php
/**
 * Twenty Nineteen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage onｴclever_easel
 * @since 1.0.0
 */

/**
 * Twenty Nineteen only works in WordPress 4.7 or later.
 */

// CSS


function custom_action_row($actions, $post){
    
    if ( !current_user_can( 'administrator' ) ) { //管理者以外なら非表示
       unset($actions['inline hide-if-no-js']); //クイック編集
       unset($actions['view']); //プレビュー 
    }

    return $actions;
}
add_filter('post_row_actions','custom_action_row', 10, 2);


add_filter( 'run_wptexturize', '__return_false' );


function my_customize_mce_options( $init ) {
  global $allowedposttags;

  $init['valid_elements']          = '*[*]';
  $init['extended_valid_elements'] = '*[*]';
  $init['valid_children']          = '+a[' . implode( '|', array_keys( $allowedposttags ) ) . ']';
  $init['indent']                  = true;
  $init['wpautop']                 = false;
  $init['force_p_newlines'] = false;
  $init['force_br_newlines'] = true;
  $init['forced_root_block'] = '';

  return $init;
}

add_filter( 'tiny_mce_before_init', 'my_customize_mce_options' );


// playceholderの変更
function title_placeholder_change( $title ) {
	$screen = get_current_screen();
	if ( $screen->post_type == 'sketchbook' ) {
		$title = '输入作品的标题';
	}
	return $title;
}
add_filter( 'enter_title_here', 'title_placeholder_change' );





function add_custom_preview_css() {
?>
<style>
    .misc-pub-post-status{
        display:none;   
    }
    #save-action .spinner, #show-comments a {
    /* float: left; */
    position: absolute;
}
</style>
<?php
}
add_action( 'admin_footer-post-new.php', 'add_custom_preview_css' );


add_action('init', function() {
remove_filter('the_excerpt', 'wpautop');
remove_filter('the_content', 'wpautop');
});
add_filter('tiny_mce_before_init', function($init) {
$init['wpautop'] = false;
$init['apply_source_formatting'] = true;
return $init;
});



function add_custom_preview_button() {
?>

<script>
  (function($) {
    $('#wp-content-wrap').append('<a id="custom-preview" class="button">'+ $('#post-preview').text() +'</a>');
    $(document).on('click', '#custom-preview', function(e) {
      e.preventDefault();
      $('#post-preview').click();
    });
  }(jQuery));
</script>
<?php
}
add_action( 'admin_footer-post-new.php', 'add_custom_preview_button' );
add_action( 'admin_footer-post.php', 'add_custom_preview_button' );



function views() {
		$views = $this->get_views();
		$views = apply_filters( 'views_' . $this->screen->id, $views );

		if ( empty( $views ) )
			return;

		echo "<ul class='subsubsub'>\n";
		foreach ( $views as $class => $view ) {
			$views[ $class ] = "\t<li class='$class'>$view";
		}
		echo implode( " |</li>\n", $views ) . "</li>\n";
		echo "</ul>";
	}

//パーマリンク非表示
//add_filter( 'get_sample_permalink_html' , '__return_false' );













function my_admin_bar_bump_cb() {
	?>
<style type="text/css" media="screen">
	html { margin-top: 0px !important; padding-top: 0px !important;}
	* html body { margin-top: 0px !important;  padding-top: 0px !important;}
	@media screen and ( max-width: 782px ) {
		html { margin-top: 0px !important; padding-top: 0px !important; }
		* html body { margin-top: 0px !important; padding-top: 0px !important; }

	}

</style>

	<?php
}

function my_admin_bar_init() {
	// remove_action( 'wp_head', '_admin_bar_bump_cb' );
	add_action( 'wp_head', 'my_admin_bar_bump_cb' );
}
add_action( 'admin_bar_init', 'my_admin_bar_init' );


function my_edit_posts_per_page ($posts_per_page) {
	return 10;
}
add_filter('edit_posts_per_page', 'my_edit_posts_per_page');

//投稿者用、管理画面easelヘッダー

function my_admin_enqueue_scripts(){ ?>
<?php if ( current_user_can('author') || current_user_can('editor') ): ?>
<?php get_header('admin'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
      $(function() {
    $('#postimagediv').insertBefore('#submitdiv');
});
  </script>

<script>
      $(function() {
    $('#postdivrich').insertBefore('#titlediv');
});
</script>
<script>
      $(function() {
    $('#wp-content-editor-tools2').insertAfter('#wp-content-editor-container');
});
</script>

<section class="section--scrolldown" id="scrolldown" style="display: none;"><a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;"><span></span>スクロールしてください</a></section>
<div id="content-container" style="display: block;">
<section id="header-title" style="margin-bottom: 0px;">
<h2 id="header_title">作品を新規作成</h2>
</section>
</div>
<?php endif; ?>
<?php }
add_action( 'admin_enqueue_scripts', 'my_admin_enqueue_scripts' );


//投稿者用、管理画面easelフッター

add_action( 'admin_footer-post.php', 'my_post_edit_page_footer' );
if ( current_user_can('author') || current_user_can('editor') ):
function my_post_edit_page_footer(){
  echo '<section class="sketchbook_footer">
<div class="sketch_title"><span class="sketch_icon"></span>SketchBook</div>
<div class="sketch_menu">
    <a href="' .home_url(). '/wp-admin/post-new.php?post_type=sketchbook">新的创作</a>
    <a href="' .home_url(). '/wp-admin/edit.php?post_type=sketchbook">修改作品</a>
</div>
</section><section id="footer_copyright">
    <div style="text-align: center; margin-bottom: 10px; padding-top: 5px;"><a href="http://inertiaart.io/" target="_blank"><img src="' .get_bloginfo("stylesheet_directory"). '/assets/img/INERTIA_LOGO.svg" style="height: 15px; margin: 0 10px;"/></a>
    </div>
    <div style="text-align: center"><a href="' .home_url(). '/privacy"><span class="footer--text footer--text__small">隐私权政策</span></a><span style="margin: 3px;"></span><a href="' .home_url(). '/faq"><span class="footer--text footer--text__small">常见问题</span></a><span style="margin: 3px;"></span><a href="' .home_url(). '/transaction"><span class="footer--text footer--text__small">基于特定商户交易法的公示信息</span></a></div><div style="text-align: center"><a href="' .home_url(). '/trouble-shooting"><span class="footer--text footer--text__small">故障排除</span></a><span style="margin: 3px;"></span><span style="margin: 3px;"></span><a href="' .home_url(). '/contact"><span class="footer--text footer--text__small">问询</span></a></div>
    <p class="footer--text footer--text__small">&copy; 2021 株式会社INERTIA All Rights Reserved.</p>
  </section>';
}
endif;


add_action( 'admin_footer-post-new.php', 'my_post_edit_page_new_footer' );
if ( current_user_can('author') || current_user_can('editor')):
function my_post_edit_page_new_footer(){
  echo '<section class="sketchbook_footer">
<div class="sketch_title"><span class="sketch_icon"></span>SketchBook</div>
<div class="sketch_menu">
    <a href="' .home_url(). '/wp-admin/post-new.php?post_type=sketchbook">新的创作</a>
    <a href="' .home_url(). '/wp-admin/edit.php?post_type=sketchbook">修改作品</a>
</div>
</section><section id="footer_copyright">
    <div style="text-align: center; margin-bottom: 10px; padding-top: 5px;"><a href="http://inertiaart.io/" target="_blank"><img src="' .get_bloginfo("stylesheet_directory"). '/assets/img/INERTIA_LOGO.svg" style="height: 15px; margin: 0 10px;"/></a>
    </div>
    <div style="text-align: center"><a href="' .home_url(). '/privacy"><span class="footer--text footer--text__small">隐私权政策</span></a><span style="margin: 3px;"></span><a href="' .home_url(). '/faq"><span class="footer--text footer--text__small">常见问题</span></a><span style="margin: 3px;"></span><a href="' .home_url(). '/transaction"><span class="footer--text footer--text__small">基于特定商户交易法的公示信息</span></a></div><div style="text-align: center"><a href="' .home_url(). '/trouble-shooting"><span class="footer--text footer--text__small">故障排除</span></a><span style="margin: 3px;"></span><span style="margin: 3px;"></span><a href="' .home_url(). '/contact"><span class="footer--text footer--text__small">问询</span></a></div>
    <p class="footer--text footer--text__small">&copy; 2021 株式会社INERTIA All Rights Reserved.</p>
  </section>';
}
endif;




//投稿者用、管理画面easelフッター

add_action( 'admin_footer-edit.php', 'my_post_edit_edit_footer' );
if ( current_user_can('author') || current_user_can('editor') ):
function my_post_edit_edit_footer(){
  echo '<section class="sketchbook_footer">
<div class="sketch_title"><span class="sketch_icon"></span>SketchBook</div>
<div class="sketch_menu">
    <a href="' .home_url(). '/wp-admin/post-new.php?post_type=sketchbook">新的创作</a>
    <a href="' .home_url(). '/wp-admin/edit.php?post_type=sketchbook">修改作品</a>
</div>
</section><section id="footer_copyright">
    <div style="text-align: center; margin-bottom: 10px; padding-top: 5px;"><a href="http://inertiaart.io/" target="_blank"><img src="' .get_bloginfo("stylesheet_directory"). '/assets/img/INERTIA_LOGO.svg" style="height: 15px; margin: 0 10px;"/></a>
    </div>
    <div style="text-align: center"><a href="' .home_url(). '/privacy"><span class="footer--text footer--text__small">隐私权政策</span></a><span style="margin: 3px;"></span><a href="' .home_url(). '/faq"><span class="footer--text footer--text__small">常见问题</span></a><span style="margin: 3px;"></span><a href="' .home_url(). '/transaction"><span class="footer--text footer--text__small">基于特定商户交易法的公示信息</span></a></div><div style="text-align: center"><a href="' .home_url(). '/trouble-shooting"><span class="footer--text footer--text__small">故障排除</span></a><span style="margin: 3px;"></span><span style="margin: 3px;"></span><a href="' .home_url(). '/contact"><span class="footer--text footer--text__small">问询</span></a></div>
    <p class="footer--text footer--text__small">&copy; 2021 株式会社INERTIA All Rights Reserved.</p>
  </section>';
}
endif;




// カスタム投稿の場合
add_filter( 'get_user_option_screen_layout_sketchbook', 'custom_post_one_columns_screen_layout' );
function custom_post_one_columns_screen_layout() {
  return 1;
}




//ログアウトリダイレクト

add_filter( 'logout_redirect', 'custom_logout_redirect' );
function custom_logout_redirect( $redirect_to ) {
  $redirect_to = home_url();
  return $redirect_to;
}








//管理画面CSS

function admin_css() {
if (!current_user_can('level_8')) { 
  echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo("stylesheet_directory").'/css/admin.css">';
}}
add_action( 'admin_head', 'admin_css' );





//レベル別bodyのclass
function add_user_role_class( $admin_body_class ) {
    global $current_user;
    if ( ! $admin_body_class ) {
        $admin_body_class .= ' ';
    }
    $admin_body_class .= 'role-' . urlencode( $current_user->roles[0] );
    return $admin_body_class;
}
add_filter( 'admin_body_class', 'add_user_role_class' );



//stripe payments注意書き
function my_asp_ng_pp_output_before_buttons(){ ?>
   <div class="text-center">
       <p>※下記のメールアドレスが<b style="font-weight:bold">easelログイン用メールアドレス</b>及び、<b style="font-weight:bold">本登録用アクティベーションメールの送付先</b>になりますので、<b style="color:#ff0000;">お支払い前に必ずご確認ください。</b></p>
<p>Apple Pay：Apple Payボタンを選択した直後に表示される明細項目で設定可能なメールアドレス</p>
<p>Google Pay：お支払いプロファイルの連絡先情報に設定されているメールアドレス</p>
<p>クレジットカード：お支払い方法選択後の記入フォームに入力したメールアドレス</p>



    </div>
<?php }
add_action( 'asp_ng_pp_output_before_buttons', 'my_asp_ng_pp_output_before_buttons' );


//クイックタグ消去
function remove_quicktags( $qtInit ) {
	$qtInit['buttons'] = 'dfw';
	return $qtInit;
}
add_filter( 'quicktags_settings', 'remove_quicktags' );





//投稿画面のメタボックスの配置を変更する方法
function eyecatch_arrangement_script( $hook ) {
    $screen = get_current_screen();
  if ( $screen->post_type == 'sketchbook' ) {
      if ( $hook == 'post.php' || $hook == 'post-new.php' ) {
    $script = <<<SCRIPT
    jQuery(function($) {
      $('#submitdiv').insertAfter('#postimagediv');
    });
    SCRIPT;
        wp_add_inline_script( 'editor', $script );
      }
  }
}
add_action( 'admin_enqueue_scripts', 'eyecatch_arrangement_script' );




add_action( 'admin_enqueue_scripts', 'add_codemirror_for_quick_draft' );

	function add_codemirror_for_quick_draft(){

		if ( $post_type == 'post' ) {
			return;
		}

		$settings= wp_enqueue_code_editor( array( 'type'=> 'text/javascript' ) );

		if ( true=== $settings ) {
			return;
		}
        $settings= wp_enqueue_code_editor( array( 'type'=> 'text/javascript' ) );
        
		wp_add_inline_script(
			'code-editor',
			sprintf(
				'jQuery( function() { wp.codeEditor.initialize( "content", %s ); } );',
				wp_json_encode( $settings )
			)
		);	
	}




remove_filter('the_title', 'wptexturize');
remove_filter('the_content', 'wptexturize');
remove_filter('the_excerpt', 'wptexturize');
remove_filter('comment_text', 'wptexturize');
remove_filter('the_title'  , 'convert_chars');
remove_filter('the_content', 'convert_chars');
remove_filter('the_excerpt', 'convert_chars');
remove_filter('comment_text', 'convert_chars');

function show_only_authorpost($query) {
    // global $current_user;
    // if(is_admin()){
    //     if(current_user_can('author') ){
    //         $query->set('author', $current_user->ID);
    //     }
    // }
}
add_action('pre_get_posts', 'show_only_authorpost');

add_action( 'wp_before_admin_bar_render', 'hide_before_admin_bar_render' );
function hide_before_admin_bar_render() {
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu( 'wp-logo' );
}
function admin_favicon() {
  echo '<link rel="shortcut icon" type="image/x-icon" href="'.get_stylesheet_directory_uri().'/assets/image/favicon.ico" />';
}
add_action( 'admin_head', 'admin_favicon' );

function after_title() {
  echo '<!--<p>パーマリンクは英数字にてご指定下さい。</p>-->';
}
add_action( 'edit_form_after_title', 'after_title' );

function after_editor() {
  echo '<div id="wp-content-editor-tools2" class="wp-editor-tools hide-if-no-js"><div id="wp-content-media-buttons" class="wp-media-buttons"><button type="button" id="insert-media-button" class="button insert-media add_media" data-editor="content"><span class="wp-media-buttons-icon"></span> 追加媒体</button></div>
<div class="wp-editor-tabs"></div>
</div>';
}
add_action( 'edit_form_after_editor', 'after_editor' );

add_filter( 'admin_post_thumbnail_html', function( $html, $post_id, $thumbnail_id ) {
	if ( 'brand' === get_post_type( $post_id ) ) {
		$html = <<<HTML
			{$html}
			<p class="description">正方形で表示されるので、1024x1024pxにしてください。</p>
HTML;
	}
	return $html;
}, 10, 3 );

//カスタム投稿ポスト「sketchbook」のみビジュアルエディターモードを無効化する
function disable_cf_visual_editor_mode() {
	global $typenow;
	if ($typenow == 'sketchbook') {
		add_filter('user_can_richedit', 'disable_cf_visual_editor_mode_filter');
	}
}

function disable_cf_visual_editor_mode_filter() {
	return false;
}
add_action('load-post.php', 'disable_cf_visual_editor_mode');
add_action('load-post-new.php', 'disable_cf_visual_editor_mode');

//カスタム投稿ポスト「sketchbook」のみメディアボタンとコンテンツ保護機能を無効化する
add_action('admin_print_styles', 'admin_book_css_custom');
function admin_book_css_custom() {
global $typenow;
if($typenow == 'sketchbook'):
echo '<style>#swpm_sectionid {display: none;}</style>';
endif;
}

add_action( 'template_redirect', 'login_init' );

function login_init() {
    if ( ! is_page( 'login' ) ) {
        return;
    }

    global $email, $error;
    $email = "";
    $error = "";

    if ( isset( $_POST['login_nonce'] ) ) {
        //nonceチェック
        if ( ! wp_verify_nonce( $_POST['login_nonce'], 'login') ) {
            $error .= '不正な遷移です';
        } else {
            //入力チェック
            if ( ! empty( $_POST['user_email'] ) ) {
                $email = $_POST['user_email'];
            } else {
                $error .= "メールアドレスを入力してください";
            }
            if ( ! empty( $_POST['user_pass'] ) ) {
                $password = $_POST['user_pass'];
            } else {
                $error .= "パスワードを入力してください";
            }
        }

        if ( ! $error ) {
            //ユーザー取得
            $user = get_user_by( 'email', $email );
            if ( false === $user ) {
                $error .= "メールアドレスが間違っています。";
            } else {
                //ログイン実行
                $creds = array();
                $creds['user_login'] = $user->data->user_login;
                $creds['user_password'] = $password;
                $creds['remember'] = true;
                $user = wp_signon( $creds, false );
                if ( is_wp_error( $user ) ) {
                    if ( array_key_exists( 'incorrect_password', $user->errors ) ) {
                        $error .= "パスワードが間違っています。";
                    } else {
                        $error .= "ログインに失敗しました。";
                    }
                } else {
                    //リダイレクト
                    wp_safe_redirect( home_url() );
                }
            }
        }
    }
}
//カスタム投稿の追加と、カテゴリー、タグ機能の追加
function my_custom_init() {

    //カテゴリタイプの設定（カスタムタクソノミーの設定）
      register_taxonomy(
        'sketchbookcat', //カテゴリー名（任意）
        'sketchbook', //カスタム投稿名
        array(
          'hierarchical' => true, //カテゴリータイプの指定
          'update_count_callback' => '_update_post_term_count',
          //ダッシュボードに表示させる名前
          'label' => 'sketchbookのカテゴリー', 
          'public' => true,
          'show_ui' => false
        )
      );    
    //タグタイプの設定（カスタムタクソノミーの設定）
      register_taxonomy(
        'sketchbooktag', //タグ名（任意）
        'sketchbook', //カスタム投稿名
        array(
          'hierarchical' => false, //タグタイプの指定（階層をもたない）
          'update_count_callback' => '_update_post_term_count',
          //ダッシュボードに表示させる名前
          'label' => 'sketchbookのタグ', 
          'public' => true,
          'show_ui' => false
        )
      );
  
}
add_action( 'init', 'my_custom_init' );



/**
 * ユーザー編集(ユーザー新規追加、プロフィール含む)の名姓の項目を姓名の順にします。
 */



function lastfirst_name() {
	?><script>
		jQuery(function($){
			$('#last_name').closest('tr').after($('#first_name').closest('tr'));
		});
	</script><?php
}
add_action( 'admin_footer-user-new.php', 'lastfirst_name' );
add_action( 'admin_footer-user-edit.php', 'lastfirst_name' );
add_action( 'admin_footer-profile.php', 'lastfirst_name' );
/**
 * ユーザー一覧の名前を姓名に変更します。(列の内部名の設定)
 */
function lastfirst_users_column( $columns ) {
	$new_columns = array();
	foreach ( $columns as $k => $v ) {
		if ( 'name' == $k ) $new_columns['lastfirst_name'] = $v;
		else $new_columns[$k] = $v;
	}
	return $new_columns;
}
add_filter( 'manage_users_columns', 'lastfirst_users_column' );
/**
 * ユーザー一覧の名前を姓名に変更します。(値の設定)
 */
function lastfirst_users_custom_column( $output, $column_name, $user_id ) {
	if ( 'lastfirst_name' == $column_name ) {
		$user = get_userdata($user_id);
		return $user->last_name . ' ' . $user->first_name;
	}
}
add_filter( 'manage_users_custom_column', 'lastfirst_users_custom_column', 10, 3 );
/**
 * ユーザー一覧の姓名のソート設定を行います。
 */
function lastfirst_users_sortable_column( $columns ) {
	$columns['lastfirst_name'] = 'lastfirst_name';
	return $columns;
}
add_filter( 'manage_users_sortable_columns', 'lastfirst_users_sortable_column' );
/**
 * ユーザー一覧の姓名のソート処理を追加します。
 */
function lastfirst_pre_user_query($query){
	global $wpdb;
	// 姓名
	if ( isset($query->query_vars['orderby']) && 'lastfirst_name' == $query->query_vars['orderby'] ) {
		$query->query_from .= " LEFT JOIN $wpdb->usermeta AS ln ON ($wpdb->users.ID = ln.user_id) AND ln.meta_key = 'last_name' ";
		$query->query_from .= " LEFT JOIN $wpdb->usermeta AS fn ON ($wpdb->users.ID = fn.user_id) AND fn.meta_key = 'first_name' ";
		$query->query_orderby = " ORDER BY CONCAT(ln.meta_value, fn.meta_value) " . ($query->query_vars["order"] == 'ASC' ? 'asc' : 'desc');
	}
	return $query;
}
add_filter( 'pre_user_query', 'lastfirst_pre_user_query' );

 
//更新通知を管理者権限のみに表示
function update_nag_admin_only() {
  if ( ! current_user_can( 'administrator' ) ) {
    remove_action( 'admin_notices', 'update_nag', 3 );
  }
}
add_action( 'admin_init', 'update_nag_admin_only' );


// フッターWordPressリンクを非表示に
function custom_admin_footer() {
 echo '';
 }
add_filter('admin_footer_text', 'custom_admin_footer');

// 管理バーのWordPressシンボルマークを非表示
function remove_admin_bar_menu( $wp_admin_bar ) {
 $wp_admin_bar->remove_menu( 'wp-logo' ); 
 }
add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 70 );

// 管理バーのヘルプメニューを非表示にする
function my_admin_head(){
 echo '<style type="text/css">#contextual-help-link-wrap{display:none;}</style>';
 }
add_action('admin_head', 'my_admin_head');

// ようこそを非表示にする
remove_action( 'welcome_panel', 'wp_welcome_panel' );

add_action( 'admin_menu', 'remove_admin_menu', 999 );
function remove_admin_menu() {
  remove_menu_page( 'edit-comments.php' );        // コメント
}
function remove_footer_version() {
  remove_filter('update_footer', 'core_update_footer');
}
add_action( 'admin_menu', 'remove_footer_version');

function child_category_link_custom( $query = array()) {

	if (isset($query['category_name']) && strpos($query['category_name'], '/') === false && isset($query['name'])) {
		$parent_category = get_category_by_slug($query['category_name']);
		$child_categories = get_categories('child_of='.$parent_category->term_id);	
		foreach ($child_categories as $child_category) {
			if ($query['name'] === $child_category->category_nicename) {
				$query['category_name'] = $query['category_name'].'/'.$query['name'];
				unset($query['name']);
			}
		}
	}
	return $query;
}
add_filter('request', 'child_category_link_custom');


function post_order_id( $query ){
    if( is_admin() ){
        if( $query->get( 'orderby' ) !== 'date' ){
                $query->set( 'orderby', 'id' );
        }
        if( $query->get( 'order' ) !== 'DESC' ){
                $query->set( 'order', 'DESC' );
        }
    }
}
add_action( 'pre_get_posts', 'post_order_id' );
add_action( 'auth_redirect', 'subscriber_go_to_home' );
function subscriber_go_to_home( $user_id ) {
	$user = get_userdata( $user_id );
	if ( !$user->has_cap( 'edit_posts' ) ) {
		wp_redirect( get_home_url() );
		exit();
	}
}
function show_only_authorimage( $where ){
    global $current_user;
    if(is_admin()){
        if(current_user_can('author') ){
            if( isset( $_POST['action'] ) && ( $_POST['action'] == 'query-attachments' ) ){
                $where .= ' AND post_author='.$current_user->data->ID;
            }
        }
    }
    return $where;
}
add_filter( 'posts_where', 'show_only_authorimage' );

add_filter( 'user_can_richedit', 'disable_visual_editor' );
function disable_visual_editor( $default ) {
  $screen = get_post_type();
  if ( $screen == 'post' ) {
    return false;
  } else {
    return $default;
  }

}

// アイキャッチ画像項目を追加します。
function my_manage_columns( $columns ) {
	if ( ! is_array( $columns ) ) {
		$columns = array();
	}
	$new_columns = array();
	foreach ( $columns as $key => $value ) {
		if ( $key == 'title' ) {
			$new_columns['featured-image'] = '图像';
		}
		$new_columns[$key] = $value;
	}
	return $new_columns;
}

add_filter( "manage_posts_columns", 'my_manage_columns' );	// 投稿一覧
add_filter( "manage_pages_columns", 'my_manage_columns' );	// 固定ページ一覧

// アイキャッチ画像項目を出力します。
function my_manage_custom_column( $column, $post_id ) {
	if ( $column == 'featured-image' ) {
		if ( has_post_thumbnail( $post_id ) ) {
			echo get_the_post_thumbnail( $post_id, 'thumbnail' );
		} else {
			echo '<div class="featured-image-none"></div>';
		}
	}
}

add_action( "manage_posts_custom_column", 'my_manage_custom_column', 10, 2 );	// 投稿一覧
add_action( "manage_pages_custom_column", 'my_manage_custom_column', 10, 2 );	// 固定ページ一覧

// アイキャッチ画像項目のスタイルを出力します。
function my_admin_head_edit() {
	$style = <<<STYLE
.fixed .column-featured-image {
	width: 60px;
}
.featured-image img,
.featured-image div {
	width: 48px;
	height: 48px;
}
.featured-image-none {
	border: #cccccc dashed 1px;
}
.wp-list-table tr:not(.inline-edit-row):not(.no-items) td.column-featured-image::before {
	content: "";
}
.wp-list-table tr:not(.inline-edit-row):not(.no-items) td.column-featured-image {
	display: table-cell !important;
}
.wp-list-table tr:not(.inline-edit-row):not(.no-items) td.hidden {
	display: none !important;
}
STYLE;

	echo '<style type="text/css" id="my-featured-image-column-css">' . "\n";
	echo $style;
	echo "</style>\n";
}

add_action( 'admin_head-edit.php', 'my_admin_head_edit', 100 );

// 投稿一覧にアイキャッチ画像フィルター選択項目を追加します。
function my_restrict_manage_posts_featured_image( $post_type ) {
	if (
		$post_type == 'post'		// 投稿一覧
		|| $post_type == 'page' 	// 固定ページ一覧
	) {
		$filter = isset( $_GET['featured_image_filter'] ) ? $_GET['featured_image_filter'] : '';
		echo '<select name="featured_image_filter" id="featured_image_filter">';
		echo '<option value="all" ' . selected( 'all', $filter ) . '>封面图</option>';
		echo '<option value="notset" ' . selected( 'notset', $filter ) . '>未設定</option>';
		echo '</select>' . "\n";
	}
}
add_action( 'restrict_manage_posts', 'my_restrict_manage_posts_featured_image' );

// 投稿一覧のクエリ―にアイキャッチ画像の条件を設定します。
function my_parse_query_featured_image( $query ) {
	global $pagenow;

	if ( is_admin() && $pagenow == 'edit.php' && isset( $_GET['featured_image_filter'] ) && $_GET['featured_image_filter'] != 'all' ) {
		$query->query_vars['meta_key'] = '_thumbnail_id';
		$query->query_vars['meta_compare'] = 'NOT EXISTS';
		$query->query_vars['meta_value'] = '';
	}
}
add_filter( 'parse_query', 'my_parse_query_featured_image' );




function admin_edit_confirm() {
	echo '<script>
	jQuery(document).ready(function() {
		
		//公開時と更新時に確認アラートを表示する
		jQuery("#publish").on("click", function(e){
			if (!confirm(jQuery("#publish").val() + "してもよろしいですか？")) {
				jQuery("#ajax-loading").hide();
				jQuery("#publish").removeClass("button-primary-disabled");
				jQuery("#save-post").removeClass("button-disabled");
				return false;
			}
		});
		
		//一括でゴミ箱を空にしてしまう「ゴミ箱を空にする」ボタンを非表示に
		if(jQuery("#delete_all").length > 0){
			jQuery("#delete_all").hide();
		}
		
		//ゴミ箱へ移動させる時
		jQuery(".trash .submitdelete").on("click", function(event) {
			if( !confirm("この作品を削除しますか？" )){
				return false;
			}
		});
		
		//ゴミ箱内の記事を「完全に削除する」時
		jQuery(".delete .submitdelete").on("click", function(event) {
			if( !confirm("この作品を完全に削除しますか？" )){
				return false;
			}
		});
	});
</script>';
}
add_action("admin_print_scripts", "admin_edit_confirm", 20);


remove_action( 'in_admin_header', 'action_in_admin_header', 10, 0 ); 

// 確認メッセージを設定
//$publish_msg = '保存します。よろしいですか？';
// 
//function confirm_publish() {
// 
//    global $publish_msg;
// 
//    // 「公開」ボタン押下時に確認メッセージを出力
//    echo '<script type="text/javascript"><!--
//    var publish = document.getElementById("publish");
//    if (publish !== null) publish.onclick = function(){
//        return confirm("' . $publish_msg . '");
//    };
//    // --></script>';
// 
//}
// 上記のJavaScriptのコードをフッターに挿入
//add_action('admin_footer', 'confirm_publish');


// 投稿者の場合投稿者名を非表示
// function custom_columns($columns) {
//         unset($columns['author']);
//         return $columns;
// }
// add_filter( 'manage_posts_columns', 'custom_columns' );



function custom_edit_newpost_delete($hook) {
	if($hook == 'post-new.php' || $hook == 'post.php'){

    $postType = get_post_type();
    if ( $postType == 'sketchbook' ) { //カスタム投稿スラッグがrecruitなら非表示
        echo '<style>.wrap .wp-heading-inline + .page-title-action{display: none;}</style>';
	  }

	}
}
add_action('admin_enqueue_scripts', 'custom_edit_newpost_delete');


add_filter( 'image_send_to_editor', 'remove_image_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_image_attribute', 10 );

function remove_image_attribute( $html ){
  $html = preg_replace( '/(width|height)="\d*"\s/', '', $html );
  $html = preg_replace( '/class=[\'"]([^\'"]+)[\'"]/i', '', $html );
  $html = preg_replace( '/title=[\'"]([^\'"]+)[\'"]/i', '', $html );
  $html = preg_replace( '/<a href=".+">/', '', $html );
  $html = preg_replace( '/<\/a>/', '', $html );
  return $html;
}


add_action( 'admin_print_styles', 'admin_css_custom' );
function admin_css_custom() {
  echo '<style>.attachment-details label[data-setting="caption"], .attachment-details label[data-setting="description"], div.attachment-display-settings { display: none; }</style>';
}


add_action('admin_head', function () {
    echo <<<EOT
<style>#visibility-radio-password, #password-span, label[for=visibility-radio-password] {display: none;}</style>
<script>jQuery(function(){jQuery("select#post_status > option[value=pending]").remove();});</script>
EOT;
});



function add_mce_content_placeholder( $plugin_array ){
    if( 'sketchbook' === get_post_type() ){
        $plugin_array[ 'placeholder' ] = get_template_directory_uri() . '/assets/js/plugin.min.js';
    }
    return $plugin_array;
}
add_filter( 'mce_external_plugins', 'add_mce_content_placeholder' );
function add_the_content_editor_placeholder( $the_content_editor_html ){
    if( 'sketchbook' === get_post_type() ){
        $placeholder = '请输入资源';
        $the_content_editor_html = preg_replace( '/<textarea/', "<textarea placeholder=\"{$placeholder}\"", $the_content_editor_html );
    }
    return $the_content_editor_html;
}
add_filter( 'the_editor', 'add_the_content_editor_placeholder' );

/**
 * Send GA event
 */
function gaEventScript() {
    global $current_user;
    echo <<< EOM
    <script>
    const email = "{$current_user->user_email}"
    const path = location.pathname;
    if (email && window.gtag) {
        window.gtag('event', 'page_view_by_user', {
            'event_category': path,
            'event_label': email
        });
    }
    </script>
EOM;
}
add_action('wp_footer', 'gaEventScript');


// hide adminbar
add_filter( 'show_admin_bar', '__return_false' );

?>