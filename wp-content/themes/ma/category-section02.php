<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header('sec'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>



  <section id="list-container">
    <ul id="section-list">
      <li class="section-Title" id="sec01_00">
        <div class="section-topFig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image02.gif"></div>
        <div class="seclist-blocks">
          <h3 class="section-enTitle">Form</h3>
          <h2 class="section-jpTitle">図形</h2>
        </div>
      </li>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
     <li class="section-contents" id="sec02_01">
        <a href="https://ch.easelart.io/01/section02/sec02_01">
          <div class="seclist-blocks">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_01.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_01.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_01-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">02</p>
            </div>
            <div class="seclist-title">
              <h2>点を書く</h2>
            </div>
          </div>
        </a>
      </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_02.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">03</p>
            </div>
            <div class="seclist-title">
              <h2>直線を描く</h2>
            </div>
          </div>
        </a>
      </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_03.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">04</p>
            </div>
            <div class="seclist-title">
              <h2>三角形を描く</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_04.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">05</p>
            </div>
            <div class="seclist-title">
              <h2>四角形を描く</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_05.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">06</p>
            </div>
            <div class="seclist-title">
              <h2>円を描く</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_06.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_06.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_06-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">07</p>
            </div>
            <div class="seclist-title">
              <h2>キャンバスをつくる</h2>
            </div>
              </div>
          </div>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_07.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">08</p>
            </div>
            <div class="seclist-title">
              <h2>プログラムの構造</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_08.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">09</p>
            </div>
            <div class="seclist-title">
              <h2>乱数を使う</h2>
            </div>
          </div>
          </a>
        </li>
			<?php else : ?>
<?php
				$args = array(
					'post_type' => 'post', /* 投稿タイプ */
					'paged' => $paged ,
					'category_name' => 'section02' // 表示したいカテゴリーのスラッグを指定
				); ?>
				<?php query_posts( $args ); ?>
<?php if(is_archive()) : query_posts($query_string.'&order=ASC'); endif ?>
		

				<?php while (have_posts()) : the_post(); ?>

<?php
  $category = get_the_category();
  $cat_id   = $category[0]->cat_ID;
  $cat_name = $category[0]->cat_name;
  $cat_slug = $category[0]->category_nicename;
?>
      <li class="section-contents" id="sec02_01">
        <a href="<?php the_permalink(); ?>">
          <div class="seclist-blocks">
            <hr class="contentlist-hr">
            <div class="number-blocks">
		<?php if ( has_post_thumbnail() ):?>
		<?php the_post_thumbnail("topinfosize"); ?>
		<?php else : ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/image/news_sample.png" alt="INERTIA NEWS" />
		<?php endif; ?>
              <p class="number_sec01"><?php $page_mumber = SCF::get('page_mumber'); echo $page_mumber; ?></p>
            </div>
            <div class="seclist-title">
              <h2><?php the_title(); ?></h2>
            </div>
          </div>
        </a>
      </li>
<?php endwhile; ?>
			<?php endif; ?>
<?php else : ?>
     <li class="section-contents" id="sec02_01">
        <a href="https://ch.easelart.io/01/section02/sec02_01">
          <div class="seclist-blocks">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_01.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_01.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_01-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">02</p>
            </div>
            <div class="seclist-title">
              <h2>点を書く</h2>
            </div>
          </div>
        </a>
      </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_02.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">03</p>
            </div>
            <div class="seclist-title">
              <h2>直線を描く</h2>
            </div>
          </div>
          </a>
      </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_03.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">04</p>
            </div>
            <div class="seclist-title">
              <h2>三角形を描く</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_04.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">05</p>
            </div>
            <div class="seclist-title">
              <h2>四角形を描く</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_05.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">06</p>
            </div>
            <div class="seclist-title">
              <h2>円を描く</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_06.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_06.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_06-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">07</p>
            </div>
            <div class="seclist-title">
              <h2>キャンバスをつくる</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_07.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">08</p>
            </div>
            <div class="seclist-title">
              <h2>プログラムの構造</h2>
            </div>
          </div>
          </a>
        </li>

      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/inertia-section_img_08.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">09</p>
            </div>
            <div class="seclist-title">
              <h2>乱数を使う</h2>
            </div>
          </div>
          </a>
        </li>
<?php endif; ?>










    </ul>

    <hr class="contentlist-hr">
  </section>














<?php get_footer(); ?>
