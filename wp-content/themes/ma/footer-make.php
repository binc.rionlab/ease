		<!--==============================footer=================================-->
  <section id="list-underBlock"></section>
  <section id="footer-menu">
    <ul id="navi-ul">
      <li class="navi-list">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>01/">
          <div class="navi-blocks">
            <p class="navi-jpTitle">学习</p>
            <p class="navi-enTitle">Entry</p>
          </div>
        </a>
      </li>
      <li class="navi-list navi-active">
        <div class="navi-blocks">
          <p class="navi-jpTitle">模仿</p>
          <p class="navi-enTitle">Make</p>
        </div>
      </li>
    </ul>
  </section>
  <div class="modal" id="showPlanInfo" style="overflow: visible;">
    <div class="modal-wrapper" style="position: relative; border-radius: 3px;">
      <div class="modal-content">
        <h2>ESQUISSE MASTER</h2>
        <div class="modal--block">
          <h3><i class="material-icons">tag_faces</i> 入門編をコンプリート</h3>
          <p>入門編のすべてのコンテンツにアクセスできるようになります。</p>
        </div>
        <div class="modal--block">
          <h3><i class="material-icons">brush</i> つくる編で出会う</h3>
          <p>つくる編のすべてのコンテンツにアクセスできるようになります。</p>
        </div>
        <div class="modal--block">
          <div><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/mozart.png" style="width: 80%; display: block; margin: auto;"></div>
        </div>
        <div class="modal--block">
          <a href="#">
            <div class="button--detail">近日公開！</div>
          </a>
        </div>
      </div>
      <div style="position: absolute; width: 50px; height: 50px; background: #ffffff; display: flex; justify-content: center; align-items: center; border-radius: 50%; top: -25px; right: -25px;"><i class="fas fa-times modal-close waves-effect" style="font-size: 36px; left: 1px;"></i></div>
    </div>
  </div>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/masonry.pkgd.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/imagesloaded.pkgd.min.js"></script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/make_list.js"></script>
  <script>
    Pace.on('done',function(){$('#list-container').fadeIn();$('#box-container').fadeIn();$('#footer_copyright').fadeIn();$('.modal').modal();});
    function showInfo() {$('#showPlanInfo').modal('open');return false;}
  </script>
<?php wp_footer(); ?>
</body>
</html>