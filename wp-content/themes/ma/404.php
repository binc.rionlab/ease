<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header('sec'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>
<a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;">
<div class="inner">
	<div id="primary" class="site-content lower-site-content">
		<div id="content" role="main">

			<article id="post-0" class="post error404 no-results not-found">
				<header class="entry-header">
					<h1 class="entry-title">対象のページは存在しませんでした。</h1>
				</header>

				<div class="entry-content">
					
				</div><!-- .entry-content -->
			</article><!-- #post-0 -->

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- /.inner -->
<?php get_footer(); ?>
