<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header('sec'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>



  <section id="list-container">
    <ul id="section-list">
      <li class="section-Title" id="sec01_00">
        <div class="section-topFig"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/01img/image12.gif"></div>
        <div class="seclist-blocks">
          <h3 class="section-enTitle">Image</h3>
          <h2 class="section-jpTitle">イメージ</h2>
        </div>
      </li>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
      <li class="section-contents" id="sec02_01">
        <a href="https://ch.easelart.io/01/section12/sec12_01">
          <div class="seclist-blocks">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_01.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_01.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/sec12_01-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">55</p>
            </div>
            <div class="seclist-title">
              <h2>イメージを表示する</h2>
            </div>
          </div>
            </a>
      </li>
      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_02.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_02.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/sec12_02-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">56</p>
            </div>
            <div class="seclist-title">
              <h2>イメージの大きさを変える</h2>
            </div>
          </div>
          </a>
      </li>
      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_03.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_03.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/sec12_03-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">57</p>
            </div>
            <div class="seclist-title">
              <h2>イメージを加工する</h2>
            </div>
          </div>
          </a>
      </li>
      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="200" height="200" src="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_04.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">58</p>
            </div>
            <div class="seclist-title">
              <h2>イメージの色を調べる</h2>
            </div>
          </div>
          </a>
      </li>



			<?php else : //有料会員 ?>
<?php
				$args = array(
					'post_type' => 'post', /* 投稿タイプ */
					'paged' => $paged ,
					'category_name' => 'section12' // 表示したいカテゴリーのスラッグを指定
				); ?>
				<?php query_posts( $args ); ?>
		<?php if(is_archive()) : query_posts($query_string.'&order=ASC'); endif ?>

				<?php while (have_posts()) : the_post(); ?>

<?php
  $category = get_the_category();
  $cat_id   = $category[0]->cat_ID;
  $cat_name = $category[0]->cat_name;
  $cat_slug = $category[0]->category_nicename;
?>
      <li class="section-contents" id="sec02_01">
        <a href="<?php the_permalink(); ?>">
          <div class="seclist-blocks">
            <hr class="contentlist-hr">
            <div class="number-blocks">
		<?php if ( has_post_thumbnail() ):?>
		<?php the_post_thumbnail("topinfosize"); ?>
		<?php else : ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/image/news_sample.png" alt="INERTIA NEWS" />
		<?php endif; ?>
              <p class="number_sec01"><?php $page_mumber = SCF::get('page_mumber'); echo $page_mumber; ?></p>
            </div>
            <div class="seclist-title">
              <h2><?php the_title(); ?></h2>
            </div>
          </div>
        </a>
      </li><?php endwhile; ?>

        
        
			<?php endif; ?>
<?php else : //ログイン前 ?>
      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_01.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_01.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/sec12_01-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">55</p>
            </div>
            <div class="seclist-title">
              <h2>イメージを表示する</h2>
            </div>
          </div>
          </a>
      </li>
      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_02.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_02.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/sec12_02-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">56</p>
            </div>
            <div class="seclist-title">
              <h2>イメージの大きさを変える</h2>
            </div>
          </div>
          </a>
      </li>
      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="300" height="300" src="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_03.jpg" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" srcset="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_03.jpg 300w, https://ch.easelart.io/wp-content/uploads/2019/10/sec12_03-150x150.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />		              <p class="number_sec01">57</p>
            </div>
            <div class="seclist-title">
              <h2>イメージを加工する</h2>
            </div>
          </div>
          </a>
      </li>
      <li class="section-contents" id="sec02_01">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
          <div class="seclist-blocks glayout">
            <hr class="contentlist-hr">
            <div class="number-blocks">
				<img width="200" height="200" src="https://ch.easelart.io/wp-content/uploads/2019/10/sec12_04.gif" class="attachment-topinfosize size-topinfosize wp-post-image" alt="" />		              <p class="number_sec01">58</p>
            </div>
            <div class="seclist-title">
              <h2>イメージの色を調べる</h2>
            </div>
          </div>
          </a>
      </li>


<?php endif; ?>











    </ul>

    <hr class="contentlist-hr">
  </section>














<?php get_footer(); ?>
