<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

	
	
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<!--		<header class="entry-header">
			<div class="txt-right"><?php the_time("Y年n月j日 l"); ?></div>
		</header><!-- .entry-header -->

		<div class="kiji">
			<?php the_content(); ?>
		</div><!-- .entry-summary -->
		
		

				


<!--▼ページ送り▼-->
<?php
$prevpost = get_adjacent_post(true, '', true); //前の記事
$nextpost = get_adjacent_post(true, '', false); //次の記事
if( $prevpost or $nextpost ){ //前の記事、次の記事いずれか存在しているとき
?>

								
								<div class="article_archives_fPager">
									
<?php
 if ( $prevpost ) { //前の記事が存在しているとき
  echo '<div class="article_archives_fpItem">
  										<a href="' . get_permalink($prevpost->ID) . '">
											<div class="aafp_img">' . get_the_post_thumbnail($prevpost->ID, 'thumbnail') . '</div>
											<div class="aafp_inner">
												<div class="aafp_inner_date">' . get_the_date($prev_post->ID) . '</div>
												<div class="aafp_inner_ttl">' . get_the_title($prevpost->ID) . '</div>
											</div>
										</a>
									</div>';
 } else { //前の記事が存在しないとき
  echo '<div class="alignleft nopost"><a href="' . network_site_url('/') . '">TOPへ戻る</a></div>';
 }
 if ( $nextpost ) { //次の記事が存在しているとき
  echo '<div class="article_archives_fpItem">
										<a href="' . get_permalink($nextpost->ID) . '">
											<div class="aafp_img">' . get_the_post_thumbnail($nextpost->ID, 'thumbnail') . '</div>
											<div class="aafp_inner">
												<div class="aafp_inner_date">' . get_the_date($nextpost->TIME) . '</div>
												<div class="aafp_inner_ttl">' . get_the_title($nextpost->ID) . '</div>
											</div>
										</a>
									</div>
								</div>';
 } else { //次の記事が存在しないとき
  echo '<div class="alignright nopost"><a href="' . network_site_url('/') . '">TOPへ戻る</a></div>';
 }
?>

<?php } ?>



				
		</div>
	</article><!-- #post -->
	
