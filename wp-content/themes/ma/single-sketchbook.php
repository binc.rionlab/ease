<?php
/*
Template Name: free
Template Post Type: post,news
*/
get_header('single'); ?>
    <?php
if (is_preview()) {
  // プレビュー時に使用するテンプレートを指定
get_template_part('inc/preview-nav');
} else {
  // 通常時
get_template_part('inc/category-nav');
}
 ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});

  </script>
<script type="text/javascript">
<!--
$(function() {
  $(".tab2__item2").click(function() {
    // 非表示に設定
    $(".tab2-body__item--1").css("display", "none");
  });
  $(".tab2__item2").click(function() {
    // 非表示に設定
    $("canvas").css("display", "none");
  });
  $(".tab2__item2").click(function() {
    // 表示に設定
    $(".tab2-body__item--2").css("display", "block");
  });
  $(".tab2__item1").click(function() {
    // 非表示に設定
    $(".tab2-body__item--2").css("display", "none");
  });
  $(".tab2__item1").click(function() {
    // 表示に設定
    $(".tab2-body__item--1").css("display", "block");
  });
  $(".tab2__item1").click(function() {
    // 表示に設定
    $("canvas").css("display", "block");
  });
});
//-->
</script>
<section class="section--scrolldown" id="scrolldown" style="display: none;"><a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;"><span></span>スクロールしてください</a></section>
<div id="content-container" style="display: block;">
<section id="header-title" style="margin-bottom: 0px;">
    <?php
if (is_preview()) {
  // プレビュー時に使用するテンプレートを指定
echo '<h2 id="header_title">Preview SketchBook</h2>';
} else {
  // 通常時
echo '<h2 id="header_title">SketchBook</h2>';
}
 ?>
</section>



<?php while ( have_posts() ) : the_post(); ?>
<module-tool-bar class="py8">
    <div class="wrap-module f flex-between"> 
        <div class="wrap-title f fh ml16"> <p><?php the_title(); ?></p> </div> 
        <div class="wrap-icon mr12 f flex-between"> 
            <div class="tab2__item1 on"><div class="wrap-item tab2__link" data-tab-body="1"> <div class="wrap-icon f fh"><img id="playIcon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/icon-play.svg"></div> </div></div> 
            <div class="tab2__item2"><div class="wrap-item tab2__link" data-tab-body="2"> <div class="wrap-icon f fh"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/icon-code.svg"></div> </div></div> 
            <div id="single_camera" class="tab2__item3"><div class="wrap-item tab2__link" data-tab-body="3"> <div class="wrap-icon f fh"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/file_download_black.svg"></div> </div></div> 
        </div>
    </div>
</module-tool-bar>
					<div class="tab2-body">
<section class="demo--wrapper skechbook_user tab2-body__item tab2-body__item--1 on">
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.9.0/p5.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.9.0/addons/p5.dom.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.9.0/addons/p5.sound.min.js"></script>
<script>
<?php remove_filter ('the_content', 'wpautop'); ?>
<?php add_filter ('run_wptexturize', '__return_false'); ?>
<?php 
the_content(); 
?>
document.getElementById('single_camera').addEventListener('click', () => {
  const id_canvas = document.getElementById('defaultCanvas0')
  if(id_canvas) {
    const base64_canvas = id_canvas.toDataURL("image/jpeg", 0.5)
    localStorage.setItem('base64_canvas', base64_canvas)
    const downloadLink = document.createElement("a");
     downloadLink.href = base64_canvas;
     downloadLink.download = `${new Date()}.jpeg`;
     downloadLink.click();
  }
})
</script>

</section>

<section class="code_wrap tab2-body__item tab2-body__item--2 ">
<pre><code class="js"><?php add_filter ('the_content', 'wpautop'); ?><?php echo nl2br ( get_the_content() ); ?></code></pre>
</section>



<div class="clearfix"><h3 class="archive-title"><?php printf( __( 'Author: %s', 'twentytwelve' ), '<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a>' ); ?></h3></div>



</div>
<?php endwhile; // end of the loop. ?>
<?php get_footer('free'); ?>
