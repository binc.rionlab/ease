<!--
				<a class="menu-trigger menu-btn" id="js__btn">
			  		<div class="menu-trigger-inner">
					 	<span></span>
						<span></span>
						<span></span>
					 </div>
				</a>
-->
				<!-- モーダルメニュー -->
				<nav class="modal_menu" id="js__nav">
					<div class="modal_menu_inner">
						<ul>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>about/">
								<li>ABOUT</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>news/">
								<li>NEWS</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>school/">
								<li>SCHOOL</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>education/">
								<li>LEARNING</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>interview/">
								<li>INTERVIEW</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>column/">
								<li>COLUMN</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>gallery/">
								<li>GALLERY</li>
							</a>

							
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>contact/">
								<li>Contact</li>
							</a>
						</ul>
					</div>
				</nav>
				<header class="header-top inviewzoomIn">

					<div class="header_left">
						<h1 class="header_logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/image/top_logo.png">
							</a>
						</h1>
						<nav class="header_nav">
							<ul>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>about/">
										ABOUT
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>news/">
										NEWS
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>school/">
										SCHOOL
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>education/">
										LEARNING
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>interview/">
										INTERVIEW
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>column/">
										COLUMN
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>gallery/">
										GALLERY
									</a>
								</li>s

							</ul>
						</nav>
					</div>

				</header>





				<div class="fix-menu ">
					<header class="header_flt">
						<div class="header_left_flt inviewfadeInUp">
							<h1 class="header_logo_flt">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/image/top_logo.png">
								</a>
							</h1>
							<nav class="header_nav_flt">
							<ul>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>about/">
										ABOUT
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>news/">
										NEWS
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>school/">
										SCHOOL
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>education/">
										LEARNING
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>interview/">
										INTERVIEW
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>column/">
										COLUMN
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>gallery/">
										GALLERY
									</a>
								</li>

							</ul>
							</nav>
						</div>

					</header>
				</div>
<?php 
show_login_menu();
?>