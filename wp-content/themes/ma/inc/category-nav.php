<style>
   @media (min-width:993px) {
    .brand-logo{
      left: 80px;
    } 
   } 
 </style>
<script>
	 if(window.location.href=="https://ch.easelart.io/01"){
		 location.replace("https://ch.easelart.io/0-1");
	 }
</script>
<div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a class="brand-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
          <h1><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_w.svg" alt="easel"></h1>
        </a><a class="sidenav-trigger show-on-large" href="#" data-target="mobile-demo"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
          <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>">主页</a></li>
          <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>01/">学习</a></li>
          <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>02/">模仿</a></li>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register?free">有料会員登録</a></li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login?swpm-logout=true">退出账号</a></li>
			<?php else : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login?swpm-logout=true">退出账号</a></li>
			<?php endif; ?>
<?php else : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>login">ログイン</a></li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register">会員登録</a></li>
<?php endif; ?>
        </ul>
      </div>
    </nav>
  </div>
  <ul class="sidenav" id="mobile-demo">
    <li>
      <div class="user-view">
        <div class="background"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/mainimage.png" style="height: 300px;" /></div><a href="http://inertiaart.io/"><span class="white-text name" style="font-family: DINNextLTPro-Bold,sans-serif; font-size:23px;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_w.svg" alt="easel"></span></a><span class="white-text email" style="font-family: DINNextLTPro-Bold,sans-serif; mix-blend-mode: soft-light;">COMPLETE VERSION</span></div>
    </li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>"><i class="material-icons">home</i>主页</a></li>
    <li>
      <div class="divider"></div>
    </li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>01/"><i class="material-icons">tag_faces</i>学习</a></li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>02/"><i class="material-icons">brush</i>模仿</a></li>
    <li>
      <div class="divider"></div>
    </li>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login"><i class="material-icons">account_box</i>个人页面</a></li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register?free"><i class="material-icons">shop</i>有料会員登録</a></li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login?swpm-logout=true"><i class="material-icons">exit_to_app</i>退出账号</a></li>
			<?php else : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>mysketchbook"><i class="material-icons">import_contacts</i>SketchBook</a></li>
    <li>
      <div class="divider"></div>
    </li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login"><i class="material-icons">account_box</i>个人页面</a></li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login?swpm-logout=true"><i class="material-icons">exit_to_app</i>退出账号</a></li>
			<?php endif; ?>
<?php else : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>login"><i class="material-icons">input</i>ログイン</a></li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register"><i class="material-icons">touch_app</i>会員登録</a></li>
<!--		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register"><i class="material-icons">shop</i>有料会員登録</a></li>-->
<?php endif; ?>
    <li>
      <div class="divider"></div>
    </li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>faq/"><i class="material-icons">question_answer</i>常见问题</a></li>
    <li>
      <div class="divider"></div>
    </li>
    <li><a class="waves-effect" href="https://lp.ch.easelart.io/"><i class="material-icons">launch</i>关于easel</a></li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>contact/"><i class="material-icons">contact_mail</i>问询</a></li>
  </ul>