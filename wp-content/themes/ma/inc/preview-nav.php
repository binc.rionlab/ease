 <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a class="brand-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
          <h1><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_w.svg" alt="easel"></h1>
        </a><a class="sidenav-trigger" href="#" data-target="mobile-demo"><i class="material-icons">menu</i></a>

      </div>
    </nav>
  </div>
