				<a class="menu-trigger menu-btn" id="js__btn">
			  		<div class="menu-trigger-inner">
					 	<span></span>
						<span></span>
						<span></span>
					 </div>
				</a>
				<!-- モーダルメニュー -->
				<nav class="modal_menu" id="js__nav">
					<div class="modal_menu_inner">
						<ul>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>background/">
								<li>Background</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>award/">
								<li>MADD. Award</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>entry/">
								<li>Entry</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>judges/">
								<li>Judges</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>schedule/">
								<li>Schedule</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>maddbook/">
								<li>MADD. book</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>news/">
								<li>News</li>
							</a>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>winningentries/">
								<li>Winning entries</li>
							</a>
						</ul>
					</div>
				</nav>

				<header class="header inviewzoomIn">
					<div class="header_left">
						<h1 class="header_logo">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/image/logo.svg">
							</a>
						</h1>
						<nav class="header_nav">
							<ul>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>background/">
										Background
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>award/">
										MADD. Award
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>entry/">
										Entry
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>judges/">
										Judges
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>schedule/">
										Schedule
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>maddbook/">
										MADD. book
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>news/">
										News
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>winningentries/">
										Winning entries
									</a>
								</li>
							</ul>
						</nav>
					</div>

				</header>





				<div class="fix-menu">
					<header class="header_flt">
						<div class="header_left_flt">
							<h1 class="header_logo_flt">
								<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/image/logo.svg">
								</a>
							</h1>
							<nav class="header_nav_flt">
							<ul>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>background/">
										Background
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>award/">
										MADD. Award
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>entry/">
										Entry
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>judges/">
										Judges
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>schedule/">
										Schedule
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>maddbook/">
										MADD. book
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>news/">
										News
									</a>
								</li>
								<li>
									<a href="<?php echo esc_url( home_url( '/' ) ); ?>winningentries/">
										Winning entries
									</a>
								</li>
							</ul>
							</nav>
						</div>

					</header>
				</div>