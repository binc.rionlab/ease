$(function () {
  var $grid = $('#box-container').masonry({
    columnWidth: '.box-size1',
    itemSelector: '.item',
  });

  Pace.on('done', function(){
    // layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
      $grid.masonry('layout');
    });

  });

});