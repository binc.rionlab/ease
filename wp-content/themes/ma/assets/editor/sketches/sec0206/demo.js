function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234); 
  noFill();
  strokeWeight(5);
}

function draw() {
  ellipse(80, 80, 160);
  ellipse(120, 120, 240);
  ellipse(160, 160, 320);
  ellipse(200, 200, 400);
  ellipse(240, 240, 480);
}