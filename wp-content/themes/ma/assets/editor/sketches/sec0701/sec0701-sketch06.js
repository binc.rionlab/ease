var x;

function setup() {
  createCanvas(300, 300);
  noStroke();
  x = 0;
}

function draw() {
  background(238, 238, 238, 10);
  
  x = x + 5;
  
  if (x > 300) {
    x = 0;
  }

  fill(0, 200, x);
  ellipse(x, 150, 50);
}