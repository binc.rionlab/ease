var diameter;

function setup() {
  createCanvas(200, 200);
  strokeWeight(3);
}

function draw() {
  background(250, 250, 250);

  diameter = second();

  fill(255, 255, 255);

  if (diameter > 50) {  
    
  }

  ellipse(100, 100, diameter + 50);

  fill(0, 0, 0);
  textSize(30);
  text(second(), 10, 30);
}