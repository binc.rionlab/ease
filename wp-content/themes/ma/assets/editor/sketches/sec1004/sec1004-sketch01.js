var blueHeight;

function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  strokeWeight(5);
  noFill();

  stroke(250, 250, 20);
  rect(0, 0, 250, 150);
}