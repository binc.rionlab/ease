var num;
var values = [];

var time;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  angleMode(DEGREES);
  noFill();

  num = 50;
  for (var i = 0; i < num; i = i + 1) {
    values[i] = 0.0;
  }
  
  time = 0.0;
}

function draw() {
  background(234, 234, 234);
  
  for (var i = 0; i < num; i = i + 1) {
    x = 360 / (num - 1) * i + time;
    values[i] = windowWidth / 3 * sin(x);
    var ellipseX = windowWidth / 2 + values[i];
    var ellipseY = windowHeight / (num - 1) * i;

    stroke(0, 160, 250);
    ellipse(ellipseX, ellipseY - 20, 25);

    stroke(240, 35, 95);
    ellipse(mirrorX(ellipseX), ellipseY + 70, 25);

    stroke(150, 150, 150);
    line(ellipseX, ellipseY - 20,
      mirrorX(ellipseX), ellipseY + 70);
  }

  time = time + 1;
}

function mirrorX(x) {
  var mx;
  mx = windowWidth - x;
  return mx;
}