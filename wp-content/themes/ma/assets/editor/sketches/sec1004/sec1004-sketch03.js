var mirrorX;
var pmirrorX;

function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238, 10);

  noFill();
  
  stroke(0, 160, 250);
  ellipse(mouseX, mouseY, 50);
  
  stroke(240, 35, 95);
  mirrorX = mirror(mouseX);
  ellipse(mirrorX, mouseY, 50);
}

function mirror(x) {
  var mx;
  mx = 300 - x;
  return mx;
}