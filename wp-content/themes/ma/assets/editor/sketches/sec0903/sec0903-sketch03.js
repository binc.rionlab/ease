var pointX = [];
var pointY = [];
var speedX = [];
var speedY = [];

function setup() {
  createCanvas(300, 300);
  angleMode(DEGREES);

  for (var i = 0; i < 3000; i = i + 1) {
    pointX[i] = random(0, 300);
    pointY[i] = random(0, 300);
    speedX[i] = random(-1, 1);
    speedY[i] = random(-1, 1);
  }
}

function draw() {
  background(238, 238, 238, 50);

  for (var i = 0; i < 3000; i = i + 1) {
    pointX[i] = pointX[i] + speedX[i];
    pointY[i] = pointY[i] + speedY[i];

    if (pointX[i] > 300) {
      pointX[i] = 0;
    }

    if (pointX[i] < 0) {
      pointX[i] = 300;
    }

    if (pointY[i] > 300) {
      pointY[i] = 0;
    }

    if (pointY[i] < 0) {
      pointY[i] = 300;
    }
  }

  var R = (1 + sin(frameCount%360)) / 2;
  stroke(255 * R, 128, 255);
  for (var i = 0; i < 3000; i = i + 1) {
    point(pointX[i], pointY[i]);
  }

}