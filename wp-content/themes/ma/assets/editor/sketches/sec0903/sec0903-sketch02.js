var diameter = [];

function setup() {
  createCanvas(300, 300);

  for (var i = 0; i < 20; i = i + 1) {
    diameter[i] = 10 * i;
  }
}

function draw() {
  background(250, 250, 250);
  noFill();

  for (var i = 0; i < 20; i = i + 1) {
    diameter[i] = diameter[i] + 1;
    if (diameter[i] > 300) {
      diameter[i] = 0;
    }
  }

  for (var i = 0; i < 20; i = i + 1) {
    ellipse(150, 150, diameter[i]);
  }
}