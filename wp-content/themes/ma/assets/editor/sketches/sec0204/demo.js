function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  noStroke();

  //adding value to red and green.
  fill(frameCount, 128 + frameCount, 128, 2);

  //adding value to the x coordinate of the left top point.
  rect(frameCount, 50, 200, 250);

}
