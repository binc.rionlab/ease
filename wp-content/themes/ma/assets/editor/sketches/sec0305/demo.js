function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  var from = color(0, 128, 255);
  var to = color(255, 100, 150);

  noStroke();

  for (var i = 0.0; i < width; i = i + 20.0) {
    stroke(lerpColor(from, to, i / width));
    strokeWeight(i / 25);
    line(i, 0, i, height);
  }
}