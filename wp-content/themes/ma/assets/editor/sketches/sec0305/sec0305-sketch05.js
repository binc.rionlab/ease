function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);

  stroke(0, 200, 255);
  strokeWeight(3);
  rect(20, 20, 260, 260);
  
  stroke(40, 185, 235);
  strokeWeight(5);
  rect(40, 40, 220, 220);
  
  stroke(85, 165, 220);
  strokeWeight(7);
  rect(60, 60, 180, 180);
  
  stroke(128, 150, 200);
  strokeWeight(9);
  rect(80, 80, 140, 140);
  
  stroke(170, 135, 185);
  strokeWeight(11);
  rect(100, 100, 100, 100);
  
  stroke(210, 115, 168);
  strokeWeight(13);
  rect(120, 120, 60, 60);
  
  stroke(255, 100, 150);
  strokeWeight(15);
  rect(140, 140, 20, 20);
}