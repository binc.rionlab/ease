function setup() {
  createCanvas(windowWidth,
               windowHeight);
}

function draw() {
  var from = color(0, 128, 255);
  var to = color(255, 100, 150);

  noStroke();

  var tmp = frameCount % 360
  background(lerpColor(from, to, sin(tmp * PI / 180)));
}