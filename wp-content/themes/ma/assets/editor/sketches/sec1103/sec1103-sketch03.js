function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
}

function draw() {
  background(238, 238, 238);
 
  textSize(40);
  stroke(0, 200, 255);
  strokeWeight(5);
  noFill();
  text("Hello World", 30, 150);
}