function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);
  noStroke();
  
  for (var i = 0; i < 10000; i = i + 1) {
    var x = random(0, 300);
    var y = random(0, 300);
    var R = 250 - 250 * y / 300;
    var G = 250 * x / 300;
    var B = 95 + 35 * y / 300;
    fill(R, G, B);
    ellipse(x, y, 15);
  }
}

function draw() {

}