function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  noStroke();
 
  fill(85, 140, 200);
  rect(mouseX, mouseY,
       100, 200);

  fill(255, 185, 35);
  rect(mouseX + 100, mouseY,
       100, 200);
}