var centerXa;
var centerXb;

function setup() {
  createCanvas(300, 300);

  centerXa = 150;
  centerXb = 150;
}

function draw() {
  background(250, 250, 250);
  noStroke();
 
  fill(85, 140, 200);
  ellipse(centerXa, 150, 120);
 
  fill(255, 185, 35);  
  ellipse(centerXb, 150, 60);
}