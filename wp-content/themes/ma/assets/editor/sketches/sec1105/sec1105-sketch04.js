var radius;  
var diameter;  

function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
 
  radius = dist(150, 150, mouseX, mouseY);

  noStroke();
  fill(10, 200, 250);
  ellipse(150, 150, radius * 2);
 
  textSize(21);
  fill(0, 0, 0);
  diameter = radius * 2;  
    
}