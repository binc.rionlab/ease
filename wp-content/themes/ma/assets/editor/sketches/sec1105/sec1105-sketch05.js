var d;

function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  
  d = dist(150, 150, mouseX, mouseY);

  noFill();
  if (d < 75) {
    fill(0, 200, 255);      
  }
  
  ellipse(150, 150, 150);
  line(150, 150, mouseX, mouseY);
  
  fill(0, 0, 0);
  text(floor(d), 20, 30);
}