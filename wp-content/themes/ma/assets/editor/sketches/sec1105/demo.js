var values = [];
var time;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  time = 0;
}

function draw() {
  background(234, 234, 234, 200);

  var numX = 10;

  for (var i = 0; i < numX; i = i + 1) {
    values[i] = noise(i * 0.05, time);
  }

  noStroke();
  for (var i = 0; i < numX; i = i + 1) {
    values[i] = map(values[i], 0, 1, -1, 1);

    var ratio = abs(values[i]);
    var R = (1.0 - ratio) * 240;
    var G = 250;
    var B = 95 + 155 * ratio;
    values[i] = windowHeight * values[i];

    var x = i * windowWidth / numX;
    var y = windowHeight / 2 - values[i];
    var w = windowWidth / numX;
    var h = values[i];

    fill(R, G, B);
    rect(x, y, w, h);

    fill(0, 0, 0);
    text(int(h), x, y);
  }

  time = time + 0.003;
}