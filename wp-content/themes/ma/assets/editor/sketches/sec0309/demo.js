function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  var from = color(0, 200, 255, 40);
  var to = color(255, 100, 150, 40);

  fill(lerpColor(from, to, random(1.0)));
  rect(random(width), random(height), 50, 50);
}