var sound;

var startButton;
var pauseButton;
var isPlaying;
var muted;

function preload() {  
  sound = loadSound("sound2.mp3")
}

function setup() {
  createCanvas(300, 300);

  startButton = createButton("Press to start sound");
  startButton.position(70, 100);
  startButton.mousePressed(start);
  
  pauseButton = createButton("Pause");
  pauseButton.position(70, 130);
  pauseButton.mousePressed(pause);
  
  isPlaying = false;
  muted = false;
}

function draw() {

}

function start() {
  if (isPlaying == false) {
    sound.loop();
    isPlaying = true;
  }

  if (muted) {
    sound.setVolume(1.0);
    muted = false;
  }
}

function pause() {
  muted = !muted;
  if (muted) {
    sound.setVolume(0.0);
  } else {
    sound.setVolume(1.0);
  }
}

// 
// 声音数据
// 
// 心脏的跳动、紧张 
// written by 123理论
// https://dova-s.jp/se/play868.html
// 