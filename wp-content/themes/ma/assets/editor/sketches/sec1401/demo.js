var sound;
var amplitude;
var X;
var Y;
var prevX;
var prevY;

var startButton;
var pauseButton;
var initialized;
var muted;

function preload() {
  sound = loadSound("sounds/heart_beat.mp3");
}

function start() {
  if (initialized == false) {
    sound.loop();
    initialized = true;
  }

  if (muted) {
    sound.setVolume(1.0);
    muted = false;
  }
}

function pause() {
  muted = !muted;
  if (muted) {
    sound.setVolume(0.0);
  } else {
    sound.setVolume(1.0);
  }
}

function setup() {
  createCanvas(300, 300);
  background(234, 234, 234);

  startButton = createButton("Press to start sound");
  startButton.position(70, 40);
  startButton.mousePressed(start);

  pauseButton = createButton("Pause");
  pauseButton.position(70, 70);
  pauseButton.mousePressed(pause);

  amplitude = new p5.Amplitude();

  X = 0;
  Y = height - 30;

  prevX = X;
  prevY = Y;

  initialized = false;
  muted = false;
}

function draw() {
  var level = amplitude.getLevel();

  X = X + 2;
  Y = height - 30 - map(level, 0, 1, 0, height);

  strokeWeight(3);
  stroke(40, 40, 40);
  line(prevX, prevY, X, Y);

  prevX = X;
  prevY = Y;

  if (prevX > width) {
    background(234, 234, 234);
    X = 0;
    prevX = 0;
  }
}

//
// 声音数据
//
// 心脏的跳动、紧张
// written by 123理论
// https://dova-s.jp/se/play868.html
//
