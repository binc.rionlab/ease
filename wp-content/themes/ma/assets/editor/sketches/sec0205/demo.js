
function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  noStroke();

  //adding value to red and green.
  fill(frameCount, 128 + frameCount, 128, 10);

  //adding value to the radius of the circle.
  ellipse(150, 80, frameCount);
}