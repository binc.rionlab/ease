function setup() {
  createCanvas(windowWidth,
               windowHeight);
}

function draw() {
  background(234, 234, 234);
  strokeWeight(10);
  point(1 / 4 * width, 1 / 6 * height);

  strokeWeight(2);
  line(1 / 4 * width, 1 / 6 * height, 3 / 4 * width, 2 / 6 * height);
  triangle(1 / 4 * width, 3 / 6 * height,
    3 / 4 * width, 3 / 6 * height,
    3 / 4 * width, 2 / 6 * height);
  rect(1 / 4 * width, 3 / 6 * height, width / 2, height / 6);
  ellipse(width / 2, 5 / 6 * height, height / 3.03);
}