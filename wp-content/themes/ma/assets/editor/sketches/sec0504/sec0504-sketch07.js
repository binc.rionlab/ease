function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);
}

function draw() {
  noFill();  
  rect(0, 0, 
       300 / frameCount, 
       300 / frameCount);
}