var heightA;
var heightB;

function setup() {
  createCanvas(300, 300);
 
  heightA = 260;
  heightB = 260;
}

function draw() {
  background(250, 250, 250);
  noStroke();
 
  fill(220, 210, 0);
  rect(20, 20, 120, heightA)

  fill(180, 240, 220);
  rect(160, 20, 120, heightB);
}