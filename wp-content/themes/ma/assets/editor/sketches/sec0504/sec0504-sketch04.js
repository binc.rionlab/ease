var heightA;
var heightB;

function setup() {
  createCanvas(300, 300);  
}

function draw() {
  background(238, 238, 238);
  noStroke();
 
  heightA = frameCount;
  heightB = heightA / 2;
  
  fill(220, 210, 0);
  rect(20, 20, 120, heightA)

  fill(180, 240, 220);
  rect(160, 20, 120, heightB);
}