var a = 1.4;
var b = 1.2;
var c = 1.0;
var d = 1.0;

var x = 1.0;
var y = 1.0;
var xn = 1.0;
var yn = 1.0;

var accA = 0.0;
var accB = 0.0;
var accC = 0.001;
var accD = 0.001;

var values = [];

var num = 1000;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);

  for (var i = 0; i < num; i++) {
    values.push(new p5.Vector(1, 1));
  }

}

function draw() {
  background(234, 234, 234, 30);

  fill(255, 0, 0, 20);
  noStroke();

  a = a + accA;
  b = b + accB;
  c = c + accC;
  d = d + accD;

  for (var i = 0; i < num; i++) {
    xn = sin(b * x) + d * sin(b * y);
    yn = cos(x);
    values[i].x = xn;
    values[i].y = yn;

    x = xn;
    y = yn;
  }

  translate(width / 2, height / 2);
  for (var i = 0; i < num; i++) {
    ellipse(values[i].x * 50, values[i].y * 50, 4, 4);
  }
}