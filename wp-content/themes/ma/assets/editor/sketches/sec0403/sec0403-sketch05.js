function setup() {
  createCanvas(300, 300);
  strokeWeight(3);
  noFill();
}

function draw() {
  background(238, 238, 238);
 
  rect(0, 0, mouseX, mouseY);
  rect(mouseX, mouseY, mouseX, mouseY);
}