var tapRight;

function setup() {
  createCanvas(300, 300);
  noStroke();
}

function draw() {
  background(238, 238, 238);
  if (mouseX > 150) {
    tapRight = true;
  }

  if (mouseX < 150) {
    tapRight = false;
  }

  if (tapRight == true) {
    fill(255, 0, 150);
    rect(150, 0, 150, 300);
  }

  if (tapRight == false) {
    fill(0, 200, 255);
    rect(0, 0, 150, 300);
  }
}