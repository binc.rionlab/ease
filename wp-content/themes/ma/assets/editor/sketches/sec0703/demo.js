var cells = [];
var numX;
var numY;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  stroke(0, 0, 0);

  numX = 50;
  numY = 50;
  
  for (var i = 0; i < numX; i++) {
    cells[i] = [];
    for (var j = 0; j < numY; j++) {
      cells[i][j] = floor(random() * 2);
    }
  }

  drawCells();
}

function draw() {
  rule();
  drawCells();
}

function drawCells() {
  var sizeX = width / numX;
  var sizeY = height / numY;
  for (var i = 0; i < numX; i++) {
    for (var j = 0; j < numY; j++) {
      if (cells[i][j] == 1) fill(40, 40, 40);
      if (cells[i][j] == 0) fill(234, 234, 234);
      rect(i * sizeX, j * sizeY, sizeX, sizeY);
    }
  }
}

function rule() {
  var temp = [];

  for (var i = 0; i < numX; i++) {
    temp[i] = [];
    for (var j = 0; j < numX; j++) {
      temp[i][j] = 0;
    }
  }

  for (var i = 1; i < numX - 1; i++) {
    for (var j = 1; j < numY - 1; j++) {
      var neighbors = 0;

      if (cells[i - 1][j - 1] == 1) neighbors++;
      if (cells[i][j - 1] == 1) neighbors++;
      if (cells[i + 1][j - 1] == 1) neighbors++;
      if (cells[i - 1][j] == 1) neighbors++;
      if (cells[i + 1][j] == 1) neighbors++;
      if (cells[i - 1][j + 1] == 1) neighbors++;
      if (cells[i][j + 1] == 1) neighbors++;
      if (cells[i + 1][j + 1] == 1) neighbors++;

      if (cells[i][j] == 1) {
        if (neighbors == 0 || neighbors == 1) temp[i][j] = 0;
        if (neighbors == 2 || neighbors == 3) temp[i][j] = 1;
        if (neighbors >= 4) temp[i][j] = 0;
      }

      if (cells[i][j] == 0) {
        if (neighbors == 3) temp[i][j] = 1;
      }

    }
  }

  for (var i = 0; i < numX; i++) {
    for (var j = 0; j < numY; j++) {
      cells[i][j] = temp[i][j];
    }
  }

}