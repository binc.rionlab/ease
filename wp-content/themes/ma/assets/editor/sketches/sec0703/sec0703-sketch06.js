var d;

function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);
  noFill();
  d = 0;
}

function draw() { 
  if (second() % 2 == 0) {
    stroke(255, 100, d / 2);
    ellipse(150, 150, d);
  }
  
  d = d + 0.5;
}