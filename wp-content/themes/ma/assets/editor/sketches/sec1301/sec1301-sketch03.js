var button;
var eraser;

function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
 
  randomEllipse();
 
  button = createButton("draw");  
  button.position(100, 40);  
  button.mousePressed(randomEllipse);  

  eraser = createButton("clear");  
  
}

function randomEllipse() {
  fill(0, 200, 255, 128);
  ellipse(random(0, 300), random(0, 300), random(0, 300));
}

function clearCanvas() {
  background(250, 250, 250);
}

function draw() {

}