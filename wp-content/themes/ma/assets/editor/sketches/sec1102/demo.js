var word = 'LOVE';
var colors = [];

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  fill(0, 0, 0);
  
  var N = word.length;
  var prev = 0;
  var j = 0;

  while (1) {
    var char_num = N * (j + 1);
    var char_size = windowWidth / char_num;
    textSize(char_size * 1.35);
    for (var i = 0; i < char_num; i++) {
      var textX = i * char_size;
      var textY = prev + char_size;
      text(word.charAt(i % N), textX, textY);
    }
    prev += char_size + 1.0;
    if (prev > windowHeight) {
      break;
    }
    j = j + 1;
  }
}

function draw() {
  
}