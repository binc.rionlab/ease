var greeting;

function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
  greeting = "你好";
}

function draw() {
  background(250, 250, 250);
  text(greeting, 60, 100);
}