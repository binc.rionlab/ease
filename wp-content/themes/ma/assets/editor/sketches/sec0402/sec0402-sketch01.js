var centerX;
var centerY;

function setup() {
  createCanvas(300, 300);
  strokeWeight(3);
}
  
function draw() {
  background(250, 250, 250);
  
  centerX = 140;
  centerY = 120;
  
  ellipse(centerX, centerY, 50);
}