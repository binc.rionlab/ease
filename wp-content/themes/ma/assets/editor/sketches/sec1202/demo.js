var positionX = [];
var positionY = [];
var speedX = [];
var speedY = [];
var carpImg = [];
var z = [];

function preload() {

  for (var i = 0; i < 150; i = i + 1) {
    carpImg[i] = loadImage("images/carp" + int(random(0, 5)) + ".png");
    positionX[i] = random(0, windowWidth);
    positionY[i] = random(0, windowHeight);
    speedX[i] = random(-0.1, 0.1);
    speedY[i] = random(-0.1, -1);
    z[i] = random(0.3, 1);
  }
}

function setup() {
  createCanvas(windowWidth,
               windowHeight);
}

function draw() {
  background(234, 234, 234, 200);

  for (var i = 0; i < 150; i = i + 1) {
    positionX[i] = positionX[i] + speedX[i];
    positionY[i] = positionY[i] + speedY[i];
    tint(255, z[i] * 255);
    image(carpImg[i], positionX[i], positionY[i],
          z[i] * carpImg[i].width, z[i] * carpImg[i].height);
    if (positionX[i] < -100) positionX[i] = windowWidth;
    if (positionX[i] > windowWidth + 100) positionX[i] = 0;
    if (positionY[i] < -200) positionY[i] = windowHeight;
    if (positionY[i] > windowHeight) positionY[i] = 0;
  }
}