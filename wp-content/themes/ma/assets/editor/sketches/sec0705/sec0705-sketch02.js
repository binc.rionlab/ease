var rectX;

function setup() {
  createCanvas(200, 200);
  strokeWeight(3);
}

function draw() {
  background(250, 250, 250);

  rectX = second();

  fill(255, 255, 255);

  if (rectX < 20) {
    fill(255, 0, 128);
  }
  else if (rectX < 40) {
    fill(128, 255, 0);
  }

  rect(rectX, 60, 80, 80);

  fill(0, 0, 0);
  textSize(30);
  text(second(), 10, 30);
}