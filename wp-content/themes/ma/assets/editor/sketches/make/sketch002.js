var X = 150;
var time;
var param = [];

function setup() {
  createCanvas(X * 2, X * 2);
  fill(255, 255, 255);
  stroke(0, 0, 0);
  time = 0;
}

function draw() {
  rect(0, 0, width - 1, height - 1);
  
  if (time < 50) {
    param[0] = random(1, 16); 
    param[1] = random(1, 16); 
    param[2] = random(1, 16); 
    param[3] = random(1, 16); 
  } else if (time > 200) {
    time = 0;
  }

  lines(param[0], 0, 0);
  lines(param[1], X, 0);
  lines(param[2], 0, X);
  lines(param[3], X, X);

  time = time + 1;
}

function lines(mode, x, y) {
  var num = 10;
  var span = X / num;

  for (var i = 0; i <= num; i++) {
    if (mode & 0b0001) {
      line(x + span * i, y, x + span * i, y + X);
    }

    if (mode & 0b0010) {
      line(x, y + span * i, x + X, y + span * i);
    }

    if (mode & 0b0100) {
      line(x + span * i, y, x, y + span * i);
      line(x + span * i, y + X, x + X, y + span * i);
    }

    if (mode & 0b1000) {
      line(x + span * i, y, x + X, y - span * i + X);
      line(x, y - span * i + X, x + span * i, y + X);
    }
  }
}