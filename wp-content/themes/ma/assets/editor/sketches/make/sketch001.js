var N = 3;
var SIZE = 300;

var rects = [];
var colors = [];
var flag = 0;

function setup() {
  createCanvas(SIZE, SIZE);

  flag = 0;
  colors[flag] = [];
  colors[flag][0] = color(223, 103, 138);
  colors[flag][1] = color(198, 73, 117);
  colors[flag][2] = color(240, 130, 139);
  colors[flag][3] = color(239, 164, 185);

  flag = 1;
  colors[flag] = [];
  colors[flag][0] = color(0, 158, 216);
  colors[flag][1] = color(0, 120, 170);
  colors[flag][2] = color(59, 177, 205);
  colors[flag][3] = color(64, 200, 206);

  rects = [];
  for (var i = 0; i < N; i++) {
    rects[i] = SIZE - i * (SIZE / (N - 1));
  }
}

function draw() {
  for (var i = 0; i < rects.length; i++) {
    rects[i]++;
    if (rects[i] > SIZE + SIZE / (N - 1)) {
      rects.shift();
      rects.push(0);
      flag = 1 - flag;
    }
    draw_rects(SIZE / 2, SIZE / 2, rects[i], colors[(flag + i) % 2]);
  }
}

function draw_rects(x, y, size, colors) {
  var half = size / 2;
  var quarter = size / 4;

  rectMode(CENTER);
  noStroke();

  fill(colors[0]);
  rect(x - quarter, y - quarter, half, half);

  fill(colors[1]);
  rect(x + quarter, y - quarter, half, half);

  fill(colors[2]);
  rect(x - quarter, y + quarter, half, half);

  fill(colors[3]);
  rect(x + quarter, y + quarter, half, half);
}
