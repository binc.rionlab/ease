var num;
var radius;
var posX = [], posY = [];
var velX = [], velY = [];
var colors = [];
var context;

function setup() {
  var canvas = createCanvas(windowWidth,
               windowHeight);
  context = canvas.drawingContext;
  fill(255, 255, 255);

  num = 5;
  radius = windowHeight / 2;
  
  for (var i = 0; i < num; i = i + 1) {
    posX[i] = random(0, windowWidth);
    posY[i] = random(0, windowHeight);
    velX[i] = random(-6, 6);
    velY[i] = random(-6, 6);
  }
    
  colors[0] = '250, 240, 128';
  colors[1] = '250, 140, 40';
  colors[2] = '240, 75, 45';
  colors[3] = '230, 55, 90';
  colors[4] = '230, 55, 90';
  colors[5] = '180, 60, 180';
}

function draw() {
  clear();
  noStroke();
  
  var gradient;
  for (var i = 0; i < num; i = i + 1) {
    gradient = context.createRadialGradient(posX[i], posY[i], 1, posX[i], posY[i], radius);
    gradient.addColorStop(0, `rgba(` + colors[i] + `, 1)`);
    gradient.addColorStop(1, `rgba(` + colors[i + 1] + `, 0)`);
    context.fillStyle = gradient;
    ellipse(posX[i], posY[i], 2 * radius);
  }

  loadPixels();
  var d = pixelDensity();
  var len = 4 * (width * d) * (height * d);
  for (var i = 0; i < len; i = i + 4) {
    if (pixels[i + 3] < 200) {
      pixels[i + 3] = 0;
    }
  }
  updatePixels();

  for (var i = 0; i < num; i = i + 1) {
    posX[i] = posX[i] + velX[i];
    posY[i] = posY[i] + velY[i];
    if (posX[i] > windowWidth || posX[i] < 0) velX[i] = -velX[i];
    if (posY[i] > windowHeight || posY[i] < 0) velY[i] = -velY[i];
  }

}