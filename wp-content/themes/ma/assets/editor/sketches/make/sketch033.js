function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  noFill();
  strokeWeight(5);
}

function draw() {
  ellipse(height * 0.1, height * 0.1, height * 0.2);
  ellipse(height * 0.2, height * 0.2, height * 0.4);
  ellipse(height * 0.3, height * 0.3, height * 0.6);
  ellipse(height * 0.4, height * 0.4, height * 0.8);
  ellipse(height * 0.5, height * 0.5, height * 1.0);
  ellipse(height * 0.6, height * 0.6, height * 1.2);
  ellipse(height * 0.7, height * 0.7, height * 1.4);
  ellipse(height * 0.8, height * 0.8, height * 1.6);
  ellipse(height * 0.9, height * 0.9, height * 1.8);
  ellipse(height * 1.0, height * 1.0, height * 2.0);
}