var img;

function preload() {
  img = loadImage("images/canvas.jpg");
}

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  img.resize(windowWidth,
             windowHeight);
}

function draw() {
  image(img, 0, 0);
  filter(INVERT);

  image(img, 1  / 2 * width, 0);
}

// Unsplash
// https://unsplash.com/photos/JLfem8ViKVA