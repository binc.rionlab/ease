var curPnt, prevPnt;
var p0, p1, p2, p3;

function setup() {
  createCanvas(400, 400);

  curPnt = createVector(0, 0);
  prevPnt = createVector(0, 0);

  p0 = createVector(50, 50);
  p1 = createVector(100, 300);
  p2 = createVector(300, 300);
  p3 = createVector(350, 50);
}

function drawPolygon() {
  line(p0.x, p0.y, p1.x, p1.y);
  line(p1.x, p1.y, p2.x, p2.y);
  line(p2.x, p2.y, p3.x, p3.y);
}

function drawCubicBezier() {
  var w0, w1, w2, w3;

  for (var t = 0.0; t < 1.05; t += 0.05) {
    w0 = (1 - t) * (1 - t) * (1 - t);
    w1 = 3 * (1 - t) * (1 - t) * t;
    w2 = 3 * (1 - t) * t * t;
    w3 = t * t * t;

    curPnt.x = p0.x * w0 + p1.x * w1 + p2.x * w2 + p3.x * w3;
    curPnt.y = p0.y * w0 + p1.y * w1 + p2.y * w2 + p3.y * w3;

    if (t != 0.0) {
      line(curPnt.x, curPnt.y, prevPnt.x, prevPnt.y);
    }

    prevPnt.set(curPnt);
  }
}

function draw() {
  background(255);
  stroke(150);
  drawPolygon();
  stroke(0, 128, 255);
  drawCubicBezier();
}

function mouseDragged() {
  var threshold = 10;

  if (dist(mouseX, mouseY, p0.x, p0.y) < threshold) {
    p0.x = mouseX; p0.y = mouseY;
  }

  if (dist(mouseX, mouseY, p1.x, p1.y) < threshold) {
    p1.x = mouseX; p1.y = mouseY;
  }

  if (dist(mouseX, mouseY, p2.x, p2.y) < threshold) {
    p2.x = mouseX; p2.y = mouseY;
  }

  if (dist(mouseX, mouseY, p3.x, p3.y) < threshold) {
    p3.x = mouseX; p3.y = mouseY;
  }
}