var max_times = 5;
var SIZE = 300;

function setup() {
  createCanvas(SIZE, SIZE);
  noStroke();
  frameRate(2);
}

var times = 0;
var change = 1;

function draw() {
  background(0, 0, 0);

  fill(255, 255, 255);
  triangle(SIZE/2, 0, 0, SIZE, SIZE, SIZE);

  fill(0);
  SierpinskiGasket(SIZE/2, 0, 0, SIZE, SIZE, SIZE, times);

  times += change;

  if (times > max_times || times < 1) {
    change *= -1;
  }
}

function SierpinskiGasket(Ax, Ay, Bx, By, Cx, Cy, times) {
  if(times > 0) {
    SierpinskiGasket(Ax, Ay, (Ax + Bx) * 0.5, (Ay + By) * 0.5, (Ax + Cx) * 0.5, (Ay + Cy) * 0.5, times - 1);
    SierpinskiGasket(Bx, By, (Ax + Bx) * 0.5, (Ay + By) * 0.5, (Bx + Cx) * 0.5, (By + Cy) * 0.5, times - 1);
    SierpinskiGasket(Cx, Cy, (Bx + Cx) * 0.5, (By + Cy) * 0.5, (Ax + Cx) * 0.5, (Ay + Cy) * 0.5, times - 1);
  }
  triangle((Ax + Bx) * 0.5, (Ay + By) * 0.5, (Bx + Cx) * 0.5, (By + Cy) * 0.5, (Cx + Ax) * 0.5, (Cy + Ay) * 0.5);
}