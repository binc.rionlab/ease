var N = 12;
var bump = 3;
var speed = 0.001;
var span = 3;
var SIZE = 300;

function setup() {
  createCanvas(SIZE, SIZE);
  noStroke();
}

var centerX = SIZE / 2;
var centerY = SIZE / 2;
var change_diameter = SIZE / (bump * N);
var change_centerX;

function draw() {
  background(238, 238, 238);
  draw_circles();
}

function draw_circles() {
  var dia = SIZE;
  var flag = 0;

  centerX = SIZE / 2;
  centerY = SIZE / 2;
  change_centerX = span;

  for (var n = 0; n < bump; n++) {
    for (var i = 0; i < N; i++) {
      fill(255 * flag);
      ellipse(centerY + (width / 2 - centerX) * cos(speed * millis()), centerY, dia);
      dia -= change_diameter;
      centerX += change_centerX;
      flag = !flag;
    }
    change_centerX = -change_centerX;
  }
}