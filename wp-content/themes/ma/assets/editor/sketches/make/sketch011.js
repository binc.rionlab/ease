var xn = 2.0;
var yn = 5.0;
var a = 0.000001;
var b = 0.05;
var m = -0.071;
var step = 0.0001;
var N = 10000;

function setup() {
  createCanvas(windowWidth, windowHeight);
  stroke(0, 0, 0, 80);
}

var xn1;
var yn1;

function draw() {
  m += step;

  background(238, 238, 238, 50);

  for (var i = 0; i < N; i++) {
    xn1 = yn + a * (1.0 - (b * yn * yn)) * yn + F(xn);
    yn1 = -xn + F(xn1);

    ellipse(20 * xn1 + width / 2 - 35, 20 * yn1 + height / 2, 1);

    xn = xn1;
    yn = yn1;
  }
}

function F(x) {
  return (m * x) + ((2.0 * (1.0 - m) * x * x) / (1.0 + x * x));
}