function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  strokeWeight(2);

  for (var i = 0; i < 100; i = i + 1) {
    var y = random(0, height);
    var R = 240 - 240 * y / height;
    var G = 35 + 125 * y / height;
    var B = 95 + 155 * y / height;
    stroke(R, G, B);
    line(0, y, width, y);
  }
}

function draw() {

}