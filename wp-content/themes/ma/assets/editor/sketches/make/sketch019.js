var waves = [];
var wave_num;

var time;
var colors = [];

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  angleMode(DEGREES);
  noStroke();

  wave_num = 20;
  for (var i = 0; i < wave_num; i = i + 1) {
    var ratio = i / wave_num;
    var mode = (i % 2) ? 'sin' : 'cos';
    var num = random(10, 40);
    var amp = 10 + ratio * windowHeight / 2;
    var dia = 10 + ratio * 15;
    waves[i] = new Wave(mode, num, amp, dia);
  }

  for (var i = 0; i < wave_num; i = i + 1) {
    waves[i].setup();
  }

  colors.push(color(150, 180, 250));
  colors.push(color(200, 200, 250));
  colors.push(color(255, 160, 225));
  colors.push(color(255, 100, 105));
  colors.push(color(200, 25, 70));
  colors.push(color(95, 25, 55));

  time = 0.0;
}


function draw() {
  background(238, 238, 238);

  for (var i = 0; i < wave_num; i = i + 1) {
    waves[i].update();
  }

  for (var i = 0; i < wave_num; i = i + 1) {
    var c = getColor((i / (wave_num - 1)));
    fill(c);
    waves[i].draw();
  }

  time = time + 1;
}

class Wave {
  constructor(mode, num, amp, dia) {
    this.mode = mode;
    this.num = num;
    this.amplitude = amp;
    this.diameter = dia;
  }

  setup() {
    this.values = [];
    for (var i = 0; i < this.num; i = i + 1) {
      this.values[i] = 0.0;
    }
  }

  update() {
    for (var i = 0; i < this.values.length; i = i + 1) {
      var x = 180 / (this.values.length - 1) * i + time;
      if (this.mode == 'sin') {
        this.values[i] = this.amplitude * sin(x);
      }
      else if (this.mode == 'cos') {
        this.values[i] = this.amplitude * cos(x);
      }
    }
  }

  draw() {
    for (var i = 0; i < this.values.length; i = i + 1) {
      var ellipseX = windowWidth / (this.values.length - 1) * i;
      var ellipseY = windowHeight / 2 + this.values[i];
      ellipse(ellipseX, ellipseY, this.diameter);
    }
  }

}

function getColor(t) {
  t = constrain(t, 0.000, 0.9999);
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}