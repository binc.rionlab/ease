var diagonal;
var division;

var colors = [];
var time;

var ellipses = [];
var ellipse_num;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  noStroke();
  angleMode(DEGREES);

  ellipse_num = 15;
  division = 40;

  for (var i = 0; i < ellipse_num; i = i + 1) {
    var diameter = i * max(windowWidth,
               windowHeight) / (ellipse_num - 1);
    var radius = diameter / 2;
    var x = [], y = [];
    for (var k = 0; k < 360 / division; k = k + 1) {
      var randomX = random(0.95, 1.05);
      var randomY = random(0.95, 1.05);
      x[k] = randomX * radius * cos(k * division);
      y[k] = randomY * radius * sin(k * division);
      x[k] = x[k] + windowWidth / 2;
      y[k] = y[k] + windowHeight / 2;
    }
    ellipses[i] = { 'x': x, 'y': y };
  }

  colors.push(color(25, 15, 40));
  colors.push(color(105, 105, 215));
  colors.push(color(90, 175, 245));
  colors.push(color(120, 255, 255));
  colors.push(color(170, 255, 175));
  colors.push(color(255, 255, 128));
}

function draw() {
  background(255, 255, 255);

  for (var i = ellipse_num - 1; i >= 0; i = i - 1) {
    var c = getColor(i / (ellipse_num - 1));
    fill(c);
    beginShape();
    for (var k = 0; k < 360 / division; k = k + 1) {
      curveVertex(ellipses[i].x[k], ellipses[i].y[k]);
    }
    curveVertex(ellipses[i].x[0], ellipses[i].y[0]);
    curveVertex(ellipses[i].x[1], ellipses[i].y[1]);
    curveVertex(ellipses[i].x[2], ellipses[i].y[2]);
    endShape();
  }
}


function getColor(t) {
  t = constrain(t, 0.000, 0.9999);
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}