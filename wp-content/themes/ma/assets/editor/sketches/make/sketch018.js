var xoff, yoff;

var time;

var colors = [];

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(238, 238, 238);

  noFill();
    
  colors.push(color(250, 240, 128, 128))
  colors.push(color(250, 140, 40, 128));
  colors.push(color(240, 75, 45, 128));
  colors.push(color(230, 55, 90, 128));
  colors.push(color(180, 60, 180, 128));
  colors.push(color(35, 5, 55, 128));

  xoff = 0.0;
  yoff = 0.0;
  
  time = 0;
}

function draw() {

  var c = getColor(time);
  stroke(c);

  beginShape();
  var xoff = 0;
  for (var x = 0; x <= windowWidth; x += 5) {
    var y = map(noise(xoff, yoff), 0, 1, 0, windowHeight);
    curveVertex(x, y);
    xoff += 0.03;
  }
  yoff += 0.002;
  curveVertex(windowWidth,
               windowHeight);
  endShape();
  
  time = time + 0.001;

  if (time > 1.0) {
    time = 0;
  } 
}

function getColor(t) {
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}