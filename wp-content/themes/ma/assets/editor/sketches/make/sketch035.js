function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  frameRate(20);
}

function draw() {
  strokeWeight(7);
  stroke(frameCount % 255);
  line(random(width), 0, random(width), height);
}