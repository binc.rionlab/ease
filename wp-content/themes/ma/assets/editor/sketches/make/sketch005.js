var NX = 10;
var NY = 20;
var interval_x = 50;
var interval_y = 80;

var roughness_x = 0.4;
var roughness_y = 0.8;
var roughness_angle = 0.1;

var box_x;
var box_y;
var box_angle;
var box_size;

function setup() {

  box_x = [];
  box_y = [];
  box_angle = [];
  box_size = 25;

  for (var i = 0; i < NX; i++) {
    box_x[i] = [];
    box_y[i] = [];
    box_angle[i] = [];
    for (var j = 0; j < NY; j++) {
      box_x[i][j] = interval_x / 2 + box_size / 2 + box_size * i;
      box_y[i][j] = interval_y / 2 + box_size / 2 + box_size * j;
      box_angle[i][j] = radians(random(-roughness_angle * j * NY, roughness_angle * j * NY));
    }
  }

  createCanvas(NX * box_size + interval_x, NY * box_size + interval_y);
  background(238, 238, 238);
  rectMode(CENTER);
  stroke(50, 50, 50);
  noFill();

  for (var i = 0; i < NX; i++) {
    for (var j = 0; j < NY; j++) {
      push();
      translate(box_x[i][j], box_y[i][j]);
      push();
      rotate(box_angle[i][j]);
      push();
      translate(random(-roughness_x * j, roughness_x * j), random(-roughness_y * j, roughness_y * j));
      rect(0, 0, box_size, box_size);
      pop();
      pop();
      pop();
    }
  }

}

function draw() {

}