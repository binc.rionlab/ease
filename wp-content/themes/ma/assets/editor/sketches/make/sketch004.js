var N = 30;
var a = 10;

function setup() {
  createCanvas(N * a, N * a);
  background(30, 30, 30);
  stroke(250, 250, 250);
  noFill();
  rectMode(CENTER);

  for (var n = 0; n <= N; n++) {

    if (n < N * 0.5) {
      rect(N * a * 0.5, N * a * 0.5, N * a - n * a, N * a - n * a);
    }

    line(n * a, 0, n * a * 0.5 + N * a * 0.25, N * a * 0.25);
    line(n * a, N * a, n * a * 0.5 + N * a * 0.25, N * a * 0.75);
    line(0, n * a, N * a * 0.25, n * a * 0.5 + N * a * 0.25);
    line(N * a, n * a, N * a * 0.75, n * a * 0.5 + N * a * 0.25);

    line(n * a * 0.5 + N * a * 0.25, N * a * 0.25, n * a * 0.5 + N * a * 0.25, N * a * 0.75);
    line(N * a * 0.25, n * a * 0.5 + N * a * 0.25, N * a * 0.75, n * a * 0.5 + N * a * 0.25);

  }

}