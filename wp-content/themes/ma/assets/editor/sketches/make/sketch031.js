function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  noStroke();
  fill(frameCount, 128 + frameCount, 128, 2);
  rect(frameCount, height * 0.25,
       200, height * 0.5);
}