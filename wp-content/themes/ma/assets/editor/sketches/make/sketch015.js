var rectX = [];
var rectY = [];
var rectWidth = [];
var rectHeight = [];

var colors = [];
var time;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  angleMode(DEGREES);
  rectMode(CENTER);

  for (var i = 90; i > 0; i = i - 5) {
    rectX[i] = windowWidth / 2;
    rectY[i] = windowHeight / 2;
    rectWidth[i] = i * (windowHeight / 90);
    rectHeight[i] = windowHeight * sin(i);
  }

  colors.push(color(95, 25, 55, 128));
  colors.push(color(200, 25, 70, 128));
  colors.push(color(255, 100, 105, 128));
  colors.push(color(255, 160, 225, 128));
  colors.push(color(200, 200, 250, 128));

  time = 0;
}

function draw() {
  background(238, 238, 238);
  strokeWeight(2);
  stroke(255, 255, 255, 20);
  
  for (var i = 90; i > 0; i = i - 5) {
    var c = getColor(i / 90);
    fill(c);
    rect(rectX[i] + 30 * sin(3.0 * i + time), rectY[i], rectWidth[i], rectHeight[i]);
  }

  time = time + 1;
}

function getColor(t) {
  t = constrain(t, 0.000, 0.9999);
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}