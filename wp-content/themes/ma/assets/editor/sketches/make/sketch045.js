var diameter;

function setup() {
  createCanvas(windowWidth,
               windowHeight);

  diameter = 150;
  noStroke();
  fill(250, 100, 100, 20);
}

function draw() {
  background(234, 234, 234);

  translate(width / 2, height / 2);
  for (var i = 0; i < TWO_PI; i += 0.1) {
    var x = diameter * cos(i);
    var y = diameter * sin(i);

    ellipse(x, y, 180);
  }
}