function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  noStroke();
  fill(frameCount, 128 + frameCount, 128, 5);
  ellipse(150, 80, frameCount);
}