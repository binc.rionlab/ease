var colors = [];
var time;

function setup() {
  createCanvas(windowWidth,
               windowHeight);

  colors.push(color(255, 130, 90));
  colors.push(color(255, 175, 120));
  colors.push(color(250, 210, 210));
  colors.push(color(245, 230, 235));
  colors.push(color(200, 220, 235));
  colors.push(color(120, 215, 240));
  colors.push(color(0, 190, 225));
  colors.push(color(0, 120, 170));
  
  time = 0.0;
}

function draw() {
  background(255, 255, 255);

  var column = 20;
  var row = 20;

  for (var x = 0; x < width; x += width / column) {
    var c = getColor(abs(x - width / 2) / (width / 2));
    stroke(c);

    var ax = 5.0 * x / width;
    strokeWeight(2 + 8 * (1.0 + sin(ax + time)));
    line(x, 0, x, height);
  }

  for (var y = 0; y < height; y += height / row) {
    var c = getColor(abs(y - height / 2) / (height / 2));
    stroke(c);

    var ay = 10.0 * y / height;
    stroke(c);
    strokeWeight(2 + 5 * (1.0 + sin(ay + time)));
    line(0, y, width, y);
  }

  time = time + 0.05;
}

function getColor(t) {
  t = constrain(t, 0.000, 0.9999);
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}