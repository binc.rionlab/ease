function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  var w = width;
  var h = height;
  var hw = width / 2;
  var hh = height / 2;

  var color_bg_A = color(235, 121, 24);
  var color_bg_B = color(226, 95, 13);
  var color_bg_C = color(224, 73, 20);
  var color_bg_D = color(211, 56, 38);

  var color_stroke_A = color(207, 139, 138);
  var color_stroke_B = color(173, 149, 169);
  var color_stroke_C = color(122, 158, 154);
  var color_stroke_D = color(114, 134, 83);

  triangles(0, 0, 0, hh, hw, 0, color_bg_A, color_stroke_A);
  triangles(hw, 0, hw, hh, w, 0, color_bg_B, color_stroke_A);

  triangles(hw, 0, 0, hh, hw, hh, color_bg_B, color_stroke_B);
  triangles(w, 0, hw, hh, w, hh, color_bg_C, color_stroke_B);

  triangles(0, hh, 0, h, hw, hh, color_bg_B, color_stroke_C);
  triangles(hw, hh, hw, h, w, hh, color_bg_C, color_stroke_C);

  triangles(hw, hh, 0, h, hw, h, color_bg_C, color_stroke_D);
  triangles(w, hh, hw, h, w, h, color_bg_D, color_stroke_D);
}

function triangles(a_x, a_y, b_x, b_y, c_x, c_y, color_bg, color_stroke) {
  noStroke();
  fill(color_bg);
  triangle(a_x, a_y, b_x, b_y, c_x, c_y);

  var A = sqrt((b_x - c_x) * (b_x - c_x) + (b_y - c_y) * (b_y - c_y));
  var B = sqrt((c_x - a_x) * (c_x - a_x) + (c_y - a_y) * (c_y - a_y));
  var C = sqrt((a_x - b_x) * (a_x - b_x) + (a_y - b_y) * (a_y - b_y));
  var AB_x = b_x - a_x;
  var AB_y = b_y - a_y;
  var AC_x = c_x - a_x;
  var AC_y = c_y - a_y;
  var O_x = a_x + (B * AB_x + C * AC_x) / (A + B + C);
  var O_y = a_y + (B * AB_y + C * AC_y) / (A + B + C);

  strokeWeight(2);
  stroke(color_stroke);
  noFill();
  for (var i = 0.26; i < 1; i += 0.045) {
    triangle(a_x + (O_x - a_x) * i * i, a_y + (O_y - a_y) * i * i,
      b_x + (O_x - b_x) * i * i, b_y + (O_y - b_y) * i * i,
      c_x + (O_x - c_x) * i * i, c_y + (O_y - c_y) * i * i)
  }
}
