var SIZE = 300;
var max_times = 5;
var speed = 1.01;

function setup() {
  createCanvas(SIZE, SIZE);
  noStroke();
  rectMode(CENTER);
}

var factor = 1;

function draw() {
  background(0, 0, 0);
  fill(255, 255, 255);
  
  factor = factor * speed;
  if (factor > 3.0) {
    factor = 1;
  }

  scale(factor);

  SierpinskiCarpet(SIZE / 2, SIZE / 2, SIZE / 3, max_times);
}

function SierpinskiCarpet(Ax, Ay, B, times) {
  if (times > 0) {
    SierpinskiCarpet(Ax - B, Ay - B, B / 3, times - 1);
    SierpinskiCarpet(Ax, Ay - B, B / 3, times - 1);
    SierpinskiCarpet(Ax + B, Ay - B, B / 3, times - 1);
    SierpinskiCarpet(Ax - B, Ay, B / 3, times - 1);
    SierpinskiCarpet(Ax + B, Ay, B / 3, times - 1);
    SierpinskiCarpet(Ax - B, Ay + B, B / 3, times - 1);
    SierpinskiCarpet(Ax, Ay + B, B / 3, times - 1);
    SierpinskiCarpet(Ax + B, Ay + B, B / 3, times - 1);
  }
  rect(Ax, Ay, B, B);
}