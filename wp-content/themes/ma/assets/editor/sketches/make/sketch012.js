var angle = 137.5;
var start = 3;
var a = 6;
var spot_size = 12;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(238, 238, 238);
  stroke(0, 0, 0);
  colorMode(HSB);
}

function draw() {
  translate(width / 2, height / 2);

  var theta = start * radians(angle);
  var rad = sqrt(start) * a;

  fill(map(start, 0, 2500, 0, 360), 85, 100);
  ellipse(rad * cos(theta), rad * sin(theta), spot_size);

  start++;
}