var img;

function preload() {
  img = loadImage('images/peace.jpg');
}

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  frameRate(30);
  img.resize(width, height);
  background(0);
}

function draw() {
  for (var i = 0; i < 100; i++) {
    var x = random(width);
    var y = random(height);
    var col = img.get(x, y);
    var rotation = map(saturation(col), 0, 255, 0, 360);
    var length = map(brightness(col), 0, 255, 0, 40);
    noStroke();
    fill(red(col), green(col), blue(col), 127);
    push();
    translate(x, y);
    rotate(radians(rotation));
    rect(0, 0, 2, length);
    pop();
  }
}

// Unsplash
// https://unsplash.com/photos/RVF0ngUujks