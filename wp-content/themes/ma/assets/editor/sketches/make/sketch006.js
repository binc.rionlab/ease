var NX = 15;
var NY = 25;
var SIZE = 20;
var roughness = 1.3;
var range_min = 0.1;
var range_max = 2.0;

function setup() {
  createCanvas(NX * SIZE, NY * SIZE);
  background(245, 239, 227);
  stroke(58, 48, 39);
  for (var i = 0; i < NX; i++) {
    for (var j = 0; j < NY; j++) {
      squares(SIZE / 2 + i * SIZE, SIZE / 2 + j * SIZE, SIZE);
    }
  }
}

function squares(x, y, size) {
  var a_x, a_y, b_x, b_y, c_x, c_y, d_x, d_y;

  var N = random(3, 10);
  var ratio = size / N;

  for (var i = 0; i < N; i++) {
    a_x = x - size / 2; a_y = y - size / 2;
    b_x = x - size / 2; b_y = y + size / 2;
    c_x = x + size / 2; c_y = y + size / 2;
    d_x = x + size / 2; d_y = y - size / 2;

    a_x += random(-roughness, roughness);
    a_y += random(-roughness, roughness);
    b_x += random(-roughness, roughness);
    b_y += random(-roughness, roughness);
    c_x += random(-roughness, roughness);
    c_y += random(-roughness, roughness);
    d_x += random(-roughness, roughness);
    d_y += random(-roughness, roughness);

    line(a_x, a_y, b_x, b_y);
    line(b_x, b_y, c_x, c_y);
    line(c_x, c_y, d_x, d_y);
    line(d_x, d_y, a_x, a_y);

    size = size - ratio * random(range_min, range_max);
  }
}

function draw() {

}