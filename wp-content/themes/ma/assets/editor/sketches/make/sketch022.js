var cellX;
var cellY;
var time;
var colors = [];

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(255, 255, 255);
  noStroke();

  colors.push(color(255, 255, 128));
  colors.push(color(170, 255, 175));
  colors.push(color(90, 175, 245));
  colors.push(color(105, 105, 215));
  colors.push(color(25, 15, 40));

  cellX = 50;
  cellY = 50;

  time = 0;
}

function draw() {
  for (var y = 0; y < cellY; y = y + 1) {
    for (var x = 0; x < cellX; x = x + 1) {
      var val = noise(x / 100, y / 100, time);
      var c = getColor(val);

      var rectWidth = windowWidth / cellX;
      var rectHeight = windowHeight / cellY;
      var rectX = x * rectWidth;
      var rectY = y * rectHeight;

      fill(c);
      rect(rectX, rectY, rectWidth, rectHeight);
    }
  }

  time += 0.01;
}

function getColor(t) {
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}