function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  noFill();

  for (var i = 0; i < 1000; i = i + 1) {
    stroke(random(100, 255), random(50, 255), random(0, 255));
    ellipse(windowWidth / 2, windowHeight/ 2, i * 2);
  }
}

function draw() {

}