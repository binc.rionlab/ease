var colors = [];
var diamter;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  noStroke();

  diameter = 38;

  colors.push(color(25, 15, 40));
  colors.push(color(105, 105, 215));
  colors.push(color(90, 175, 245));
  colors.push(color(120, 255, 255));
  colors.push(color(170, 255, 175));
  colors.push(color(255, 255, 128));
}

function draw() {
  background(255, 255, 255);
  var xoff = 0, yoff = 0;
  for (var y = 0; y <= height; y += diameter * sqrt(3) / 2) {
    for (var x = 0; x <= width; x += diameter) {
      xoff = (yoff % 2) ? diameter / 2 : 0;
      var amplitude = abs(sin(x + xoff + 0.01 * frameCount));
      fill(getColor(amplitude));      
      ellipse(x + xoff, y, diameter * amplitude);
    }
    yoff = yoff + 1;
  }
}

function getColor(t) {
  t = constrain(t, 0.000, 0.9999);
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}