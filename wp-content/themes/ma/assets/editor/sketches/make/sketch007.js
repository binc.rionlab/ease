var angle;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  angleMode(DEGREES);
  angle = 30;
  background(234, 234, 234);
  stroke(20, 20, 20);
  translate(width / 2, height);
  branch(height / 4);
}

function draw() {
}

function branch(L) {
  angle = random(25, 40);
  var w = map(L, 0, width / 4, 1, 20);
  strokeWeight(w);
  line(0, 0, 0, -L);
  translate(0, -L);
  if (L > 5) {
    push();
    rotate(angle);
    branch(L * 0.7);
    pop();
    push();
    rotate(-angle);
    branch(L * 0.7);
    pop();
  }
}

//
// Reference:
// FRACTAL TREES - RECURSIVE
// CODING CHALLENGE #14
// https://thecodingtrain.com/CodingChalLges/014-fractaltree.html
//