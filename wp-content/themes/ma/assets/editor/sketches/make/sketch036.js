function setup() {
  createCanvas(windowWidth,
               windowHeight);
}

function draw() {
  var from = color(0, 128, 255);
  var to = color(255, 100, 150);

  noStroke();

  for (var i = 0.0; i < width; i += 5.0) {
    fill(lerpColor(from, to, i / width));
    rect(i, 0, 5, height);
  }
}