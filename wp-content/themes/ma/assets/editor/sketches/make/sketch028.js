function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);

  for (val = 5; val < windowWidth; val = val + 50) {
    strokeWeight(val / 10);
    point(val, height / 2);
  }
}