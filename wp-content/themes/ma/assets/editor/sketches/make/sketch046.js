var diameter;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  frameRate(10);
  noStroke();
  noFill();

  diameter = 150;
}

function draw() {
  fill(0, 0, 0);
  text("Touch Me", 20, 40);

  if (mouseIsPressed) {
    stroke(255, 100, 100, 10);
    noFill();
    translate(mouseX, mouseY);
    for (var i = 0; i < TWO_PI; i += 0.1) {
      var x = diameter * cos(i);
      var y = diameter * sin(i);
      ellipse(x, y, 180);
    }
  }
}