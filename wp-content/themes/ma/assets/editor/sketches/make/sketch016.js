var A = 1.4;
var B = 2.5;
var C = 1.4;
var D = -2.3;

var time;
var colors = [];

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(238, 238, 238);
  noStroke();

  colors.push(color(150, 180, 250, 128));
  colors.push(color(200, 200, 250, 100));
  colors.push(color(255, 160, 225, 10));
  colors.push(color(255, 100, 105, 10));
  colors.push(color(200, 25, 70, 10));
  colors.push(color(95, 25, 55, 5));


  time = 0;
}

function draw() {
  translate(windowWidth / 2, windowHeight / 2);

  var x = random(-3, 3);
  var y = random(-3, 3);

  for (var i = 0; i < 2000; i++) {
    var xx = sin(A * y) - cos(B * x);
    var yy = sin(C * x) - cos(D * y);
    var c = getColor(time);
    fill(c);
    ellipse(xx * width * 0.25, yy * height * 0.25, 1);
    x = xx;
    y = yy;
  }

  time = time + 0.01;

  if (time > 1.0) {
    time = 0.0;
    C += 0.1;
  }
}

function getColor(t) {
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}