var centerXa;
var centerXb;
var centerXc;
var centerXd;
var centerXe;

function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);

  centerXa = 0;
  centerXb = 0;
  centerXc = 0;
  centerXd = 0;
  centerXe = 0;
}

function draw() {
  noStroke();

  centerXa = centerXa + 1;
  centerXb = centerXb + 1.5;
  centerXc = centerXc + 2;
  centerXd = centerXd + 2.5;
  centerXe = centerXe + 3;

  fill(0, 200, 255);
  ellipse(centerXa, 30, 60);

  fill(63.75, 175, 228.75);
  ellipse(centerXb, 90, 60);

  fill(127.5, 150, 202.5);
  ellipse(centerXc, 150, 60);

  fill(191.25, 125, 176.25);
  ellipse(centerXd, 210, 60);

  fill(255, 100, 150);
  ellipse(centerXe, 270, 60);
}
