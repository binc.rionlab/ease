var centerYa;
var centerYb;

function setup() {
  createCanvas(300, 300);
  centerYa = 150;
  centerYb = 150;
}

function draw() {
  background(250, 250, 250);
  noStroke();
   
  fill(85, 140, 200);
  ellipse(150, centerYa, 120);
 
  fill(255, 185, 35);
  ellipse(150, centerYb, 60);
}