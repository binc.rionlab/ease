var centerYa;
var centerYb;

function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);
  centerYa = 150;
  centerYb = 150;
}

function draw() {
  noStroke();

  centerYa = centerYa + 1;
  centerYb = centerYb - 1;

  fill(85, 140, 200);
  ellipse(150, centerYa, centerYa);

  fill(250, 250, 255);
  ellipse(150, centerYb, centerYb);
}