var ellipseX;
var ellipseY;

function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
  strokeWeight(2);
}

function draw() {
  ellipseX = random(0, 300);
  ellipseY = random(0, 300);

  if (ellipseX > 100) {
    fill(215, 35, 30);
    ellipse(ellipseX, ellipseY, random(10, 20));
  }

  if (ellipseX < 200) {
    fill(45, 150, 180);
    ellipse(ellipseX, ellipseY, random(10, 20));
  }

}