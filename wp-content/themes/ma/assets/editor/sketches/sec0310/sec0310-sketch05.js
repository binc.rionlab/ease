function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  
  strokeWeight(50);
  stroke(255, 100, 150, 10);
  line(0, 0, 0, 300);

  stroke(255, 100, 150, 40);
  line(50, 0, 50, 300);

  stroke(255, 100, 150, 80);
  line(100, 0, 100, 300);

  stroke(255, 100, 150, 120);
  line(150, 0, 150, 300);

  stroke(255, 100, 150, 160);
  line(200, 0, 200, 300);

  stroke(255, 100, 150, 200);
  line(250, 0, 250, 300);

  stroke(255, 100, 150, 240);
  line(300, 0, 300, 300);
}