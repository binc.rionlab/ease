function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
    
  noStroke();
  fill(0, 160, 140);
  rect(10, 100, 280, 100);
 
  strokeWeight(45);

  stroke(255, 220, 0, 0);
  point(40, 125);
    
  stroke(255, 220, 0, 64);
  point(95, 150);
    
  stroke(255, 220, 0, 128);
  point(150, 150);
    
  stroke(255, 220, 0, 192);
  point(205, 150);
    
  stroke(255, 220, 0, 255);
  point(260, 150);    
}