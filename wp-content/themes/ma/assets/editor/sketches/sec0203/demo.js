function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {

  noStroke();
  fill(0, 128, 128, 2);

  //adding value to the x coordinate of the first point.
  triangle(150 + frameCount / 2, 75, 30, 290, 270, 290);
}

