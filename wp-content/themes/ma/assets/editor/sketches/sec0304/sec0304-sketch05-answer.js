function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  stroke(250, 100, 40);
  rect(20, 20, 260, 260);
  rect(80, 80, 140, 140);
}