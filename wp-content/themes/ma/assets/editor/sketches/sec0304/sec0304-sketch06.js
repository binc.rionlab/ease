function setup() {
  createCanvas(300, 300);
  strokeWeight(3);  
}

function draw() {
  background(238, 238, 238);

  stroke(0, 200, 255);
  rect(20, 20, 260, 260);
  
  stroke(40, 185, 235);
  rect(40, 40, 220, 220);
  
  stroke(85, 165, 220);
  rect(60, 60, 180, 180);
  
  stroke(128, 150, 200);
  rect(80, 80, 140, 140);
  
  stroke(170, 135, 185);
  rect(100, 100, 100, 100);
  
  stroke(210, 115, 168);
  rect(120, 120, 60, 60);
  
  stroke(255, 100, 150);
  rect(140, 140, 20, 20);
}