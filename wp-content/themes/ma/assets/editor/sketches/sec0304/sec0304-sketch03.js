function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);

  line(20, 100, 280, 100);

  stroke(60, 190, 250);
  line(20, 200, 280, 200);

  stroke(250, 0, 60);
}