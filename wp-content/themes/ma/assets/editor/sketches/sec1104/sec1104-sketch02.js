var haiku;

function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
  haiku = "天生\n我材\n必有用";
}

function draw() {
  background(250, 250, 250);
 
  textSize(20);
  text(haiku, 15, 40);
}