var haiku;

function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
  haiku = "天生我材必有用";
}

function draw() {
  background(250, 250, 250);
 
  textSize(20);
  text(haiku, 15, 40, 150, 300);
}