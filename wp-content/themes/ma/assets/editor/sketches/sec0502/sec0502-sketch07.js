function setup() {
  createCanvas(300, 300);  
}

function draw() {
  background(238, 238, 238);
  noStroke();
 
  fill(85, 140, 200);
  ellipse(mouseX, mouseY, 120);

  fill(255, 185, 35);
  ellipse(mouseX, mouseY - 90, 60);
  ellipse(mouseX, mouseY + 90, 60);
  ellipse(mouseX - 90, mouseY, 60);
  ellipse(mouseX + 90, mouseY, 60);
}