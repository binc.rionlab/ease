var diaSlider;
var diameter;

var GSlider;
var G;

function setup() {
  createCanvas(300, 300);
  diaSlider = createSlider(0, 300, 150);
  diaSlider.position(10, 70);

  GSlider = createSlider(0, 255, 200);
  GSlider.position(10, 100);
}

function draw() {
  background(238, 238, 238);

  diameter = diaSlider.value();


  noStroke();
  fill(250, G, 0);
  ellipse(150, 150, diameter);

  fill(0, 0, 0);
  text(diameter, 150, 45);
  text(G, 150, 75);
}