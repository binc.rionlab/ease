var diaSlider;
var diameter;

function setup() {
  createCanvas(300, 300);

  diaSlider = createSlider(0, 300, 150);
  diaSlider.position(30, 150);
}

function draw() {
  background(238, 238, 238);

  diameter = diaSlider.value();

  noStroke();
  fill(250, 200, 0);
  ellipse(150, 150, diameter);

  fill(0, 0, 0);
  text(diameter, 160, 140);
}

