var diaSlider;
var diameter;

var RSlider;
var GSlider;
var BSlider;

var R;
var G;
var B;

function setup() {
  createCanvas(300, 300);
  diaSlider = createSlider(0, 300, 150);
  diaSlider.position(80, 40);

  RSlider = createSlider(0, 255, 50);
  RSlider.position(80, 70);
  GSlider = createSlider(0, 255, 200);
  GSlider.position(80, 100);
  BSlider = createSlider(0, 255, 255);
  BSlider.position(80, 130);
}

function draw() {
  background(250, 250, 250);

  diameter = diaSlider.value();
  R = RSlider.value();
  G = GSlider.value();
  B = BSlider.value();

  noStroke();
  fill(R, G, B);
  ellipse(150, 150, diameter);

  fill(0, 0, 0);
  text(diameter, 160, 30);
  text(R, 160, 60);
  text(G, 160, 90);
  text(B, 160, 120);
}
