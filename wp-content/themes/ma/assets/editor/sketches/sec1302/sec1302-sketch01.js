function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);

  noStroke();
  fill(250, 200, 0);
  ellipse(150, 150, 150);
}