var sound;
var panning;

var startButton;
var pauseButton;
var panSlider;
var isPlaying;
var muted;

function preload() {
  sound = loadSound("sound1.mp3");
}

function setup() {
  createCanvas(300, 300);

  startButton = createButton("Press to start sound");
  startButton.position(90, 130);
  startButton.mousePressed(start);
  
  pauseButton = createButton("Pause");
  pauseButton.position(90, 160);
  pauseButton.mousePressed(pause);
 
  panSlider = createSlider(-1.0, 1.0, 0.0, 0.1);
  panSlider.position(90, 190);

  isPlaying = false;
  muted = false;
}

function draw() {
  background(250, 250, 250);
  
  panning = panSlider.value();

  if (muted == false) {
    sound.pan(panning); 
  }

  text(panning,170,105);
}

function start() {
  if (isPlaying == false) {
    sound.loop();
    isPlaying = true;
  }

  if (muted) {
    sound.setVolume(1.0);
    muted = false;
  }
}

function pause() {
  muted = !muted;
  if (muted) {
    sound.setVolume(0.0);
  } else {
    sound.setVolume(1.0);
  }
}

// 
// 声音数据
// 
// 心脏的跳动、紧张 
// written by 123理论
// https://dova-s.jp/se/play868.html
// 