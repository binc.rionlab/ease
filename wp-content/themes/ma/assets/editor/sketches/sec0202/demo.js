var val1x = 100;
var val1y = 0;
var val2x = 300;
var val2y = 0;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  line(val1x, val1y, val2x, val2y);

  val1y = val1y + 5;
  val2y = val2y + 3;
}