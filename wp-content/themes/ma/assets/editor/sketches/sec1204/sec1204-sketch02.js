var monalisa;
var color = [];

function preload() {
  monalisa = loadImage("image5.jpg");
}

function setup() {
  createCanvas(300, 450);
  background(250, 250, 250);
}

function draw() {
  image(monalisa, 0, 0);
 
  color = get(mouseX, mouseY);  

  noStroke();
  fill(255, 255, 255);
  text("R: " + color[0], 10, 20);  
  text("G: " + color[1], 10, 40);  
  text("B: " + color[2], 10, 60);  
}