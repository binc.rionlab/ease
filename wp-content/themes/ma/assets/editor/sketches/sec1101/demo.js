var abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var time;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  frameRate(5);
  textSize(windowHeight);
  time = 0;
}

function draw() {
  background(234, 234, 234);
  fill(0, 0, 0);
  text(abc[time], 0, windowHeight);
  time = time + 1;
  if (time > 25) {
    time = 0;
  }
}