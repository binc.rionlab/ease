function setup() {
  createCanvas(300, 300);
  strokeWeight(3);
}

function draw() {
  ellipse(random(0, 300), random(0, 300), random(50, 150));
}