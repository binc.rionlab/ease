function setup() {
  createCanvas(300, 300);
}

function draw() {
  stroke(random(0, 255), random(0, 255), random(0, 255));
  line(0, random(0, 300), 150, random(0, 300));
  line(random(150, 300), 0, random(150, 300), 300);
}