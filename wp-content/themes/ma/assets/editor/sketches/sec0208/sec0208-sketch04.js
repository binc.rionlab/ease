function setup() {
  createCanvas(300, 300);
  strokeWeight(3);
}

function draw() {
  noFill();
  stroke(random(0, 255), random(0, 255), random(0, 255));
  ellipse(random(0, 300), random(0, 300), random(50, 150));
}