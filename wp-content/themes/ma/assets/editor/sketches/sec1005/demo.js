var centerX;
var centerY;
var diagonal;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  noFill();

  centerX = windowWidth / 2;
  centerY = windowHeight / 2;
  diagonal = dist(centerX, centerY, 0, 0);

  background(234, 234, 234);
}

function draw() {
  for (var i = 0; i < 20; i = i + 1) {
    var ellipseX = random(0, windowWidth);
    var ellipseY = random(0, windowHeight);
    var distance = dist(centerX, centerY, ellipseX, ellipseY);
    var ratio = distance / diagonal;

    stroke(240 * ratio, 160 - 125 * ratio, 250 - 155 * ratio, 255 * ratio);
    ellipse(ellipseX, ellipseY, 1 + 20 * ratio);
  }
}