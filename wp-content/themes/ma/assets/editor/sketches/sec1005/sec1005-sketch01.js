function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  noFill();
  strokeWeight(3);

  drawRect(50, 50, 200, 200);
}

function drawRect(rX, rY, rW, rH) {
  line(rX, rY, rX + rW, rY);
  line(rX + rW, rY, rX + rW, rY + rH);
  line(rX + rW, rY + rH, rX, rY + rH);
  line(rX, rY + rH, rX, rY);
}