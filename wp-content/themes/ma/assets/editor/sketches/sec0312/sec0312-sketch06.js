function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);
}

function draw() {
  stroke(random(50, 255), 120, random(200, 255), random(0, 255));
  line(random(0, 300), random(0, 300), random(0, 300), random(0, 300)); 
}