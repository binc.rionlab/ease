function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
}

function draw() {
  fill(0, 150, 220);
  noStroke();
  ellipse(random(0, 300), random(0, 300), random(1, 50));
}