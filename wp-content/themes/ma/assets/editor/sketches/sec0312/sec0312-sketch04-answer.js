function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
}

function draw() {
  fill(0, random(100, 255), random(200, 255));
  noStroke();
  ellipse(random(0, 300), random(0, 300), random(1, 50));
}