function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
}

function draw() {
  stroke(0, 150, 220);
  noFill();
  ellipse(random(0, 300), random(0, 300), random(1, 15));
}