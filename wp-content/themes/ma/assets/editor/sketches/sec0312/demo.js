function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  fill(0, random(100, 255), random(200, 255), random(0, 255));
  noStroke();
  ellipse(random(0, width), random(0, width), random(1, 50));
}