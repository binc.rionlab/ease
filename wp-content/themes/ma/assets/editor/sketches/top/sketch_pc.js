var nx = 100;
var ny = 100;
var p = [];
var v = [];
var time;

var parts = [];
var N = 4000;

var pad = 10;

function setup(){
  createCanvas(windowWidth,
               windowHeight);

  colorMode(HSB, 360, 100, 100, 255);

  var zoff = Math.floor(random(50, 100));
  var sc = Math.floor(random(120, 200));
  field(zoff, sc);

  reset();
  time = 0.0;

}

function reset() {
  for (var i = 0; i < N; i = i + 1) {
    parts[i] = {
      x: random(-pad, width + pad),
      y: random(-pad, height + pad),
      pX: 0.0,
      pY: 0.0,
      vx: 0.0,
      vy: 0.0,
      age: random(0, 10)
    };

    parts[i].pX = parts[i].x;
    parts[i].pY = parts[i].y;
  }
}

function draw() {
  background(0, 0, 100, 1);

  var fx, fy, ix, iy, t, s, X, Y;
  var H, S, B, A;

  for (var i = 0; i < N; i = i + 1) {
    fx = (parts[i].x * nx) / width;
    fy = (parts[i].y * ny) / height;

    ix = constrain(Math.floor(fx), 1, nx - 2);
    iy = constrain(Math.floor(fy), 1, ny - 2);

    t = fx - ix;
    s = fy - iy;

    X = interp(v[ix    ][iy    ].x,
               v[ix + 1][iy    ].x,
               v[ix    ][iy + 1].x,
               v[ix + 1][iy + 1].x,
               t, s);

    Y = interp(v[ix    ][iy    ].y,
               v[ix + 1][iy    ].y,
               v[ix    ][iy + 1].y,
               v[ix + 1][iy + 1].y,
               t, s);

    parts[i].vx = 5.0 * X;
    parts[i].vy = 5.0 * Y;

    parts[i].x += parts[i].vx;
    parts[i].y += parts[i].vy;

    H = 180 * p[ix][iy] + 0.5 * time + 120;
    S = 80 + 0.333 * time;
    B = 90 + 20 * p[ix][iy];
    A = 20;

    stroke(H, S, B, A);
    line(parts[i].pX, parts[i].pY,
         parts[i].x, parts[i].y);

    if (
      parts[i].age > 20 ||
      parts[i].x < 0 ||
      parts[i].x > width ||
      parts[i].y < 0 ||
      parts[i].y > height
    ) {
      resetParticle(parts[i]);
    }

    parts[i].pX = parts[i].x;
    parts[i].pY = parts[i].y;
    parts[i].age++;
  }

  time++;


  if (time > 300) {
    background(0, 0, 100);
    var zoff = Math.floor(random(50, 100));
    var sc = Math.floor(random(120, 200));
    field(zoff, sc);
    reset();
    time = 0;
  }
}

function interp(a, b, c, d, t, s) {
  tmp1 = (1 - t) * a + t * b;
  tmp2 = (1 - t) * c + t * d;
  return (1 - s) * tmp1 + s * tmp2;
}

function resetParticle(p) {
  p.x = p.pX = random(-pad, width + pad);
  p.y = p.pY = random(-pad, height + pad);
  p.age = random(0, 10);
}

function field(zoff, sc) {
  var x, y;
  var nx1 = nx + 1;
  var ny1 = ny + 1;
  for (x = 0; x < nx1; x++) {
    p[x] = [];
    for (y = 0; y < ny1; y++) {
      p[x][y] = noise(x / sc,
                      y / sc,
                      zoff);
    }
  }

  for (x = 1; x < nx; x++) {
    v[x] = [];
    for (y = 1; y < ny; y++) {
      v[x][y] = {
        x: p[x][y + 1] - p[x][y],
        y: + p[x][y] -p[x + 1][y]
      };

      var L = sqrt(v[x][y].x * v[x][y].x + v[x][y].y * v[x][y].y);
      v[x][y].x /= L;
      v[x][y].y /= L;
    }
  }
}