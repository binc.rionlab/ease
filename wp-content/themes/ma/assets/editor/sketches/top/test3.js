var nx = 100;
var ny = 100;
var psi = [];
var v = [];
var time;

var parts = [];
var parts_num = 3000;
var zoff;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  colorMode(HSB, 360, 100, 100, 255);
  field(random(0, 100), random(20, 200));

  for (var i = 0; i < parts_num; i = i + 1) {
    parts[i] = {
      x: random(-100, width + 100),
      y: random(-100, height + 100),
      pX: 0.0,
      pY: 0.0,
      vx: 0.0,
      vy: 0.0,
    };

    parts[i].pX = parts[i].x;
    parts[i].pY = parts[i].y;
  }

  time = 0.0;
}

function draw() {
  background(0, 0, 100, 5);

  var ix, iy, H, S, B, A;
  for (var i = 0; i < parts_num; i = i + 1) {
    ix = parseInt(parts[i].x * nx / width) ;
    iy = parseInt(parts[i].y * ny / height) ;

    ix = constrain(ix, 1, nx - 1);
    iy = constrain(iy, 1, ny - 1);

    parts[i].vx = 3.0 * v[ix][iy].x;
    parts[i].vy = 3.0 * v[ix][iy].y;

    parts[i].x += parts[i].vx;
    parts[i].y += parts[i].vy;

    H = 120 + 180 * psi[ix][iy] + 0.5 * time;
    S = 80 + 1 / 3 * time;
    B = 80 + 20 * psi[ix][iy];
    A = 50;

    stroke(H, S, B, A);
    line(parts[i].pX, parts[i].pY, parts[i].x, parts[i].y);

    if (parts[i].x < 0 || parts[i].x > width ||
        parts[i].y < 0 || parts[i].y > height) {
      parts[i].x = random(-100, width + 100);
      parts[i].y = random(-100, height + 100);
    }

    parts[i].pX = parts[i].x;
    parts[i].pY = parts[i].y;
  }

  time = time + 1;

  if (time > 300) {
    background(0, 0, 100, 50);
    field(random(0, 100), random(20, 200));
    time = 0;
  }
}

function field(zoff, sc) {
  var x;

  for (x = 1; x < nx + 1; x = x + 1) {
    psi[x] = [];
    for (var y = 1; y < ny + 1; y = y + 1) {
      psi[x][y] = noise(x / sc, y / sc, zoff);
    }
  }

  for (x = 1; x < nx; x = x + 1) {
    v[x] = [];
    for (var y = 1; y < ny; y = y + 1) {
      v[x][y] = {
        x: psi[x][y + 1] - psi[x][y],
        y: -(psi[x + 1][y] - psi[x][y])
      };

      var l = sqrt(v[x][y].x * v[x][y].x + v[x][y].y * v[x][y].y);
      v[x][y].x /= l;
      v[x][y].y /= l;
    }
  }
}