var sound;
var startButton;

function preload() {
  sound = loadSound("sound4.mp3");
}

function setup() {
  createCanvas(300, 300);
  startButton = createButton("Press to start sound");
  startButton.position(70, 100);
  startButton.mousePressed(start);
}

function draw() {

}

function start() {
  sound.play();
}

// 
// 声音数据
// 
// 新年钟声
// written by NaruIDEA
// https://dova-s.jp/se/play1136.html
// 