var img;
var sound;
var speed;

var startButton;
var pauseButton;
var speedSlider;
var isPlaying;
var muted;

function preload() {
  sound = loadSound("sound5.mp3");
}

function setup() {
  createCanvas(300, 300);

  startButton = createButton("Press to start sound");
  startButton.position(90, 130);
  startButton.mousePressed(start);
  
  pauseButton = createButton("Pause");
  pauseButton.position(90, 160);
  pauseButton.mousePressed(pause);
 
  speedSlider = createSlider(0.0, 2.0, 1.0, 0.1);
  speedSlider.position(90, 190);

  isPlaying = false;
  muted = false;
}

function draw() {
  background(250, 250, 250);
  speed = speedSlider.value(); 
  sound.rate(speed);
  text(speed, 170, 80);
}

function start() {
  if (isPlaying == false) {
    sound.loop();
    isPlaying = true;
  }

  if (muted) {
    sound.setVolume(1.0);
    muted = false;
  }
}

function pause() {
  muted = !muted;
  if (muted) {
    sound.setVolume(0.0);
  } else {
    sound.setVolume(1.0);
  }
}

// 
// 声音数据
// 
// Apollo 11: That's One Small Step for (a) Man by NASA is licensed under a Creative Commons License.
// https://soundcloud.com/nasa/apollo-11-thats-one-small-step-for-a-man
// 