var diameter;

function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);
  strokeWeight(3);

  diameter = 20;
}

function draw() {
  ellipse(50,  100, diameter / 8);  
  ellipse(150, 100, diameter / 4); 
  ellipse(250, 100, diameter / 2); 
 
  ellipse(50,  200, diameter * 1); 
  ellipse(150, 200, diameter * 2); 
  ellipse(250, 200, diameter * 4); 
}