var diameter;

function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
  strokeWeight(3);

  diameter = 80;
}

function draw() {
  ellipse(50, 100, diameter);  
  ellipse(150, 100, diameter); 
  ellipse(250, 100, diameter); 
 
  ellipse(50, 200, diameter); 
  ellipse(150, 200, diameter); 
  ellipse(250, 200, diameter); 
}