function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
  strokeWeight(3);
}

function draw() {
  rect(20, 15, 140, 80);
  rect(20, 110, 140, 80);
  rect(20, 205, 140, 80);
}