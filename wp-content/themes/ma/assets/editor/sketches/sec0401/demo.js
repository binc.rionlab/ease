var diameter;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  strokeWeight(3);
  stroke(200, 200, 200);
  diameter = 150;
}

function draw() {
  var from = color(0, 200, 255, 150);
  var to = color(255, 100, 150, 150);

  if (mouseIsPressed)
    fill(lerpColor(from, to, 0.8));
  else
    fill(lerpColor(from, to, 0.2));
  ellipse(width / 2, height / 2, diameter);
}