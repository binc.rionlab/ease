var rectWidth;

function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
  strokeWeight(3);

  rectWidth = 140;
}

function draw() {
  rect(20, 15, rectWidth, 80);
  rect(20, 110, rectWidth, 80);
  rect(20, 205, rectWidth, 80);
}