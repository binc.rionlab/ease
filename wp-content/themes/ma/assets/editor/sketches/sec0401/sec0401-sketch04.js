var rectWidth;

function setup() {
  createCanvas(300, 300);
  strokeWeight(3);
}

function draw() {
  background(238, 238, 238);
  rectWidth = mouseX;
  rect(20, 15, rectWidth, 80);
  rect(20, 110, rectWidth, 80);
  rect(20, 205, rectWidth, 80);
}