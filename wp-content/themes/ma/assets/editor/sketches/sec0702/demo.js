var particlePos = [];
var particleVel = [];
var num = 1000;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);

  for (let i = 0; i < num; i++) {
    particlePos.push(new p5.Vector(random(width), random(height)));
    particleVel.push(new p5.Vector(random(-1, 1), random(-1, 1)));
  }
}

function draw() {
  background(234, 234, 234);

  fill(255, 0, 0, 20);
  noStroke();

  //draw
  for (let i = 0; i < num; i++) {
    particlePos[i].x += particleVel[i].x;
    particlePos[i].y += particleVel[i].y;

    if (particlePos[i].x > width) particlePos[i].x = 0;
    if (particlePos[i].x < 0) particlePos[i].x = width;
    if (particlePos[i].y > height) particlePos[i].y = 0;
    if (particlePos[i].y < 0) particlePos[i].y = height;

    arc(particlePos[i].x, particlePos[i].y, 10, 10, 0, PI + QUARTER_PI, PIE);
  }
}