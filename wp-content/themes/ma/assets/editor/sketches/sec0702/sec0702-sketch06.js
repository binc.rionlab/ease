var x;

function setup() {
  createCanvas(300, 300);
  noStroke();
  x = 300;
}

function draw() {
  background(238, 238, 238, 10);
  
  x = x - 5;
  
  if (x < 0) {
    x = 300;
  }

  fill(x * 0.8, 190, 250);
  ellipse(x, 150, 50);
}