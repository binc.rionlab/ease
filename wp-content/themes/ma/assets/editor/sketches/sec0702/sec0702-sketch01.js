var rectX;

function setup() {
  createCanvas(200, 200);
  strokeWeight(3);
}

function draw() {
  background(250, 250, 250);

  rectX = second();

  fill(255, 255, 255);

  if (rectX > 40) {
    fill(70, 200, 255);
  }

  rect(rectX, 60, 80, 80);

  fill(0, 0, 0);
  textSize(30);
  text(second(), 10, 30);
}