function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);

  strokeWeight(5);
  stroke(180, 0, 90);
  fill(255, 230, 0);

  rect(20, 20, 120, 260)
  line(160, 20, 280, 20);
  line(160, 150, 280, 150);
  point(160, 280);
  point(280, 280);
}