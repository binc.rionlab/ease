function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);

  strokeWeight(10);
  stroke(100, 25, 130);
  fill(255, 230, 0);

  noStroke();
    
  ellipse(135, 150, 240);
  ellipse(165, 150, 200);
  ellipse(195, 150, 160);
  ellipse(225, 150, 120);
}