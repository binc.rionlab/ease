function setup() {
  createCanvas(300, 300);
  strokeWeight(3);
}

function draw() {
  ellipse(150, 150, random(90, 240));
  rect(100, 100, 100, 100);
}