function setup() {
  createCanvas(300, 300);
  strokeWeight(3);
}

function draw() {
  ellipse(220, 150, random(20, 140));
  rect(15, 90, 120, 120);
}