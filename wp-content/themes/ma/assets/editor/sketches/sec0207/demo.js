function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  stroke(frameCount);
  rect(frameCount, frameCount, 150, 150);
}