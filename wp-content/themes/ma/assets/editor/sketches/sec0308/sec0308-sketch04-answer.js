function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);

  fill(190, 190, 10);
  triangle(150, 35, 90, 140, 210, 140);

  fill(190, 50, 60);
  triangle(90, 140, 30, 245, 150, 245);

  fill(45, 190, 170);
  noFill();
  triangle(210, 140, 150, 245, 270, 245);
}