function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  
  strokeWeight(5);
  stroke(230, 0, 60);
  fill(190, 210, 0);
 
  rect(40, 125, 220, 50);
  rect(125, 40, 50, 220);
}