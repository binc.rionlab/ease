var centerX;

function setup() {
  createCanvas(300, 300);
  centerX = 0;
  background(250, 250, 250);
}

function draw() {
  centerX = centerX + 5;
  ellipse(centerX, 200, 20);
}