var centerX;

function setup() {
  createCanvas(300, 300);
  centerX = 0;
}

function draw() {
  background(250, 250, 250);
  centerX = centerX + 5;
  ellipse(centerX, 200, 20);
}