var pointXa;  
var pointYa;  

var pointXb;  
var pointYb;  

function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);

  pointXa = 10;  
  pointYa = 10;  

  pointXb = 10;  
  pointYb = 290;  
}

function draw() {  
  pointXa = pointXa + 6;  
  pointYb = pointYb - 6;
 
  ellipse(pointXa, pointYa, 10, 10);  
  ellipse(pointXb, pointYb, 10, 10);  
}