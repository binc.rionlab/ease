var pointXa;
var pointYa;

var pointXb;
var pointYb;

var pointXc;
var pointYc;

var pointXd;
var pointYd;

function setup() {
 createCanvas(300, 300);
 background(238, 238, 238);

 pointXa = 10;
 pointYa = 10;

 pointXb = 10;
 pointYb = 290;
 pointXc = 290;
 pointYc = 10;

 pointXd = 290;
 pointYd = 290;
}

function draw() {
 pointXa = pointXa + 3;
 pointYb = pointYb - 3;
 pointYc = pointYc + 3;
 pointXd = pointXd - 3;
  
 stroke(pointYb, pointXa, pointXc, 50);
 noFill();
 ellipse(pointXa, pointYa, 5);
 ellipse(pointXb, pointYb, 5);
 ellipse(pointXc, pointYc, 5);
 ellipse(pointXd, pointYd, 5); 

 line(pointXa, pointYa, pointXb, pointYb);
 line(pointXc, pointYc, pointXd, pointYd);
}