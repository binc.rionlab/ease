var diameter = [];

function setup() {
  createCanvas(300, 300);

  diameter[0] = 80;
  diameter[1] = 160;
  diameter[2] = 240;
}

function draw() {
  background(250, 250, 250);
  strokeWeight(2);
  noFill();

  ellipse(150, 150, diameter[0]);
  ellipse(150, 150, diameter[1]);
  ellipse(150, 150, diameter[2]);
}