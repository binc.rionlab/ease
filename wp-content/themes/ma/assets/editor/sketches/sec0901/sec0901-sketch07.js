var rectX = [];
var rectY = [];

function setup() {
  createCanvas(300, 300);

  rectX[0] = 50;
  rectX[1] = 100;
  rectX[2] = 150;
  
  rectY[0] = 50;
  rectY[1] = 100;
  rectY[2] = 150;
}

function draw() {
  background(238, 238, 238);
  strokeWeight(2);

  fill(150, 180, 250);
  rect(rectX[0], rectY[0], 100, 100);
  
  fill(255, 160, 225);
  rect(rectX[1], rectY[1], 100, 100);
  
  fill(200, 25, 70);
  rect(rectX[2], rectY[2], 100, 100);
}