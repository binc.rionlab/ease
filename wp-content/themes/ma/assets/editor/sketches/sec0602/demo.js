var ball_num;
var ballX = [];
var ballY = [];
var speedX = [];
var speedY = [];
var ball_color = [];
var ball_diameter = [];

var colors = [];

function setup() {
  createCanvas(windowWidth,
               windowHeight);

  ball_num = 500;

  colors.push(color(0, 120, 170));
  colors.push(color(0, 190, 225));
  colors.push(color(120, 215, 240));
  colors.push(color(200, 220, 235));
  colors.push(color(245, 230, 235));
  colors.push(color(250, 210, 210));
  colors.push(color(255, 175, 120));
  colors.push(color(255, 130, 90));

  for (var i = 0; i < ball_num; i = i + 1) {
    ballX[i] = random(0, windowWidth);
    ballY[i] = random(0, windowHeight);
    speedX[i] = random(-2, 2);
    speedY[i] = random(-2, 2);
    ball_color[i] = floor(random(0, colors.length));
    ball_diameter[i] = random(10, 40);
  }
}

function draw() {
  noStroke();

  for (var i = 0; i < ball_num; i++) {
    ballX[i] = ballX[i] + speedX[i];
    ballY[i] = ballY[i] + speedY[i];

    if (ballX[i] > windowWidth || ballX[i] < 0) {
      speedX[i] = -1 * speedX[i];
    }

    if (ballY[i] > windowHeight || ballY[i] < 0) {
      speedY[i] = -1 * speedY[i];
    }
  }

  for (var i = 0; i < ball_num; i++) {
    fill(colors[ball_color[i]]);
    ellipse(ballX[i], ballY[i], ball_diameter[i]);
  }

}