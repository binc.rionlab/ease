var centerXa;
var centerYa;

var centerXb;
var centerYb;

function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);
  
  centerXa = 150;
  centerYa = 150;
  centerXb = centerXa;
  centerYb = centerYa;
}

function draw() {
  centerXa = centerXa + random(-3, 3);
  centerYa = centerYa + random(-3, 3);
 
  stroke(frameCount%256, 200, 0, 100);
  line(centerXa, centerYa, centerXb, centerYb);
  
  centerXb = centerXa;
  centerYb = centerYa; 
}