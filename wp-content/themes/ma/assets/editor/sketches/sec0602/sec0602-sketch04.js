var centerXa;
var centerYa;

function setup() {
  createCanvas(300, 300);
  centerXa = 150;
  centerYa = 150;
}

function draw() {
  background(238, 238, 238);
  noStroke();
 
  centerXa = centerXa + random(-2, 2);
  centerYa = centerYa + random(-2, 2);  
  
  fill(85, 140, 200);
  ellipse(centerXa, centerYa, 120);
}