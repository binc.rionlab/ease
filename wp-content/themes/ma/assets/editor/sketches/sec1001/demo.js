var pointX = [];
var pointY = [];
var speedX = [];
var speedY = [];

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  for (var i = 0; i < 3; i = i + 1) {
    pointX[i] = random(0, windowWidth);
    pointY[i] = random(0, windowHeight);
    speedX[i] = random(-5, 5);
    speedY[i] = random(-5, 5);
  }
}

function draw() {
  background(234, 234, 234, 20);
    
  for (var i = 0; i < 3; i = i + 1) {
    pointX[i] = pointX[i] + speedX[i];
    pointY[i] = pointY[i] + speedY[i];

    if (pointX[i] > windowWidth || pointX[i] < 0) {
      speedX[i] = -speedX[i];
    }
    
    if (pointY[i] > windowHeight || pointY[i] < 0) {
      speedY[i] = -speedY[i];
    }
  }

  drawTriangle();
}

function drawTriangle() {
  stroke(110, 250, 240);
  line(pointX[0], pointY[0], pointX[1], pointY[1]);
  stroke(130, 200, 245)
  line(pointX[1], pointY[1], pointX[2], pointY[2]);
  stroke(150, 145, 250);
  line(pointX[2], pointY[2], pointX[0], pointY[0]);  
}