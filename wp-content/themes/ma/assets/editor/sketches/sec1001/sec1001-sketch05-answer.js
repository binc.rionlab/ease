function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  strokeWeight(3);

  drawTriangle();  
}

function drawTriangle() {  
  line(50, 50, 250, 150);
  line(250, 150, 150, 250);
  line(150, 250, 50, 50);
}