var img;

function preload() {  
  img = loadImage("image0.jpg");
}

function setup() {
  createCanvas(300, 450);
}

function draw() {
  image(img, 0, 0);
}