function setup() {
  createCanvas(300, 500);
  background(250, 250, 250);
  strokeWeight(3);
  noFill();
  for (var i = 0; i < 100; i = i + 1) {
    ellipse(150, 250, i * 10);
  }
}
function draw() {
}