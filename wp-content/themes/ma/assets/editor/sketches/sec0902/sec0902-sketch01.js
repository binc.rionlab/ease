var diameter = [];

function setup() {
  createCanvas(300, 300);
  
  diameter[0] = 5 * 0 + 5;
  diameter[1] = 5 * 1 + 5;
  diameter[2] = 5 * 2 + 5;
}

function draw() {
  background(250, 250, 250);
  noFill();
  
  ellipse(150, 150, diameter[0]);
  ellipse(150, 150, diameter[1]);
  ellipse(150, 150, diameter[2]);
}