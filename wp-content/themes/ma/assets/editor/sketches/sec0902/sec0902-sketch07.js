var rectSize = [];
var R;
var G;
var G;

function setup() {
  createCanvas(300, 300);
  for (var i = 0; i < 100; i = i + 1) {
    rectSize[i] = 3 * i;
  }
}

function draw() {
  background(238, 238, 238);
  noFill();
 
  for (var i = 0; i < 100; i = i + 1) {
    R = 250 - i * 2.5;
    G = 35 + i * 1.5;
    B = 95 + i * 1.5;
    stroke(R, G, B);
    rect(0, 0, rectSize[i], rectSize[i]);
  }
}