var rectSize = [];

function setup() {
  createCanvas(300, 300);
 
  for(var i = 0; i < 3; i = i + 1) {
    rectSize[i] = 5 * i + 5;
  }
}

function draw() {
  background(250, 250, 250);
  noFill();
  
  rect(0, 0, rectSize[0], rectSize[0]); 
  rect(0, 0, rectSize[1], rectSize[1]); 
  rect(0, 0, rectSize[2], rectSize[2]); 
}