var rectSize = [];  

function setup() {
  createCanvas(300, 300);
 
  for (var i = 0; i < 100; i = i + 1) {
    rectSize[i] = 5 * i + 5;
  }
}

function draw() {
  background(250, 250, 250);
  noFill();
    
  for (var i = 0; i < 100; i = i + 1) {
    rect(0, 0, rectSize[i], rectSize[i]);
  }
}