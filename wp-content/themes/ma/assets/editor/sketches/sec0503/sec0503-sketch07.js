function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  
  noFill();
  strokeWeight(3);

  stroke(255, 100, 150);
  line(0, 0, mouseX * 2, mouseY * 2);
  
  stroke(0, 200, 255);
  line(0, 0, mouseX, mouseY); 
}