var widthA;
var widthB;

function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  noStroke();

  widthA = frameCount;
  widthB = widthA * 2;  
  
  fill(220, 180, 55);
  rect(20, 20, widthA, 120)

  fill(255, 225, 55);
  rect(20, 160, widthB, 120);
}