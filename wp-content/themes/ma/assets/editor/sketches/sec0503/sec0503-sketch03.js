var widthA;
var widthB;

function setup() {
  createCanvas(300, 300);

  widthA = 65;
  widthB = widthA * 2;
}

function draw() {
  background(250, 250, 250);
  noStroke();

  fill(220, 180, 55);
  rect(20, 20, widthA, 120)

  fill(255, 225, 55);
  rect(20, 160, widthB, 120);
}