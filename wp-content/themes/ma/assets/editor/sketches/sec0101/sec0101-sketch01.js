function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  line(0, mouseY, 300, mouseY);
  line(mouseX, 0, mouseX, 300);
  ellipse(mouseX, mouseY, 10);

  fill(0, 0, 0);
  text("坐标(" + int(mouseX) + "," + int(mouseY) + ")", 20, 30);
}