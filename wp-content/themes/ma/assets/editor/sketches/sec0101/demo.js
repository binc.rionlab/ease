function setup() {
  createCanvas(windowWidth,
               windowHeight);
}

function draw() {
  val1 = map(mouseX, 0, width, 0, 255);
  val2 = map(mouseY, 0, height, 0, 255);

  background(150 + val2 * 0.6);

  noStroke();

  if (frameCount < 150) {
    fill(200, 200, 200);
    textSize(20);
    text("Touch Me", width / 2 - 50, height / 2);
  }
  else {
    fill(100, 100, 100);
    textSize(15);
    text("Coordinate(" + int(mouseX) + "," + int(mouseY) + ")", mouseX + 10, mouseY - 10);
  }

  stroke(val2 + 100, val1 + 100, 150);
  strokeWeight(10);
  line(0, mouseY, width, mouseY);
  line(mouseX, 0, mouseX, height);
  strokeWeight(1);
  fill(255, 255, 255);
  ellipse(mouseX, mouseY, 13);
}