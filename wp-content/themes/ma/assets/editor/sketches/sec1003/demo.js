function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  noStroke();
}

function moving_circle(diameter) {
  var jMax = 5 / 7 * width / 2;
  for (var j = height; j > jMax; j--) {
    ellipse(width / 2, j, diameter);
  }
}

function draw() {
  fill(1, 145, 200);
  moving_circle(5 / 7 * width);

  fill(210, 60, 105);
  moving_circle(3 / 7 * width);

  fill(245, 205, 0);
  moving_circle(1 / 7 * width);
}