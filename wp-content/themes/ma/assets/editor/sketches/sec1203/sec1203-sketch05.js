var img;

function preload() {  
  img = loadImage("image1.jpg");
}

function setup() {
  createCanvas(300, 300);
}

function draw() {
  image(img, 0, 0);
  filter(INVERT); 
}