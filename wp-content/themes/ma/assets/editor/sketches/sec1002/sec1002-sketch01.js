function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  noStroke();

  drawEye();
}

function drawEye() {
  fill(0, 0, 0);
  ellipse(150, 150, 130);

  fill(255, 255, 255);
  ellipse(150, 150, 110);

  fill(0, 0, 0);
  ellipse(150, 150, 70);  

  fill(255, 255, 255);
  ellipse(150 + 7, 150, 10);
  ellipse(150 + 30, 150, 25);
}