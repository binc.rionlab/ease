function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  noStroke();

  drawEye(75);
  drawEye(225);
}

function drawEye(eyeX) {
  fill(0, 0, 0);
  ellipse(eyeX, 150, 130);

  fill(255, 255, 255);
  ellipse(eyeX, 150, 110);

  fill(0, 0, 0);
  ellipse(eyeX, 150, 70);

  fill(255, 255, 255);
  ellipse(eyeX + 7, 150, 10);
  ellipse(eyeX + 30, 150, 25);
}