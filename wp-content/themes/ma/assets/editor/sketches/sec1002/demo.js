var time;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  noFill();

  time = 0.0;
}

function draw() {
  background(234, 234, 234);

  var num = 20;
  for (var i = 0; i < num; i = i + 1) {
    var R = i * (240 / num);
    var G = 160 - i * (125 / num);
    var B = 250 - i * (155 / num);
    stroke(R, G, B);

    var x = windowWidth / 2;
    var y = windowHeight / 2;
    var spX = 0.5 * i * windowWidth / num;
    var spY = 0.5 * i * windowHeight / num;
    // var dia = spX;
    var dia = spX * sin(0.1 * i + time);
    stamp(x, y, spX, spY, dia);
  }

  time = time + 0.01;
}

function stamp(x, y, spaceX, spaceY, diameter) {
  ellipse(x - spaceX, y - spaceY, diameter);
  ellipse(x - spaceX, y + spaceY, diameter);
  ellipse(x + spaceX, y - spaceY, diameter);
  ellipse(x + spaceX, y + spaceY, diameter);
}