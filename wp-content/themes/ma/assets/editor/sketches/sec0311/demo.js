function setup() {
  createCanvas(windowWidth,
               windowHeight);
}

function draw() {
  background(234, 234, 234);

  var from = color(0, 200, 255, 150);
  var to = color(255, 100, 150, 150);

  noStroke();

  fill(lerpColor(from, to, 0.2));
  rect(1 / 7 * width, height / 2 - 1 / 7 * height, 2 / 7 * width, 2 / 7 * height);

  fill(lerpColor(from, to, 0.5));
  rect(2.5 / 7 * width, height / 2 - 1.5 / 7 * height, 2.3 / 7 * width, 2.3 / 7 * height);

  fill(lerpColor(from, to, 0.8));
  rect(3.5 / 7 * width, height / 2, 2.4 / 7 * width, 1.5 / 7 * height);
}