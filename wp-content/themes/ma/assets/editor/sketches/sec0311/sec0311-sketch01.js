function setup() {
  createCanvas(300, 300);
  strokeWeight(30);
}

function draw() {
  background(250, 250, 250);
 
  noStroke();
  fill(210, 70, 70);
  rect(150, 30, 120, 240);
 
  fill(80, 165, 215);
  rect(30, 42, 210, 64);
 
  fill(80, 165, 215);
  rect(30, 117, 210, 64);
 
  fill(80, 165, 215);
  rect(30, 192, 210, 64);
}