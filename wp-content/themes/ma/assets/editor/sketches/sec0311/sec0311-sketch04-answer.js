function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(20, 20, 20);
  noStroke();
  
  fill(255, 0, 0, 50);
  ellipse(150, 150, 250);
  ellipse(150, 150, 200);
  ellipse(150, 150, 150);
  ellipse(150, 150, 100);
}