function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  noStroke();

  fill(254, 172, 94, 20);
  ellipse(150, 150, 250);
  fill(247, 165, 108, 40);
  ellipse(150, 150, 220);
  fill(240, 159, 122, 60);
  ellipse(150, 150, 190);
  fill(233, 152, 136, 80);
  ellipse(150, 150, 160);
  fill(226, 146, 151, 100);
  ellipse(150, 150, 130);
  fill(220, 140, 165, 120);
  ellipse(150, 150, 100);
  fill(212, 133, 179, 140);
  ellipse(150, 150, 70);
  fill(205, 127, 193, 160);
  ellipse(150, 150, 40);
  fill(199, 121, 408, 180);
  ellipse(150, 150, 10);
}