function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  strokeWeight(30);
  stroke(145, 200, 200);
  fill(190, 45, 60);
  ellipse(150, 150, 200);
}