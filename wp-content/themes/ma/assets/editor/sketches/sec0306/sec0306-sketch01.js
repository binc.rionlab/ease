function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  strokeWeight(30);
  stroke(150, 180, 200);
  rect(40, 40, 220, 220);
}