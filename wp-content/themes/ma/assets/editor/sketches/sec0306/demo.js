function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
}

function draw() {
  var from = color(0, 200, 255);
  var to = color(255, 100, 150);

  for (var i = 0.0; i < width; i = i + 30.0) {
    for (var j = 0.0; j < height; j = j + 30.0) {
      stroke(238);
      fill(lerpColor(from, to, (i + j) / (width + height)));
      rect(i, j, 30, 30);
    }

  }
}