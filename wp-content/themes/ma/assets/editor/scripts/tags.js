
riot.tag2('app', '<div class="wrap-app"> <content></content> </div>', 'app,[data-is="app"]{display:block;width:100%;margin:0 auto}', '', function(opts) {
  var self = this;

  this.contents = {
    test: {
      title: 'title',
      body: 'body',
      label: 'link label',
    },
  };
});

riot.tag2('item-download-sketch', '<div class="wrap-item"> <div class="wrap-icon f fh" onclick="{download}"><img src="./img/icons/icon-camera.svg"></div> </div>', 'item-download-sketch,[data-is="item-download-sketch"]{display:block;width:36px} item-download-sketch .wrap-icon,[data-is="item-download-sketch"] .wrap-icon{cursor:pointer;width:100%;height:100%} item-download-sketch .wrap-icon img,[data-is="item-download-sketch"] .wrap-icon img{width:100%;height:100%}', '', function(opts) {
  var self = this;

  this.on('mount', function(){

  });

  this.Base64toBlob = function(base64) {

    var tmp = base64.split(',');

    var data = atob(tmp[1]);

    var mime = tmp[0].split(':')[1].split(';')[0];

    var buf = new Uint8Array(data.length);
    for (var i = 0; i < data.length; i++) {
      buf[i] = data.charCodeAt(i);
    }

    var blob = new Blob([buf], { type: mime });
    return blob;
  }

  this.download = function(){

    var canvasIframe = document.getElementById("sketcheFrame");

    var innerCanvas = canvasIframe.contentWindow.canvas;

    if(!innerCanvas){
      alert('コードを再生してください。');
      return;
    }

    var base64 = innerCanvas.toDataURL("image/png");

    $('.wrap-screenshot').toggle(true);
    document.getElementById('screenshot').src = base64;

    $("#screenshot").css("opacity", "0.1");
    $("#screenshot").css("filter", "alpha(opacity=10)");
    $("#screenshot").fadeTo("middle", 1.0);

  }
});

riot.tag2('item-run', '<div class="wrap-item"> <div class="wrap-icon f fh" onclick="{run}"><img riot-src="{src}" id="playIcon"></div> </div>', 'item-run,[data-is="item-run"]{display:block;width:36px} item-run .wrap-icon,[data-is="item-run"] .wrap-icon{cursor:pointer;width:100%;height:100%} item-run .wrap-icon img,[data-is="item-run"] .wrap-icon img{width:100%;height:100%}', '', function(opts) {
  var self = this;

  this.src = './img/icons/icon-play.svg';

  this.on('mount', function(){

  });

  this.run = function(){

    if (EDITOR_GLOB.state == EDITOR_GLOB.STATE_USER) {
      EDITOR_GLOB.userCode = EDITOR_GLOB.flask.textarea.value;
    }

    EDITOR_GLOB.sourceCode = EDITOR_GLOB.flask.textarea.value;

    $('#sketcheFrame')[0].src = './sketche.html';

    $('html,body').animate({scrollTop: 0}, 400, 'easeOutCubic');

    $('.wrap-sketche').fadeIn(600);
    $('#editor').toggle(false);

    self.setPlaySrc('./img/icons/icon-replay.svg');

    $('.wrap-screenshot').toggle(false);
  }

  riot.runCode = this.run;

  this.setPlaySrc = function(src){
    self.src = src;
    self.update();
  }

  riot.setPlaySrc = this.setPlaySrc;
});

riot.tag2('item-show-answer', '<div class="wrap-item"> <div class="wrap-icon f fh" onclick="{showAnswer}"><img src="./img/icons/icon-answer.svg"></div> </div>', 'item-show-answer,[data-is="item-show-answer"]{display:block;width:110px} item-show-answer .wrap-icon,[data-is="item-show-answer"] .wrap-icon{cursor:pointer;width:100%;height:100%} item-show-answer .wrap-icon img,[data-is="item-show-answer"] .wrap-icon img{width:100%;height:100%}', '', function(opts) {
  var self = this;

  this.on('mount', function(){
    if(window.frameElement){
      var dataAnswer = window.frameElement.dataset.answer;
      var hideAnswer = window.frameElement.dataset.hideAnswer;
      if(hideAnswer || dataAnswer==undefined){
        self.unmount();
      }
    }
  });

  this.showAnswer = function(){

    if (EDITOR_GLOB.state == EDITOR_GLOB.STATE_USER) {
      EDITOR_GLOB.userCode = EDITOR_GLOB.flask.textarea.value;
    }

    EDITOR_GLOB.state = EDITOR_GLOB.STATE_ANSWER;

    $('#editor').toggle(true);

    $('.wrap-sketche').fadeOut(600);

    $('.wrap-screenshot').toggle(false);

    EDITOR_GLOB.flask.update(EDITOR_GLOB.answerCode);
  }
});

riot.tag2('item-stop', '<div class="wrap-item"> <div class="wrap-icon f fh" onclick="{stop}"><img src="./img/icons/icon-code.svg"></div> </div>', 'item-stop,[data-is="item-stop"]{display:block;width:36px} item-stop .wrap-icon,[data-is="item-stop"] .wrap-icon{cursor:pointer;width:100%;height:100%} item-stop .wrap-icon img,[data-is="item-stop"] .wrap-icon img{width:100%;height:100%}', '', function(opts) {
  this.on('mount', function(){

  });

  this.stop = function(){

    if (EDITOR_GLOB.state == EDITOR_GLOB.STATE_USER) {
      EDITOR_GLOB.userCode = EDITOR_GLOB.flask.textarea.value;
    }

    EDITOR_GLOB.state = EDITOR_GLOB.STATE_USER;

    $('.wrap-sketche').fadeOut(600);

    $('#editor').toggle(true);

    $('.wrap-screenshot').toggle(false);

    EDITOR_GLOB.flask.update(EDITOR_GLOB.userCode);

    riot.setPlaySrc('./img/icons/icon-play.svg');
  }

  riot.stopCode = this.stop;
});

riot.tag2('module-editor', '<div class="wrap-module"> <div id="editor"></div> </div>', 'module-editor,[data-is="module-editor"]{display:block} module-editor #editor,[data-is="module-editor"] #editor{width:100%;height:calc(100vh - 52px);position:relative;top:52px;background:#F9f9f9} module-editor #editor .CodeMirror,[data-is="module-editor"] #editor .CodeMirror{height:auto;font-size:18px;background:#f9f9f9}', '', function(opts) {
  this.on('mount', function(){

    EDITOR_GLOB.editor = CodeMirror(document.getElementById('editor'), {
      value: 'Inertia P5 Editor',
      lineNumbers: true,
      mode: "javascript",
      keyMap: "sublime",
      autoCloseBrackets: true,
      matchBrackets: true,
      showCursorWhenSelecting: true,
      theme: "neo",
      tabSize: 2,
      lineWrapping: true,
      inputStyle: 'contenteditable',
    });

    var path;
    if(window.frameElement){
      path = window.frameElement.dataset.path;

      EDITOR_GLOB.mode = window.frameElement.dataset.mode;
    }else{
      path = './sketches/noSketch.js';
    }

    var callback = function(_data) {
      console.log(_data);
      EDITOR_GLOB.sourceCode = _data;

      EDITOR_GLOB.editor.setValue(_data);
    }

    EDITOR_GLOB.loadJs(path, callback);

    callback = function(_data){
      EDITOR_GLOB.stopSource = _data;
    }
    path = './sketches/stopSketche.js';
    EDITOR_GLOB.loadJs(path, callback);

  });
});

riot.tag2('module-editor2', '<div class="wrap-module"> <div id="editor"></div> </div>', 'module-editor2,[data-is="module-editor2"]{display:block} module-editor2 #editor,[data-is="module-editor2"] #editor{width:100%;height:calc(100vh - 52px);position:relative;top:52px;background:#eeeeee} module-editor2 .CodeFlask .CodeFlask__textarea,[data-is="module-editor2"] .CodeFlask .CodeFlask__textarea{font-size:20px} module-editor2 .CodeFlask .CodeFlask__pre code,[data-is="module-editor2"] .CodeFlask .CodeFlask__pre code{font-size:20px} module-editor2 .CodeFlask .add-offset,[data-is="module-editor2"] .CodeFlask .add-offset{position:relative !important;left:2px !important}', '', function(opts) {
  this.on('mount', function(){

    EDITOR_GLOB.flask = new CodeFlask,
        flasks = new CodeFlask;

    EDITOR_GLOB.flask.run('#editor', {
      language: 'js',
    });

    EDITOR_GLOB.flask.onUpdate(function(code){

    });

    EDITOR_GLOB.flask.update('var goo = "stick"');

    var path, answerPath;
    if(window.frameElement){
      path = window.frameElement.dataset.path;
      answerPath = window.frameElement.dataset.answer;

      EDITOR_GLOB.mode = window.frameElement.dataset.mode;
      console.log(EDITOR_GLOB.mode);
    }else{
      path = './sketches/noSketch.js';
      answerPath = './sketches/noSketch.js';
    }

    console.log(path);

    EDITOR_GLOB.sourceCode = "";

    EDITOR_GLOB.userCode = "";

    EDITOR_GLOB.answerCode = "";

    EDITOR_GLOB.STATE_USER = 0;
    EDITOR_GLOB.STATE_ANSWER = 1;
    EDITOR_GLOB.state = EDITOR_GLOB.STATE_USER;

    var callback = function(_data) {

      EDITOR_GLOB.userCode = _data;
      EDITOR_GLOB.flask.update(_data);
    }
    EDITOR_GLOB.loadJs(path, callback);

    callback = function(_data) {
      EDITOR_GLOB.answerCode = _data;
    }
    EDITOR_GLOB.loadJs(answerPath, callback);

    callback = function(_data){
      EDITOR_GLOB.stopSource = _data;
    }
    path = './sketches/stopSketche.js';
    EDITOR_GLOB.loadJs(path, callback);

    if(window.frameElement){
      var firstDisplay = window.frameElement.dataset.firstDisplay;
      if(firstDisplay=='sketch'){
        setTimeout(function(){
          riot.runCode();
        }, 800);
      }
    }

    if(window.frameElement){
      var dataAnswer = window.frameElement.dataset.answer;
      var hideAnswer = window.frameElement.dataset.hideAnswer;
      var isTop = window.frameElement.dataset.istop;
      var isEditorOnly = window.frameElement.dataset.isEditorOnly;

      if(hideAnswer || dataAnswer==undefined){
        if (isTop) {
          $("#editor").css("height", "calc(100vh - 112px)");
        }

        if (isEditorOnly) {

        }
      }

      else {
        $("#editor").css("height", "calc(100vh - 104px)");
      }
    }

    if(navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)){
      $('.CodeFlask .CodeFlask__pre').addClass('add-offset');
    }

  });
});

riot.tag2('module-screenshot', '<div class="wrap-module"> <div class="screenshotFrame"> <div class="fb"> <p id="message-before">画像を長押しして保存してください。</p><img id="screenshot"> <p id="message-after">画像を長押しして保存してください。</p> </div> </div> </div>', 'module-screenshot .screenshotFrame,[data-is="module-screenshot"] .screenshotFrame{width:100%;height:calc(100vh - 52px);position:relative;top:0;margin:0;overflow:hidden;display:flex;align-items:center;justify-content:center;background:#eeeeee} module-screenshot .fb,[data-is="module-screenshot"] .fb{text-align:center} module-screenshot .message-before,[data-is="module-screenshot"] .message-before{pointer-events:none} module-screenshot .message-after,[data-is="module-screenshot"] .message-after{pointer-events:none} module-screenshot img,[data-is="module-screenshot"] img{border-radius:5px;border-color:#282828;border-style:dotted;border-width:thin}', '', function(opts) {
  this.on('mount', function(){
    if(window.frameElement){
      var dataAnswer = window.frameElement.dataset.answer;
      var hideAnswer = window.frameElement.dataset.hideAnswer;
      var isTop = window.frameElement.dataset.istop;
      var isEditorOnly = window.frameElement.dataset.isEditorOnly;

      if(hideAnswer || dataAnswer==undefined){

        if (isTop) {
          $('#message-before').css("display", "");
          $('#message-before').css("padding", "10px");
          $("#screenshot").css("height", "60vh");
          $('#message-after').css("display", "none");
          $('.fb').css("margin-bottom", "175px")
        }

        else if (isEditorOnly) {
          $('#message-before').css("display", "none");
          $("#screenshot").css("width", "60%");
          $('#message-after').css("display", "");
        }

        else {
          $('#message-before').css("display", "none");
          $("#screenshot").css("height", "250px");
          $('#message-after').css("display", "");
        }
      }

      else {

        if (isTop) {

        }

        else if (isEditorOnly) {

        }

        else {
          $('#message-before').css("display", "none");
          $("#screenshot").css("height", "250px");
          $('#message-after').css("display", "");
          $(".screenshotFrame").css("height", "calc(100vh - 104px)");
        }
      }
    }
  });
});

riot.tag2('module-task-bar', '<div class="wrap-module f flex-right"> <div class="wrap-icon mr12 f fh"> <item-show-answer></item-show-answer> </div> </div>', 'module-task-bar,[data-is="module-task-bar"]{display:block;position:fixed;bottom:0;left:0;width:100%;height:52px;z-index:100;background:#f9f9f9}', 'class="py8"', function(opts) {
  var self = this;

  this.on('mount', function(){
    if(window.frameElement){
      var dataAnswer = window.frameElement.dataset.answer;
      var hideAnswer = window.frameElement.dataset.hideAnswer;
      if(hideAnswer || dataAnswer==undefined){
        self.unmount();
      }
    }
  });
});

riot.tag2('module-title', '<div class="wrap-module"> <p class="py12">エディタのタイトル</p> </div>', 'module-title,[data-is="module-title"]{display:block} module-title .wrap-module,[data-is="module-title"] .wrap-module{background:#fbf1d3} module-title .wrap-module p,[data-is="module-title"] .wrap-module p{text-align:center;font-weight:bold}', '', function(opts) {
  this.on('mount', function(){

  });
});

riot.tag2('module-tool-bar', '<div class="wrap-module f flex-between"> <div class="wrap-title f fh ml16"> <p>{title}</p> </div> <div class="wrap-icon mr12 f flex-between"> <item-run></item-run> <item-stop></item-stop> <item-download-sketch></item-download-sketch> </div> </div>', 'module-tool-bar,[data-is="module-tool-bar"]{display:block;position:fixed;top:0;left:0;z-index:100;background:#f9f9f9;width:100%} module-tool-bar .wrap-title,[data-is="module-tool-bar"] .wrap-title{background:#f9f9f9} module-tool-bar .wrap-title p,[data-is="module-tool-bar"] .wrap-title p{font-family:\'Mplus 1p\';font-weight:500;letter-spacing:2px;font-size:14px}', 'class="py8"', function(opts) {
  this.on('mount', function(){
    try {
      this.title = window.frameElement.dataset.title;
    }
    catch (e) {
      this.title = 'NO TITLE';
    }
  });
});

riot.tag2('page-sample', '<div class="wrap-page"> <module-tool-bar></module-tool-bar> <module-editor2></module-editor2> <div class="wrap-sketche"> <item-stop></item-stop> <iframe src="" id="sketcheFrame" frameborder="0"></iframe> </div> </div>', 'page-sample,[data-is="page-sample"]{display:block;width:100%;margin:0 auto} page-sample .wrap-page .wrap-sketche,[data-is="page-sample"] .wrap-page .wrap-sketche{display:none;position:fixed;z-index:100;width:100%;height:100vh;top:0} page-sample .wrap-page .wrap-sketche iframe,[data-is="page-sample"] .wrap-page .wrap-sketche iframe{background:#eee;width:100%;height:100%}', '', function(opts) {
});

riot.tag2('page-top', '<div class="wrap-page"> <module-tool-bar></module-tool-bar> <module-editor2 filename="{fileName}"></module-editor2> <div class="wrap-screenshot"> <module-screenshot></module-screenshot> </div> <div class="wrap-sketche"> <iframe src="" id="sketcheFrame" frameborder="0"></iframe> </div> <module-task-bar></module-task-bar> </div>', 'page-top,[data-is="page-top"]{display:block;width:100%;margin:0 auto} page-top .wrap-page .wrap-sketche,[data-is="page-top"] .wrap-page .wrap-sketche{display:none;position:fixed;z-index:100;width:100%;height:calc(100vh - 52px);top:52px} page-top .wrap-page .wrap-sketche iframe,[data-is="page-top"] .wrap-page .wrap-sketche iframe{background:#eee;width:100%;height:100%} page-top .wrap-page .wrap-screenshot,[data-is="page-top"] .wrap-page .wrap-screenshot{display:none;position:fixed;z-index:200;width:100%;height:calc(100vh - 52px);top:52px}', '', function(opts) {
  this.fileName = opts.content.targetName;

  this.on('mount', function(){
    if(window.frameElement){
      var dataAnswer = window.frameElement.dataset.answer;
      var hideAnswer = window.frameElement.dataset.hideAnswer;
      var isTop = window.frameElement.dataset.istop;
      var isEditorOnly = window.frameElement.dataset.isEditorOnly;

      if(hideAnswer || dataAnswer==undefined){
        if (isTop) {
          $("#sketcheFrame").css("height", "calc(100vh - 112px)");
        }

        if (isEditorOnly) {

        }
      }

      else {
        $(".wrap-sketche").css("height", "calc(100vh - 104px)");
        $(".wrap-screenshot").css("height", "calc(100vh - 104px)");
      }
    }
  });
});