# Enertia P5 Editor
P5.jsのサンプルプログラムを埋め込むエディターです。

## Set Up
`editor`ファイルを埋め込みたいコンテンツをレスポンスするサーバーにアップロードしてください。

### Example
```
 www
  |
  |____editor
  |
  |____index.html
```

基本的にこれで完了です。何かプラグインを追加する必要はありません。

## Usage
`iframe`を用いて任意の`sketches`ディレクトリ内のjsファイルをエディターに読み込ませることできます。
```
<iframe src='{your_uploaded_path}/editor.html' data-path='./sketches/{target_path}' data-title='{title_of_this_sketche}' data-mode='{mode_of_this_sketche}'></iframe>
```

- your_uploaded_path
アップロードした領域のパスです。
例えば、上の例のindex.htmlから読み込む場合、
`<iframe src='./public/editor.html' data-path='./sketches/stopSketche.js' data-title='直線を書く'></iframe>`
で読み込めます。

- target_path
読み込むjsのファイル名です。拡張子込みで記述してください。

- title_of_this_sketche
サンプルのタイトルです。エディターの左上に表示される文字列を指定します。何もしない場合「NO TITLE」が挿入されます。

- mode_of_this_sketche
jsのコンパイルモードです。`USE NO SETUP AND DRAW`を指定すると、`setup()`と`draw()`を用いないsketcheファイルを読み込むことができます。


### Example
iframeTest.htmlの中身です。ブラウザで表示すると出力が確認できます。

```
<!DOCTYPE html>

<html>

<head>
  <title>Iframe Test</title>
  <style>
    body {
      margin: 0;
      padding: 40px;
    }
    iframe {
      width: 100%;
      height: 90vh;
    }
  </style>

  <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
</head>
<body>


  <iframe src='./editor.html' data-path='./sketches/stopSketche.js' data-title='直線を書く'></iframe>
  <iframe src='./editor.html' data-path='./sketches/test/noDraw.js' data-title='直線を書く' data-mode='USE NO SETUP AND DRAW'></iframe>


</body>
</html>
```

## Custom Css
`./plugins/custom.css`を編集するとcssを追加できます。
クラス名や要素の配置はインスペクタで参照してください。
