var sound1;
var sound2;
var sound3;

function preload() {
  sound = loadSound("sound4.mp3");
}

function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  
  if (mouseIsPressed == true) {
    playSound();
  }
  
  noStroke();
  
  fill(208, 173, 167);
  ellipse(45, 150, 20);
  
  fill(173, 106, 108);
  ellipse(90, 150, 50);
  
  fill(93, 46, 70);
  ellipse(200, 150, 150);
}

function playSound(x, y) {
  if (dist(mouseX, mouseY, 45, 150) < 10) {
    sound.rate(2.0);    
    sound.play();
  }
  
  if (dist(mouseX, mouseY, 90, 150) < 25) {
    sound.rate(1.0);
    sound.play();
  }
  
  if (dist(mouseX, mouseY, 200, 150) < 75) {
    sound.rate(0.5);
    sound.play();
  }
}


// 
// 声音数据
// 
// 新年钟声
// written by NaruIDEA
// https://dova-s.jp/se/play1136.html
// 