var img;

var time;

function preload() {
  img = loadImage("images/room.jpg");
}

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  imageMode(CENTER);

  time = 1;
}

function draw() {
  img.resize(windowWidth / time, windowHeight / time);
  image(img, windowWidth / 2, windowHeight / 2);

  time = time + 1;

  if (time > 10) {
    noLoop();
  }
}

// Unsplash
// https://unsplash.com/photos/RnCPiXixooY