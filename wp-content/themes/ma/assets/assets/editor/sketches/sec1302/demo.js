var X;
var Y;

var prevX;
var prevY;

var slider1;
var slider2;
var slider3;

function setup() {
  createCanvas(300, 300);

  X = width;
  Y = height - 140;

  prevX = X;
  prevY = Y;

  angleMode(DEGREES);

  slider1 = createSlider(1, 10, 9);
  slider1.position(80, height - 80);
  slider2 = createSlider(1.0, 10.0, 5.0);
  slider2.position(80, height - 50);
  slider3 = createSlider(0, 360, 120);
  slider3.position(80, height - 20);
}

function draw() {
  background(234, 234, 234);

  console.log(slider1.width);
  fill(0, 0, 0);
  text("slider1.value : " + slider1.value(), 160, height - 95);
  text("slider2.value : " + slider2.value(), 160, height - 65);
  text("slider3.value : " + slider3.value(), 160, height - 35);

  translate(width / 2, height / 2 - 70);

  for (var t = 0; t < 360; t = t + 1) {
    var val1 = slider1.value();
    var val2 = slider2.value();
    var val3 = slider3.value();
    var x = 0.5 * X * cos(val1 * t);
    var y = 0.5 * Y * sin(val2 * t - val3);
    strokeWeight(2);
    line(x, y, prevX, prevY);
    prevX = x;
    prevY = y;
  }
}
