var diaSlider;
var diameter;

var GSlider;

function setup() {
  createCanvas(300, 300);

  diaSlider = createSlider(0, 300, 150);  
  diaSlider.position(80, 40);  

  GSlider = createSlider(0, 255, 200);  
  GSlider.position(80, 70);  
}

function draw() {
  background(250, 250, 250);
 
  diameter = diaSlider.value();  

  noStroke();
  fill(250, 200, 0);
  ellipse(150, 150, diameter);

  fill(0, 0, 0);
  text(diameter, 160, 30);
}