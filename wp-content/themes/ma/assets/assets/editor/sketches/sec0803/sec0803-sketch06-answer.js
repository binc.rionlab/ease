function setup() {
  createCanvas(300, 500);
  background(250, 250, 250);
  noFill();

  for (var i = 0; i < 1000; i = i + 1) {  
    stroke(random(100, 255), random(50, 255), random(0, 255));
    ellipse(150, 250, i * 2);
  }
}

function draw() {

}