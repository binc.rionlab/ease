function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
  strokeWeight(3);
  noFill();

  for (var i = 0; i < 3; i = i + 1) {
    ellipse(150, 150, i * 100);
  }
}

function draw() {

}