var ellipseX;
var ellipseY;
var diameter;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  strokeWeight(3);
  background(234, 234, 234);
}

function draw() {
  ellipseX = random(0, width);
  ellipseY = random(0, height);
  diameter = random(0, 30);

  if (ellipseX < 1 / 3 * width) {
    fill(160, 230, 0);
  }
  else if (ellipseX < 2 / 3 * width) {
    fill(0, 160, 250);
  }
  else if (ellipseX < width) {
    fill(240, 35, 95);
  }

  ellipse(ellipseX, ellipseY, diameter);
}