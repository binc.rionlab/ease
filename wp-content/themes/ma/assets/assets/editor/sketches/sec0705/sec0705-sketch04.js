var ellipseX;
var ellipseY;
var diameter;

function setup() {
  createCanvas(300, 300);
  stroke(255, 255, 255, 100);
  background(238, 238, 238);
}

function draw() {
  ellipseX = random(0, 300);
  ellipseY = random(0, 300);
  diameter = random(0, 50);

  if (ellipseX < 100) {
    fill(245, 175, 250, 200);
  }
  else if (ellipseX < 200) {
    fill(180, 135, 235, 200);
  }
  else if (ellipseX < 300) {
    fill(130, 145, 240, 200);
  }

  ellipse(ellipseX, ellipseY, diameter);
}