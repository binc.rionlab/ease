var X;
var Y;
var time;
var param = [];

var resetButton;

function setup() {
  createCanvas(300, 300);
  X = width / 2;
  Y = height / 2 - 30;
  
  resetButton = createButton("Press Me");
  resetButton.position(170, 280);
  resetButton.mousePressed(reset);

  reset();
}

function reset() {
  param[0] = random(1, 16); 
  param[1] = random(1, 16); 
  param[2] = random(1, 16); 
  param[3] = random(1, 16);
}

function draw() {
  background(234, 234, 234);
  
  fill(234, 234, 234);
  rect(0, 0, width - 1, height - 60 - 1);
  
  lines(param[0], 0, 0);
  lines(param[1], X, 0);
  lines(param[2], 0, Y);
  lines(param[3], X, Y);

  time = time + 1;
}

function lines(mode, x, y) {
  var num = 10;
  var spanX = X / num;
  var spanY = Y / num;

  for (var i = 0; i <= num; i++) {
    if (mode & 0b0001) {
      line(x + spanX * i, y, x + spanX * i, y + Y);
    }

    if (mode & 0b0010) {
      line(x, y + spanY * i, x + X, y + spanY * i);
    }

    if (mode & 0b0100) {
      line(x + spanX * i, y, x, y + spanY * i);
      if ( i != 0) line(x + spanX * i, y + Y, x + X, y + spanY * i);
    }

    if (mode & 0b1000) {
      line(x + spanX * i, y, x + X, y - spanY * i + Y);
      if (i != num) line(x, y - spanY * i + Y, x + spanX * i, y + Y);
    }
  }
}