var ballX = [];
var ballY = [];
var speedX = [];
var speedY = [];
var ballNum;

var button;

function setup() {
  createCanvas(300, 300);
  noStroke();
  
  ballNum = 0;
  
  for (var i = 0; i < 5; i = i + 1) {
    addBall();
  }
  
  button = createButton("add");
  button.position(20, 20);
  button.mousePressed(addBall);
}

function draw() {
  background(238, 238, 238);
  
  for (var i = 0; i < ballNum; i = i + 1) {
    ballX[i] = ballX[i] + speedX[i];
    ballY[i] = ballY[i] + speedY[i];

    if (ballX[i] < 0 || ballX[i] > 300) {
      speedX[i] = -speedX[i];
    }
    
    if (ballY[i] < 0 || ballY[i] > 300) {
      speedY[i] = -speedY[i];
    }
    
    fill(100, 200, 255, 100);
    ellipse(ballX[i], ballY[i], 100);
  }
}

function addBall() {
  ballX[ballNum] = random(0, 300);
  ballY[ballNum] = random(0, 300);
  speedX[ballNum] = random(-2, 2);
  speedY[ballNum] = random(-2, 2);
  ballNum = ballNum + 1;
}