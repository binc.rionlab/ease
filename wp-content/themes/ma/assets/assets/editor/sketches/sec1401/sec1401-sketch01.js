var sound;
var startButton;

function preload() {
  sound = loadSound("sound0.mp3");
}

function setup() {
  createCanvas(300, 300);
  startButton = createButton("Press to start sound");
  startButton.position(70, 100);
  startButton.mousePressed(start);
}

function draw() {
  fill(0, 0, 0);
  text("Congratulations!", 20, 150);
}

function start() {
  sound.play();
}

// 
// 声音数据
// 
// 揭晓音效
// written by NaruIDEA
// https://dova-s.jp/se/play1149.html
// 