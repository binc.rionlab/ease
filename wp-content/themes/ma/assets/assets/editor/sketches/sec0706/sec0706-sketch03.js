var ellipseX;
var ellipseY;

function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
}

function draw() {
  ellipseX = random(0, 300);
  ellipseY = random(0, 300);

  if (ellipseX > 100 && ellipseX < 200) {
    fill(215, 35, 30);
    ellipse(ellipseX, ellipseY, random(10, 20));
  }

  if (ellipseY > 100 && ellipseY < 200) {
    fill(45, 150, 180);
    ellipse(ellipseX, ellipseY, random(10, 20));
  }

  if (ellipseX > 100 && ellipseX < 200 && ellipseY > 100 && ellipseY < 200) {
    fill(250, 180, 0);
    ellipse(ellipseX, ellipseY, random(10, 20));
  }

}