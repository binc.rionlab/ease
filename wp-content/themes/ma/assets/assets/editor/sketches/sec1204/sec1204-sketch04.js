var colors = [];

function setup() {
  createCanvas(300, 300);
  background(238, 238, 238);
  noStroke();
}

function draw() {
  if (mouseY < 30) {
    colors = get(mouseX, mouseY);
  }
  
  fill(colors[0], colors[1], colors[2]);
  ellipse(random(0, 300), random(0, 300), random(5, 30));
  
  fill(65, 211, 189);
  rect(0, 0, 60, 30);

  fill(250, 197, 85);
  rect(60, 0, 60, 30);
  
  fill(235, 114, 133);
  rect(120, 0, 60, 30);
  
  fill(229, 190, 237);
  rect(180, 0, 60, 30);
  
  fill(252, 247, 241);
  rect(240, 0, 60, 30);
}