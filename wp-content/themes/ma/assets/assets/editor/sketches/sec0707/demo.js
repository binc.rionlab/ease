var ellipseX;
var ellipseY;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  background(234, 234, 234);
  noStroke();
}

function draw() {
  ellipseX = random(0, width);
  ellipseY = random(0, height);
  diameter = random(0, 30);

  if (ellipseX < 1 / 3 * width) {
    noStroke();
    fill(255, 255, 0, 150);
    ellipse(ellipseX, ellipseY, diameter);
  }

  if (ellipseX > 2 / 3 * width) {
    noStroke();
    fill(100, 255, 230, 150);
    ellipse(ellipseX, ellipseY, diameter);
  }

  if (ellipseX > 2 / 3 * width ||
      ellipseX < 1 / 3 * width) {
    stroke(20, 20, 20);
    noFill();
    ellipse(ellipseX, ellipseY, diameter);
  }

}