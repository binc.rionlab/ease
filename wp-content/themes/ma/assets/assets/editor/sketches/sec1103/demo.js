function setup() {
  createCanvas(windowWidth,
               windowHeight);
}

function draw() {
  background(234, 234, 234);
  for (var i = 0; i < windowHeight; i = i + 1) {
    var R = 240 * i / windowHeight;
    var G = 160 - 125 * i / windowHeight;
    var B = 250 - 165 * i / windowHeight;
    fill(R, G, B, 200);
    textSize(i);
    text("?", 0, windowHeight);
  }
}