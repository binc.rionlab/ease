function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  for (var i = 0; i < 300; i = i + 1) {
      var R = 240 * i / 300;
      var G = 160 - 125 * i / 300;
      var B = 250 - 165 * i / 300;
      fill(R, G, B, 200);
      textSize(i);
      text("月", 0, 300);
  }
}