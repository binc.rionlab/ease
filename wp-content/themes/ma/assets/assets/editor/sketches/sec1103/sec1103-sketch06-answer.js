function setup() {
  createCanvas(300, 300);
  background(250, 250, 250);
  fill(0, 0, 0);
}

function draw() {
  strokeWeight(3);
  textSize(random(0, 100));
  noStroke();
  fill(0, random(100, 255), random(200, 255), random(0, 255));
  text("Hi", random(0, 300), random(0, 300));
}