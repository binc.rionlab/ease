function setup() {
  createCanvas(300, 200);
}

function draw() {
  background(250, 250, 250);

  textSize(140);
 
  strokeWeight(10);
  stroke(0, 0, 0);
  fill(255, 200, 0);

  textStyle(NORMAL);
  text("F", 10, 150);

  textStyle(BOLD);
  text("F", 100, 150);

  textStyle(ITALIC);
  text("F", 180, 150);
}