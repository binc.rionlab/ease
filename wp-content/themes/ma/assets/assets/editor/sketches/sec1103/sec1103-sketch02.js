function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
}

function draw() {
  background(250, 250, 250);
 
  textSize(40);
  stroke(0, 200, 255);
  noFill();
  text("Hello World", 30, 150);
}