var haiku;

function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
  haiku = "古池や\n蛙飛びこむ\n水の音";
}

function draw() {
  background(250, 250, 250);
 
  textSize(20);
  text(haiku, 15, 40);
}