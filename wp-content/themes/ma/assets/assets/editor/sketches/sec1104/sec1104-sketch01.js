var haiku;

function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
  haiku = "古池や蛙飛びこむ水の音";
}

function draw() {
  background(250, 250, 250);
 
  textSize(20);
  text(haiku, 15, 40, 150, 300);
}