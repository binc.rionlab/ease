var novel;
var span;

function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
  novel = "渔阳鼙鼓动地来，\n"
  + "惊破霓裳羽衣曲。\n";  
  span = 45;
}

function draw() {
  background(238, 238, 238);
  
  textSize(20);
  textLeading(span);
  text(novel, 15, 60);
  
  textSize(10);
  text("pí", 60, 40);
  text("ní", 60, 40 + span);
  text("shāng", 70, 40 + span);
}