var ballX;
var ballY;

var speedX;
var speedY;

function setup() {
  createCanvas(300, 300);
  strokeWeight(3);

  ballX = 150;
  speedX = 3;
  
  ballY = 150;
  speedY = 2;
}

function draw() {
  background(238, 238, 238);

  ballX = ballX + speedX;
  ballY = ballY + speedY;

  if (ballX + 50 > 300) {
    speedX = -3;
  }

  if (ballX - 50 < 0) {
    speedX = 3;
  }
  
  if (ballY + 50 > 300) {
    speedY = -3;
  }
  
  if (ballY - 50 < 0) {
    speedY = 3;
  }

  noStroke();
  fill(0, 200, 255);
  ellipse(ballX, ballY, 100);
}