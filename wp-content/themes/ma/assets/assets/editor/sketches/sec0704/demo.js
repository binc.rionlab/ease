var rad = 60;
var xpos, ypos;

var xspeed = 2.8;
var yspeed = 2.2;

var xdirection = 1;
var ydirection = 1;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  noStroke();
  ellipseMode(RADIUS);

  xpos = width / 2;
  ypos = height / 2;
}

function draw() {
  background(234, 234, 234);
  fill(255, 20, 150);

  xpos = xpos + xspeed * xdirection;
  ypos = ypos + yspeed * ydirection;

  if (xpos > width - rad || xpos < rad) {
    xdirection *= -1;
  }

  if (ypos > height - rad || ypos < rad) {
    ydirection *= -1;
  }

  // Draw the shape
  ellipse(xpos, ypos, rad, rad);
}
