var ballX;
var speedX;

function setup() {
  createCanvas(300, 300);
  strokeWeight(3);

  ballX = 150;
  speedX = 3;
}

function draw() {
  background(250, 250, 250);

  ballX = ballX + speedX;

  ellipse(ballX, 150, 100);
}