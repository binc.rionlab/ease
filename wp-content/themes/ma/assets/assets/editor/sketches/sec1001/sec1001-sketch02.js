function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);  
  strokeWeight(3);
 
  drawRectangle();
}

function drawRectangle() {
  line(60, 60, 60, 240);
  line(60, 240, 240, 240);
  line(240, 240, 240, 60);
  line(240, 60, 60, 60);
}