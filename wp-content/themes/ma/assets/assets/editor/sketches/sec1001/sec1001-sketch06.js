function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(238, 238, 238);
  drawHexagon();
}

function drawHexagon() {
  fill(0, 158, 216);
  triangle(150, 150, 150, 300, 0, 225);  
  
  fill(59, 177, 205);
  triangle(150, 150, 0, 225, 0, 75);  
  
  fill(240, 130, 139);
  triangle(150, 150, 0, 75, 150, 0);  

  fill(223, 103, 138);
  triangle(150, 150, 150, 0, 300, 75);  
  
  fill(198, 73, 117);
  triangle(150, 150, 300, 75, 300, 225);  
  
  fill(0, 120, 170);
  triangle(150, 150, 300, 225, 150, 300);
}
