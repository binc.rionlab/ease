var rectX = [];

function setup() {
  createCanvas(300, 300);

  rectX[0] = 50;
  rectX[1] = 100;
  rectX[2] = 150;
}

function draw() {
  background(250, 250, 250);
  strokeWeight(2);
  fill(250, 200, 0);

  rect(rectX[0], 50, 100, 100);
  rect(rectX[1], 100, 100, 100);
  rect(rectX[2], 150, 100, 100);
}