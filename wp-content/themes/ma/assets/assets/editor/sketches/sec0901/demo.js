var rectX = [];
var rectY = [];

function setup() {
  createCanvas(windowWidth,
               windowHeight);

  rectX[0] = 1 * windowWidth / 6;
  rectX[1] = 2 * windowWidth / 6;
  rectX[2] = 3 * windowWidth / 6;
  
  rectY[0] = 1 * windowHeight / 6;
  rectY[1] = 2 * windowHeight / 6;
  rectY[2] = 3 * windowHeight / 6;
}

function draw() {
  background(234, 234, 234);
  strokeWeight(3);
  
  fill(110, 250, 240);
  rect(rectX[0], rectY[0], windowWidth / 3, windowHeight / 3);

  fill(130, 200, 245)
  rect(rectX[1], rectY[1], windowWidth / 3, windowHeight / 3);

  fill(150, 145, 250);
  rect(rectX[2], rectY[2], windowWidth / 3, windowHeight / 3);
}