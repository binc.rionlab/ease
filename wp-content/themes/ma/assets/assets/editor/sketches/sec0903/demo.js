var pointX = [];
var pointY = [];
var speedX = [];
var speedY = [];
var point_num;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  point_num = 3000;
  for (var i = 0; i < point_num; i = i + 1) {
    pointX[i] = random(0, windowWidth);
    pointY[i] = random(0, windowHeight);
    speedX[i] = random(-1, 1);
    speedY[i] = random(-1, 1);
  }
}

function draw() {
  background(234, 234, 234, 40);

  for (var i = 0; i < point_num; i = i + 1) {
    pointX[i] = pointX[i] + speedX[i];
    pointY[i] = pointY[i] + speedY[i];

    if (pointX[i] < 0 || pointX[i] > windowWidth) {
      speedX[i] = -speedX[i];
    }

    if (pointY[i] < 0 || pointY[i] > windowHeight) {
      speedY[i] = -speedY[i];
    }

    if (i < point_num * 0.5) {
      stroke(0, 160, 250);
    }
    else {
      stroke(240, 35, 95);
    }

    point(pointX[i], pointY[i]);
  }
}