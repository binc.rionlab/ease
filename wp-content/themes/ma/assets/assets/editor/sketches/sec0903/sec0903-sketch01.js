var diameter;

function setup() {
  createCanvas(300, 300);

  diameter = 0;
}

function draw() {
  background(250, 250, 250);
  noFill();

  diameter = diameter + 1;
  if (diameter > 300) {
    diameter = 0;
  }

  ellipse(150, 150, diameter);
}