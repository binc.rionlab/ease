var img;
var time;

function preload() {
  img = loadImage("images/room.jpg");
}

function setup() {
  createCanvas(300, 300);
  imageMode(CENTER);
  time = 1;
}

function draw() {
  img.resize(300 / time, 300 / time);
  image(img, 300 / 2, 300 / 2);

  time = time + 1;

  if (time > 10) {
    noLoop();
  }
}