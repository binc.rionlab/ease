var time;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  noFill();
  strokeWeight(3);
  time = 0;
}

function draw() {
  background(234, 234, 234);

  for (var i = 0; i < 10; i = i + 1) {
    var ticktack = 1 + sin(time + i);
    stroke(100, 200, i * (255 / 10));
    strokeWeight(windowWidth / 35 * ticktack);
    ellipse(windowWidth / 2, windowHeight / 2, windowHeight / 10 * i);
  }

  time = time + 0.045;
}