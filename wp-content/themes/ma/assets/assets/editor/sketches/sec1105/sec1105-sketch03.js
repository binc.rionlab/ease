var radius;  
var diameter;  
function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
}

function draw() {
  background(250, 250, 250);

  radius = dist(150, 150, mouseX, mouseY);

  noStroke();
  fill(10, 200, 250);
  ellipse(150, 150, radius * 2);
}