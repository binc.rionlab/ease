var h;
var m;
var s;

function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
}

function draw() {
  background(250, 250, 250);    
    
  h = hour();
  m = minute();
  s = second();
 
  textSize(45);
  text(h, 40, 150);
  text("：", 90, 145);
  text(m, 120, 150);
  text("：", 170, 145);
  text(s, 200, 150);
}