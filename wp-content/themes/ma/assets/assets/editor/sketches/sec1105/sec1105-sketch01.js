function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
}

function draw() {
  background(250, 250, 250);    
  line(0, 150, mouseX, 150);
    
  textSize(20);
  text(mouseX, 20, 50);
}