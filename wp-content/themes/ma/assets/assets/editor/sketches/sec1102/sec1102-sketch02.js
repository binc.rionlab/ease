var greeting;

function setup() {
  createCanvas(300, 300);
  fill(0, 0, 0);
  greeting = "こんにちは";
}

function draw() {
  background(250, 250, 250);
  textSize(30);
  text(greeting, 60, 100);
}