function setup() {
  createCanvas(300, 300);
}

function draw() {
  background(250, 250, 250);
  noFill();
  strokeWeight(3);

  rect(50, 50, 200, 200);
}