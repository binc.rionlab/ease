var points = [];
var posX, posY;
var speedX;
var yoff;

var colors = [];
var time;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  angleMode(DEGREES);
  fill(255, 255, 255);

  time = 0;

  posX = 0;
  posY = windowHeight / 2;
  speedX = 0.5;
  yoff = 0.0;

  colors.push(color(150, 180, 250, 128));
  colors.push(color(200, 200, 250, 128));
  colors.push(color(255, 160, 225, 128));
  colors.push(color(255, 100, 105, 128));
  colors.push(color(200, 25, 70, 128));
  colors.push(color(95, 25, 55, 128));
  
}

function draw() {
  points = [];
  var n = 35;
  var theta = 360 / n;
  var radius = windowHeight / 3;
  var xoff = 0.0;
  for (var i = 0; i < n; i = i + 1) {
    var amp = noise(xoff, yoff);
    points[i] = {
      x: amp * radius * cos(i * theta),
      y: amp * radius * sin(i * theta)
    }
    xoff = xoff + 0.2;
  }
  yoff = yoff + 0.001;

  posX = posX + speedX;
  posY = height / 12 * cos(360 * posX / width) + height / 2;

  for (var i = 0; i < n; i = i + 1) {
    points[i].x += posX;
    points[i].y += posY;
  }

  stroke(getColor(time));
  fill(255, 255, 255, 3);

  beginShape();
  for (var i = 0; i < points.length; i = i + 1) {
    curveVertex(points[i].x, points[i].y);
  }
  curveVertex(points[0].x, points[0].y);
  curveVertex(points[1].x, points[1].y);
  curveVertex(points[2].x, points[2].y);
  endShape();

  time += speedX / windowWidth;
}

function getColor(t) {
  t = constrain(t, 0.000, 0.999999);
  var p = t * (colors.length - 1);
  var from = floor(p);
  return lerpColor(colors[from], colors[from + 1], p - from);
}