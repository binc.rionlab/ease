var particles = [];
var particle_num;;
var diagonal;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  colorMode(HSB, 360, 100, 100, 255);

  particle_num = 5000;
  for (var i = 0; i < particle_num; i++) {
    particles[i] = new Particle();
    particles[i].setup();
  }

  diagonal = width * width + height * height;
}

function draw() {
  background(0, 0, 100, 25);
  for (var i = 0; i < particle_num; i++) {
    particles[i].draw();
  }

  if (mouseIsPressed == true) {
    for (var i = 0; i < particle_num; i++) {
      particles[i].reset();
    }
  }
}

class Particle {
  constructor() { }
  setup() {

    this.pos = createVector(random(0, windowWidth), random(0, windowHeight));
    this.prevPos = createVector(this.pos.x, this.pos.y);
    this.angle = 0.0;
    this.zoff = 0.5;
  }

  reset() {
    this.pos.set(random(0, windowWidth), random(0, windowHeight));
    this.prevPos.set(this.pos.x, this.pos.y);
    this.angle = 0.0;
    this.zoff = 0.5;
  }

  draw() {
    this.angle = noise(this.pos.x / 200, this.pos.y / 100, this.zoff) * 2.0;
    this.pos.x += cos(this.angle) * 5;
    this.pos.y += sin(this.angle) * 5;

    var ratio = (this.pos.x * this.pos.x + this.pos.y * this.pos.y) / diagonal;
    var H = 180 + 200 * ratio;
    var S = 100 + 20 * ratio;
    var B = 80 + 20 * ratio;
    var A = 50;

    stroke(H, S, B, A);
    line(this.prevPos.x, this.prevPos.y, this.pos.x, this.pos.y);

    if (this.pos.x < 0) this.pos.x = width;
    if (this.pos.x > width) this.pos.x = 0;
    if (this.pos.y < 0) this.pos.y = height;
    if (this.pos.y > height) this.pos.y = 0;

    this.prevPos = this.pos.copy();
    this.zoff += random(0, 0.005);
  }
}