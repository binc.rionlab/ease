var sound;
var volume;

var startButton;
var pauseButton;
var volumeSlider;
var isPlaying;
var muted;

function preload() {
  sound = loadSound("sound3.mp3");
}

function setup() {
  createCanvas(300, 300);

  startButton = createButton("Press to start sound");
  startButton.position(90, 130);
  startButton.mousePressed(start);
  
  pauseButton = createButton("Pause");
  pauseButton.position(90, 160);
  pauseButton.mousePressed(pause);
 
  volumeSlider = createSlider(0.0, 1.0, 0.5, 0.1);
  volumeSlider.position(90, 190);

  isPlaying = false;
  muted = false;
}

function draw() {
  background(250, 250, 250);
  
  volume = volumeSlider.value();

  if (muted == false) {
    sound.setVolume(volume); 
  }

  text(volume,170,105);
}

function start() {
  if (isPlaying == false) {
    sound.loop();
    isPlaying = true;
  }

  if (muted) {
    sound.setVolume(1.0);
    muted = false;
  }
}

function pause() {
  muted = !muted;
  if (muted) {
    sound.setVolume(0.0);
  } else {
    sound.setVolume(1.0);
  }
}

// 
// 声音数据
// 
// 目覚まし時計のアラーム音  
// written by 123理论
// https://dova-s.jp/se/play604.html
// 