var sound;
var amplitude;
var X;
var Y;
var prevX;
var prevY;

var startButton;
var pauseButton;
var initialized;
var muted;

var pts = [];
var curPnt;
var prevPnt;
var time;

function preload() {
  sound = loadSound("sounds/heart_beat.mp3");
}

function setup() {
  createCanvas(300, 300);

  startButton = createButton("Press to start sound");
  startButton.position(70, 40);
  startButton.mousePressed(start);

  pauseButton = createButton("Pause");
  pauseButton.position(70, 70);
  pauseButton.mousePressed(pause);

  amplitude = new p5.Amplitude();

  initialized = false;
  muted = false;

  X = 0;
  Y = height - 30;

  prevX = X;
  prevY = Y;

  curPnt = createVector(0, 0);
  prevPnt = createVector(0, 0);
  reset();

  time = 0.0;
}

function draw() {
  background(234, 234, 234);

  var level = amplitude.getLevel();
  X = X + 2;
  Y = map(level, 0, 1, 1, 10);

  prevX = X;
  prevY = Y;

  stroke(20, 20, 20);
  strokeWeight(Y * Y * Y);
  drawCubicBezier();
  var point = getPointAt(time);
  strokeWeight(1);
  ellipse(point.x, point.y, Y * Y * Y);
  var volume = 1.0 - point.y / height;

  if (muted) {
    sound.setVolume(0.0);
  } else {
    sound.setVolume(volume);
  }
  
  strokeWeight(1);
  noStroke();
  text(floor(volume * 1000) / 1000, point.x, point.y - 20);

  time = time + 0.002;
  if (time > 1) {
    time = 0;
    reset();
  }
}

function start() {
  if (initialized == false) {
    sound.loop();
    initialized = true;
  }

  if (muted) {
    sound.setVolume(1.0);
    muted = false;
  }
}

function pause() {
  muted = !muted;
  if (muted) {
    sound.setVolume(0.0);
  } else {
    sound.setVolume(1.0);
  }
}

function reset() {
  pts[0] = createVector(0, height / 2);
  pts[1] = createVector(1 / 3 * width, random(-height / 2, 1.5 * height));
  pts[2] = createVector(2 / 3 * width, random(-height / 2, 1.5 * height));
  pts[3] = createVector(width, height / 2);
}

function calcBernstein(t) {
  var b = [];
  b[0] = (1 - t) * (1 - t) * (1 - t);
  b[1] = 3 * (1 - t) * (1 - t) * t;
  b[2] = 3 * (1 - t) * t * t;
  b[3] = t * t * t;
  return b;
}

function getPointAt(t) {
  var b = calcBernstein(t);
  var point = createVector(0, 0);
  for (var i = 0; i < 4; i = i + 1) {
    point.x += pts[i].x * b[i];
    point.y += pts[i].y * b[i];
  }
  point.y = constrain(point.y, 30, height - 30);
  return point;
}

function drawCubicBezier() {
  for (var t = 0.0; t < 1.05; t += 0.05) {
    curPnt = getPointAt(t);
    if (t != 0.0) line(curPnt.x, curPnt.y, prevPnt.x, prevPnt.y);
    prevPnt.set(curPnt);
  }
}

// 
// 声音数据
// 
// 心脏的跳动、紧张 
// written by 123理论
// https://dova-s.jp/se/play868.html
// 