var rectSize = [];
var rectWidth = [];
var rectHeight = [];
var rectNum;

function setup() {
  createCanvas(windowWidth,
               windowHeight);
  noFill();
  rectNum = 50;
  for (var i = 0; i < rectNum; i = i + 1) {
    rectWidth[i] = i * windowWidth / rectNum;
    rectHeight[i] = i * windowHeight / rectNum;
  }
}

function draw() {
  strokeWeight(2);
  background(234, 234, 234);
  for (var i = rectNum - 1; i >= 0; i = i - 1) {
    var R = 250 - i * 250 / rectNum;
    var G = 35 + i * 125 / rectNum;
    var B = 95 + i * 125 / rectNum;
    stroke(R, G, B);
    rect(0, 0, rectWidth[i], rectHeight[i]);
  }
}