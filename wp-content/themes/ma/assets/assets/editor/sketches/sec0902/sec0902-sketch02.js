var diameter = [];

function setup() {
  createCanvas(300, 500);

  for (var i = 0; i < 100; i = i + 1) {
    diameter[i] = 5 * i + 5;
  }
}

function draw() {
  background(250, 250, 250);
  noFill();

  for (var i = 0; i < 100; i = i + 1) {
    ellipse(150, 250, diameter[i]);
  }
}