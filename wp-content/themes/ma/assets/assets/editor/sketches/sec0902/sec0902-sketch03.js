var rectSize = [];

function setup() {
  createCanvas(300, 300);
 
  rectSize[0] = 5 * 0 + 5;
  rectSize[1] = 5 * 1 + 5;
  rectSize[2] = 5 * 2 + 5;
}

function draw() {
  background(250, 250, 250);
  noFill();
  
  rect(0, 0, rectSize[0], rectSize[0]); 
  rect(0, 0, rectSize[1], rectSize[1]); 
  rect(0, 0, rectSize[2], rectSize[2]); 
}