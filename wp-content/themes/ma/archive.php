<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

		<header id="header_wrap">
			<div class="fullScreenWrap66">
<?php get_template_part('inc/nav'); ?>



				

			</div>
		</header>	
		<!--==============================content=================================-->


		<div id="main">

	<div id="top_news" class="topcont">
<!--	<h3><img src="/img/top_index/tit_top_news.png" width="200" alt="news"></h3>-->
<h3>Newss</h3>
	<div class="inner">
		<?php query_posts("showposts=4"); ?>
		<ul>
		<?php while (have_posts()) : the_post(); ?><li class="inviewzoomIn">
		<div class="ti_thumb"><a href="<?php the_permalink(); ?>" rel="bookmark">
		<?php if ( has_post_thumbnail() ):?>
		<?php the_post_thumbnail("topinfosize"); ?>
		<?php else : ?>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/asset/image/news_sample.png" alt="MADD. NEWS" />
		<?php endif; ?>
		</div>
		<div class="ti_txt">
		<p class="ti_date"><?php the_time("Y.n.j"); ?>
		<?php
		$hour = 336;
		$html = '<span class="info_new"> NEW</span>';
		$current_date = date('U');
		$entry_date = get_the_time('U');
		$min = ceil(date('U',($current_date - $entry_date))/3600);
		if ($min+8 < $hour) {
		echo $html;
		}
		?>
		</p>
		<h4><?php the_title(); ?></h4>
		<div class="excerpt"><?php the_excerpt()?></div></a>
		<!--<a href="<?php the_permalink(); ?>" rel="bookmark" class="plusmore">more view</a>-->
		</div>		
		</li><?php endwhile; ?>
		</ul>
		<?php wp_reset_query(); ?>

	</div>
	</div>


		</div>


<?php get_footer(); ?>