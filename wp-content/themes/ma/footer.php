		<!--==============================footer=================================-->
  <section id="footer_copyright">
    <div style="text-align: center; margin-bottom: 10px; padding-top: 5px;"><a href="http://inertiaart.io/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>//assets/img/INERTIA_LOGO.svg" style="height: 15px; margin: 0 10px;"/><a href="https://www.facebook.com/easel-103182437986338/" target="_blank"><i class="fab fa-facebook-square" style="height: 15px; margin: 0 2px;"></i></a><a href="https://twitter.com/easel_inertia" target="_blank"><i class="fab fa-twitter-square" style="height: 15px; margin: 0 2px;"></i></a><a href="https://www.instagram.com/easel.inertia/" target="_blank"><i class="fab fa-instagram" style="height: 15px; margin: 0 2px;"></i></a></a>
    </div>
    <div style="text-align: center"><a href="<?php echo esc_url( home_url( '/' ) ); ?>privacy"><span class="footer--text footer--text__small">隐私权政策</span></a><span style="margin: 3px;"></span><a href="<?php echo esc_url( home_url( '/' ) ); ?>faq"><span class="footer--text footer--text__small">常见问题</span></a><span style="margin: 3px;"></span><a href="<?php echo esc_url( home_url( '/' ) ); ?>transaction"><span class="footer--text footer--text__small">基于特定商户交易法的公示信息</span></a></div><div style="text-align: center"><a href="<?php echo esc_url( home_url( '/' ) ); ?>trouble-shooting"><span class="footer--text footer--text__small">故障排除</span></a><span style="margin: 3px;"></span><span style="margin: 3px;"></span><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact"><span class="footer--text footer--text__small">问询</span></a></div>
    <p class="footer--text footer--text__small">&copy; <?php echo date('Y'); ?> 株式会社INERTIA All Rights Reserved.</p>
  </section>
  <script>
    Pace.on('done',function(){$('#list-container').fadeIn();$('#box-container').fadeIn();$('#footer_copyright').fadeIn();$('.modal').modal();});
    function showInfo() {$('#showPlanInfo').modal('open');return false;}
  </script>
<?php wp_footer(); ?>
</body>
</html>
