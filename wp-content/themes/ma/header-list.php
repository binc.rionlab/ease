<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
  <meta http-equiv="Content-Style-Type" content="text/css">
  <meta http-equiv="Content-Scipt-Type" content="text/javascript">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/materialize.min.css" media="screen,projection">
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.min.js"></script>
  <script src="https://kit.fontawesome.com/9dd23d1e34.js"></script>

  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/pace.min.js"></script>
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/pace/themes/silver/pace-theme-loading-bar.css" />
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/scrollpup.js"></script>
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>//assets/css/content.min.css" type="text/css">
  <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/image/favicon.ico">
  <title><?php echo get_the_title();?></title>
<?php wp_deregister_script('jquery'); ?>
<?php wp_head(); ?>
</head>
<body>
