<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */


get_header('single'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){
		caches.keys().then(function(cacheNames) {
    cacheNames.forEach(function(cacheName) {
      caches.delete(cacheName);
    });
  });
		$('.sidenav').sidenav();});
  </script>
<a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;"></a>




			<?php while ( have_posts() ) : the_post(); ?>

					<?php the_content(); ?>
						
			<?php endwhile; // end of the loop. ?>
<div class="swpm-login-widget-form">
<form action="<?php the_permalink(); ?>" method="post">

<?php wp_nonce_field( 'login', 'login_nonce' ) ?>

<?php if ( ! empty( $error ) ): ?><p><?php echo $error; ?></p><?php endif; ?>
        <div class="swpm-login-form-inner">
            <div class="swpm-username-label">
                <label for="swpm_user_name" class="swpm-label">メールアドレス</label>
            </div>
            <div class="swpm-username-input">
                <input type="email" name="user_email" class="swpm-text-field swpm-username-field" value="<?php echo $email; ?>" />
            </div>
            <div class="swpm-password-label">
                <label for="swpm_password" class="swpm-label">パスワード</label>
            </div>
            <div class="swpm-password-input">
                <input type="password" name="user_pass" class="swpm-text-field swpm-password-field" />
            </div>
            
            <div class="swpm-login-submit">
                <button type="submit" class="swpm-login-form-submit" >ログイン</button>
            </div>
</div>

            <div class="swpm-forgot-pass-link">
                <a id="forgot_pass" class="swpm-login-form-pw-reset-link"  href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login/password-reset">パスワードをお忘れですか？</a>
            </div>
</form>
</div>

<?php get_footer(); ?>