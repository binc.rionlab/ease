<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<div class="inner">
<div id="primary" class="blog-site-content">
	<div id="content">
		
	<section id="blog">
		<!--「<?php echo get_search_query(); ?>」に-->該当する記事は<?php echo $wp_query->found_posts; ?>件あります。
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				<article>
					<header class="entry-header">
					<p class="date"><?php the_time('Y.n.j'); ?></p>
					<h1 class="entry-title"><?php the_title(); ?></h1>
					</header>
					<div class="entry-content">
					
					<div><?php the_field('old'); ?>　<?php the_field('sex'); ?>性　<?php the_field('fix'); ?></div>
								
				<?php if( get_field('days') ): ?>
					<div class="days">経過期間：<?php the_field('days'); ?></div>
				<?php endif; ?>
				
				<?php if( get_field('doctor_comment') ): ?>
				<h5>ドクターコメント</h5>
					<div><?php the_field('doctor_comment'); ?></div>
				<?php endif; ?>
				
				<?php if( get_field('imgfile') ): ?>
					<div class="img"><img src="http://www.primo-rhinoplasty.jp/<?php the_field('imgfile'); ?>"></div>
				<?php endif; ?>
				
					</div>
				</article>

	<?php			
		endwhile;
		endif;
		wp_reset_postdata();
	?>

	<div class="paging">
		<!--ページネーション-->
		<?php if (function_exists('responsive_pagination')) {
		  responsive_pagination($additional_loop->max_num_pages);
		} ?>
	</div>

</section>
</div>
</div>

<div id="secondary" class="widget-area blog_side">
<?php get_sidebar("case"); ?>
</div>
</div>

</div>
<?php get_footer(); ?>