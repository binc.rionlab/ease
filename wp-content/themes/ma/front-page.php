<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */

get_header('top'); ?>

  <div class="drawer"><a class="sidenav-trigger drawer--icon__black" href="#" data-target="mobile-demo"><i class="material-icons">menu</i></a>
    <ul class="right hide-on-med-and-down" style="display:none">
      <ul class="right hide-on-med-and-down">
        <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>">主页</a></li>
        <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>01/">学习</a></li>
        <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>02/">模仿</a></li>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register?free">有料会員登録</a></li>
		<li><a class="logout waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login?swpm-logout=true">退出账号</a></li>
			<?php else : ?>
		<li><a class="logout waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login?swpm-logout=true">退出账号</a></li>
			<?php endif; ?>
<?php else : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>login">ログイン</a></li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register">会員登録</a></li>
<?php endif; ?>


      </ul>
    </ul>
  </div>
  <ul class="sidenav" id="mobile-demo">
    <li>
      <div class="user-view">
        <div class="background"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/mainimage.png" style="height: 300px;" /></div><a href="http://inertiaart.io/"><span class="white-text name" style="font-family: DINNextLTPro-Bold,sans-serif; font-size:23px;"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_w.svg" alt="easel"></span></a><span class="white-text email" style="font-family: DINNextLTPro-Bold,sans-serif; mix-blend-mode: soft-light;">COMPLETE VERSION</span></div>
    </li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>"><i class="material-icons">home</i>主页</a></li>
    <li>
      <div class="divider"></div>
    </li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>01/"><i class="material-icons">tag_faces</i>学习</a></li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>02/"><i class="material-icons">brush</i>模仿</a></li>
    <li>
      <div class="divider"></div>
    </li>
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login"><i class="material-icons">account_box</i>个人页面</a></li>
        <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register?free"><i class="material-icons">shop</i>有料会員登録</a></li>
		<li><a class="logout waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login?swpm-logout=true"><i class="material-icons">exit_to_app</i>退出账号</a></li>
			<?php else : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>mysketchbook"><i class="material-icons">import_contacts</i>SketchBook</a></li>
    <li>
      <div class="divider"></div>
    </li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login"><i class="material-icons">account_box</i>个人页面</a></li>
		<li><a class="logout waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>membership-login?swpm-logout=true"><i class="material-icons">exit_to_app</i>退出账号</a></li>
			<?php endif; ?>
<?php else : ?>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>login"><i class="material-icons">input</i>ログイン</a></li>
		<li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>register"><i class="material-icons">touch_app</i>会員登録</a></li>
<?php endif; ?>
    <li>
      <div class="divider"></div>
    </li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>faq/"><i class="material-icons">question_answer</i>常见问题</a></li>
<!--    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>assets/editor/sketch_book.html"><i class="material-icons">menu_book</i>Sketch Book</a></li>-->
    <li>
      <div class="divider"></div>
    </li>
    <li><a class="waves-effect" href="https://lp.ch.easelart.io/"><i class="material-icons">launch</i>关于easel</a></li>
    <li><a class="waves-effect" href="<?php echo esc_url( home_url( '/' ) ); ?>contact/"><i class="material-icons">contact_mail</i>问询</a></li>
  </ul>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
	  
     $(document).ready(function(){
		console.log("dfdsfdf")
		$('.sidenav').sidenav();
		 	caches.keys().then(function(cacheNames) {
    cacheNames.forEach(function(cacheName) {
      caches.delete(cacheName);
    });
  });
	})
  </script>
  <div id="topBlock"><iframe src="<?php echo get_stylesheet_directory_uri(); ?>/assets/editor/editor.html" data-first-display="sketch" data-path="./sketches/top/sketch.js" data-title="" data-istop="true">　</iframe></div>
  <section id="topTitle">
    <h1><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo_w.svg" alt="easel"></h1>
  </section>
  <section id="footer-menu">
    <ul id="navi-ul">
      <li class="navi-list" id="">
        <a href="01/">
          <div class="navi-blocks">
            <p class="navi-jpTitle">学习</p>
            <p class="navi-enTitle">Entry</p>
          </div>
        </a>
      </li>
      <li class="navi-list" id="">
        <a href="02/">
          <div class="navi-blocks">
            <p class="navi-jpTitle">模仿</p>
            <p class="navi-enTitle">Make</p>
          </div>
        </a>
      </li>
    </ul>
  </section>
<?php get_footer('top'); ?>
