
var controller = new ScrollMagic.Controller();
			//TweenMax.set("#anime div", {position:"absolute");
			var tween1 = TweenMax.fromTo("#anime .scale_1",1 ,{top:"-90px", opacity:0}, {top:"50px",opacity: 1});
			var tween2 = TweenMax.fromTo("#anime .scale_2",1 ,{top:"-30px", opacity:0}, {top:"110px",opacity: 1, delay:1});
			var tween3 = TweenMax.fromTo("#anime .scale_3",1 ,{top:"310px", opacity:0}, {top:"460px",opacity: 1, delay:1});
			var tween4 = TweenMax.fromTo("#anime .scale_4",1 ,{top:"420px", opacity:0}, {top:"570px",opacity: 1, delay:1});
			var tween5 = TweenMax.fromTo("#anime .scale_5",1 ,{top:"545px", opacity:0}, {top:"680px",opacity: 1, delay:1});
			var tween6 = TweenMax.fromTo("#anime .scale_6",1 ,{top:"850px", opacity:0}, {top:"1000px",opacity: 1,delay:1});
			var tween7 = TweenMax.fromTo("#anime .scale_7",1 ,{top:"900px", opacity:0}, {top:"1050px",opacity: 1,delay:1});
			
			var tween8 = TweenMax.fromTo("#anime .scale_8",1 ,{left:"560px", opacity:0}, {left:"260px",opacity: 1,delay:1});
			var tween9 = TweenMax.fromTo("#anime .scale_9",1 ,{left:"700px", opacity:0}, {left:"470px",opacity: 1,delay:1});
// シーンをつくります。
// triggerElementに渡した要素が表示範囲にはいったらsetTweenで渡したアニメーションが実行されます
var scene1 = new ScrollMagic.Scene({triggerElement: "#anime .scale_1"})
.setTween(tween1)
//.addIndicators({name: "scene"}) debag用
.addTo(controller);

var scene2 = new ScrollMagic.Scene({triggerElement: "#anime .scale_2"})
.setTween(tween2)
.addTo(controller);

var scene3 = new ScrollMagic.Scene({triggerElement: "#anime .scale_3"})
.setTween(tween3)
.addTo(controller);

var scene4 = new ScrollMagic.Scene({triggerElement: "#anime .scale_4"})
.setTween(tween4)
.addTo(controller);

var scene5 = new ScrollMagic.Scene({triggerElement: "#anime .scale_5"})
.setTween(tween5)
.addTo(controller);

var scene6 = new ScrollMagic.Scene({triggerElement: "#anime .scale_6"})
.setTween(tween6)
.addTo(controller);

var scene7 = new ScrollMagic.Scene({triggerElement: "#anime .scale_7"})
.setTween(tween7)
.addTo(controller);

var scene8 = new ScrollMagic.Scene({triggerElement: "#anime .scale_8"})
.setTween(tween8)
.addTo(controller);

var scene9 = new ScrollMagic.Scene({triggerElement: "#anime .scale_9"})
.setTween(tween9)
.addTo(controller);