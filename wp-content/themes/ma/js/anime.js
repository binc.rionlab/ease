$(function(){

TweenMax.fromto(".mountain" , 1 ,
	{top:"-100px" , opacity:0} , // left 0、opacity 0の状態を指定
	{top:"0px" , opacity:1}, // left 100px、opacity 1まで変化させる
	{ease : "Linear"} // イージングです。後述しますがTweenMax独自のイージングもあります。
);

TweenMax.fromto(".board" , 1 ,
	{top:"-100px" , opacity:0} , // left 0、opacity 0の状態を指定
	{top:"0px" , opacity:1}, // left 100px、opacity 1まで変化させる
	{ease : "Linear"} // イージングです。後述しますがTweenMax独自のイージングもあります。
);


TweenMax.fromto(".flag" , 1 ,
	{top:"-100px" , opacity:0} , // left 0、opacity 0の状態を指定
	{top:"0px" , opacity:1}, // left 100px、opacity 1まで変化させる
	{ease : "Linear"} // イージングです。後述しますがTweenMax独自のイージングもあります。
);


TweenMax.fromto(".moji" , 1 ,
	{top:"-100px" , opacity:0} , // left 0、opacity 0の状態を指定
	{top:"0px" , opacity:1}, // left 100px、opacity 1まで変化させる
	{ease : "Linear"} // イージングです。後述しますがTweenMax独自のイージングもあります。
);



// move_stringクラスの要素を0.5秒かけてopacityを1にする。0.2秒毎に実行される
TweenMax.staggerTo(".move_string" , 0.5 , {opacity:1} , 0.5)
// 初期状態も指定できるstaggerFromToの方だと↓のような感じですね
//TweenMax.staggerFromTo(".move_string" , 0.5 , {opacity:0} , {opacity:1} , 0.2)
 
// 全部実行した後に特定の処理をしたい場合は↓のような感じで
//TweenMax.staggerTo(".move_string" , 0.5 , {opacity:1} , 0.2 , function(){ // なんらかの処理})



});