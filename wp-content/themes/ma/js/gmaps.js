function initialize() {
  var latlng = new google.maps.LatLng(35.657202,139.6988181);
  var myOptions = {
    zoom: 16, /*拡大比率*/
    center: latlng, /*表示枠内の中心点*/
    mapTypeId: google.maps.MapTypeId.ROADMAP/*表示タイプの指定*/
  };
  var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
  
  /*アイコン設定▼*/
  var icon = new google.maps.MarkerImage('http://ma.scale.style/wp-content/themes/ma/images/map_icon.png',
    new google.maps.Size(80,80),/*アイコンサイズ設定*/
    new google.maps.Point(0,0)/*アイコン位置設定*/
    );
  var markerOptions = {
    position: latlng,
    map: map,
    icon: icon,
    title: 'MA'
  };
  var marker = new google.maps.Marker(markerOptions);
　/*アイコン設定ここまで▲*/
  
  var styleOptions = [
			  {
			    featureType: "road.highway",
			    stylers: [
			      { saturation: -100 }
			    ]
			  },{
			    featureType: "road.arterial",
			    stylers: [
			      { saturation: -100 }
			    ]
			  },{
			    featureType: "road.local",
			    stylers: [
			      { saturation: -100 }
			    ]
			  },{
			    featureType: "transit.line",
			    stylers: [
			      { saturation: -100 }
			    ]
			  },{
			    featureType: "water",
			    stylers: [
			      { saturation: -100 }
			    ]
			  },{
			    featureType: "landscape",
			    stylers: [
			      { color: "#e6e6e6" },
			      { visibility: "simplified" }
			    ]
			  },{
			    featureType: "poi",
			    stylers: [
			      { saturation: -100 },
			      { visibility: "off" }
			    ]
			  },{
			    featureType: "transit.station",
			    stylers: [
			      { saturation: -100 }
			    ]
			  },{
			    featureType: "administrative",
			    stylers: [
			      { saturation: -100 }
			    ]
			  },{
			    featureType: "road.arterial",
			    elementType: "geometry.fill",
			    stylers: [
			      { color: "#ffffff" }
			    ]
			  },{
			    featureType: "road.highway",
			    elementType: "geometry.fill",
			    stylers: [
			      { color: "#ffffff" }
			    ]
			  },{
			    featureType: "road.highway",
			    elementType: "labels.text.stroke",
			    stylers: [
			      { visibility: "off" }
			    ]
			  },{
			    featureType: "road.arterial",
			    elementType: "labels.text.stroke",
			    stylers: [
			      { visibility: "off" }
			    ]
			  },{
			    featureType: "transit",
			    elementType: "labels.text.stroke",
			    stylers: [
			      { visibility: "off" }
			    ]
			  },{
			    featureType: "water",
			    elementType: "geometry.fill",
			    stylers: [
			      { visibility: "on" },
			      { color: "#bbbbbb" }
			    ]
			  },{
			    featureType: "road.highway",
			    elementType: "geometry.stroke",
			    stylers: [
			      { visibility: "off" }
			    ]
			  },{
			    featureType: "road.arterial",
			    elementType: "geometry.stroke",
			    stylers: [
			      { visibility: "off" }
			    ]
			  },{
			    featureType: "road.local",
			    elementType: "geometry.stroke",
			    stylers: [
			      { visibility: "off" }
			    ]
			  },{
			  }
			]
  
  var styledMapOptions = { name: 'MA' }
  var sampleType = new google.maps.StyledMapType(styleOptions, styledMapOptions);
  map.mapTypes.set('sample', sampleType);
  map.setMapTypeId('sample');
  
  
  
  
  
  
}
