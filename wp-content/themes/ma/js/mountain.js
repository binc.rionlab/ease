(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.シンボル3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgNBVQgIgEgGgEQgGgGgEgIQgDgIAAgKIAAhZQAAgKADgJQAEgHAGgFQAGgFAIgDQAGgDAHAAQAIAAAHADQAHADAGAFQAGAFAEAHQADAJAAAKIAABZQAAAKgDAIQgEAIgGAGQgGAEgHAEQgHACgIAAQgHAAgGgCgAgLg5QgEAEAAAJIAABZQAAAJAEAFQAFADAGAAQAHAAAFgDQAFgFgBgJIAAhZQABgJgFgEQgFgEgHgBQgGABgFAEg");
	this.shape.setTransform(1034.1,20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgnBWIAAiqIAlAAQAJAAAIACQAHADAHAHQAGAHACAJQADAIAAAPQAAAMgCAIQgBAIgEAGQgFAIgJAFQgIAFgNAAIgMAAIAABDgAgOgDIAMAAQAGAAAEgDQAEgCACgDIACgJIABgMIgBgMIgCgJQgCgEgDgCQgEgCgGgBIgNAAg");
	this.shape_1.setTransform(1024.4,20.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLBWIAAiqIAXAAIAACqg");
	this.shape_2.setTransform(1016.8,20.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#0084C7").s().p("AoCCvIAAldIQFAAIi5CoIC5C1g");
	this.shape_3.setTransform(1053.5,20.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#000000").ss(4,1).p("AAAl7IAAL3");
	this.shape_4.setTransform(1010,38);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0084C7").s().p("Ag1A2QgWgWAAggQAAgfAWgVQAXgXAeAAQAfAAAXAXQAWAVAAAfQAAAggWAWQgXAWgfAAQgeAAgXgWg");
	this.shape_5.setTransform(1008.8,84.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AggBLQgCgEgBgGIgBgQIAAgvIABgSQABgHACgDQADgFAFgDQAFgDAJAAQAHAAAFADQAGADAEAGIAAAAIAAg8IAZAAIAACqIgZAAIAAgLIgEAFIgFAEIgFACIgIABQgPAAgHgLgAgJgJQgCAFAAAEIAAAtQAAAIADAEQADAFAFAAQAFAAAEgEQADgFAAgGIAAgvQAAgFgDgEQgEgEgFAAQgGAAgDAEg");
	this.shape_6.setTransform(1063.3,199.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAMA+IAAhUQAAgHgDgEQgDgDgGAAQgGAAgDAEQgCAFAAAHIAABSIgZAAIAAh5IAZAAIAAALIAAAAQAEgFAGgEQAEgDAIAAQAFgBAEACQAFACAEADQADADACAHQADAFAAAIIAABdg");
	this.shape_7.setTransform(1054,201.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgTA9QgEgBgEgEQgFgDgCgHQgDgGABgKQgBgKACgIQACgIAFgEQAEgFAHgDQAIgCAKAAIAGAAIAGAAIAAgNQAAgGgDgEQgCgEgHAAQgEAAgEADQgEADgCAGIgXAAQAAgIADgGQADgHAFgFQAFgFAGgDQAHgCAIAAQAIAAAGACQAHACAFAFQAEAFAEAHQACAHAAAKIAABUIgYAAIAAgNIgBAAQgEAHgGAEQgDADgJAAgAgHALQgFAEAAAKQAAAHAEAEQACAFAGAAQAGAAADgFQAEgEAAgHIAAgRIgHAAQgHAAgGADg");
	this.shape_8.setTransform(1044.9,201.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgkBWIAAipIAZAAIAAALIAJgJQAEgEAJAAQAIAAAFADQAFACADAHQADADABAHIABAQIAAAuIgBATQgBAFgCAFQgDAEgFAEQgFACgJAAQgHABgFgEQgGgDgEgGIAAAAIAAA8gAgIg4QgDAEAAAHIAAAuQAAAGADADQAEAEAEAAQAHAAADgFQACgEAAgFIAAgsQAAgIgDgEQgDgEgGgBQgEAAgEAFg");
	this.shape_9.setTransform(1035.9,204.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AAQA9IgQglIgPAlIgaAAIAdg9Igcg8IAaAAIAOAjIAPgjIAaAAIgdA8IAeA9g");
	this.shape_10.setTransform(1027.3,201.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgSA5QgJgEgEgIQgCgFgCgGIAAgOIAAgnIAAgOQACgGACgFQAEgIAJgFQAHgEALAAQAIAAAHADQAHACAEAGQAFAFADAHQADAHgBAIIAAAgIgvAAIAAARQAAAFADAEQADADAFAAQAHAAACgEQADgEAAgFIAYAAQABAIgDAHQgDAHgFAEQgEAFgHADQgHADgIAAQgLAAgHgFgAgIghQgDADAAAHIAAAOIAXAAIAAgOQAAgHgDgDQgDgEgGAAQgFAAgDAEg");
	this.shape_11.setTransform(1018.9,201.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0084C7").s().p("AoCCvIAAldIQFAAIi5CoIC5C1g");
	this.shape_12.setTransform(1053.5,201.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f().s("#000000").ss(4,1).p("AAAl7IAAL3");
	this.shape_13.setTransform(1010,219);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0084C7").s().p("Ag1A2QgWgWAAggQAAgeAWgWQAXgXAeAAQAfAAAXAXQAWAWAAAeQAAAggWAWQgXAWgfAAQgeAAgXgWg");
	this.shape_14.setTransform(1008.8,265.2);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AggBLQgCgEgBgGIgBgQIAAgvIABgSQABgHACgDQADgFAFgDQAFgDAJAAQAHAAAFADQAGADAEAGIAAAAIAAg8IAZAAIAACqIgZAAIAAgLIgEAFIgFAEIgFACIgIABQgPAAgHgLgAgJgJQgCAFAAAEIAAAtQAAAIADAEQADAFAFAAQAFAAAEgEQADgFAAgGIAAgvQAAgFgDgEQgEgEgFAAQgGAAgDAEg");
	this.shape_15.setTransform(635.3,335.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AAMA+IAAhUQAAgHgDgEQgDgDgGAAQgGAAgDAEQgCAEAAAJIAABRIgZAAIAAh5IAZAAIAAALIAAAAQAEgFAGgEQAEgDAIgBIAJACQAFACAEADQADADACAHQADAFAAAIIAABdg");
	this.shape_16.setTransform(626,337.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgTA9QgFgBgEgEQgEgDgCgHQgCgGgBgKQABgKABgIQACgIAEgEQAFgFAHgDQAHgCALAAIAFAAIAHAAIAAgNQAAgGgCgEQgEgEgHAAQgDAAgEADQgEADgBAGIgYAAQAAgIAEgGQACgHAFgFQAFgFAHgDQAGgCAHAAQAIAAAHACQAHACAEAFQAGAFACAHQADAHABAKIAABUIgZAAIAAgNIAAAAQgGAHgEAEQgEADgJAAgAgGALQgGAEAAAKQAAAHADAEQAEAFAFAAQAGAAADgFQAEgEAAgHIAAgRIgHAAQgHAAgFADg");
	this.shape_17.setTransform(616.9,337.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AgkBWIAAipIAZAAIAAALIAJgJQAEgEAJAAQAIAAAFADQAFADADAGQADADABAHIABAQIAAAuIgBATQgBAFgCAEQgDAFgFADQgFADgJAAQgHABgFgEQgGgDgEgGIAAAAIAAA8gAgIg4QgDAEAAAHIAAAuQAAAFADAEQAEAEAEAAQAHAAADgFQACgEAAgFIAAgsQAAgHgDgFQgDgEgGgBQgEAAgEAFg");
	this.shape_18.setTransform(607.9,340.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AARA9IgRglIgQAlIgaAAIAeg9Igcg8IAZAAIAPAjIAPgjIAaAAIgdA8IAfA9g");
	this.shape_19.setTransform(599.3,337.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgTA5QgHgEgFgIQgCgFgBgGIgCgOIAAgnIACgOQABgGACgFQAFgIAHgFQAJgEAKAAQAJAAAGADQAHACAEAGQAGAFACAHQADAHAAAIIAAAgIgwAAIAAARQAAAFADAEQAEADAEAAQAHAAACgEQACgEABgFIAZAAQAAAIgDAHQgCAHgGAEQgFAFgGADQgGADgJAAQgKAAgJgFgAgIghQgDADAAAHIAAAOIAXAAIAAgOQAAgHgEgDQgDgEgFAAQgEAAgEAEg");
	this.shape_20.setTransform(590.9,337.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#0084C7").s().p("AoCCvIAAldIQFAAIi5CoIC5C1g");
	this.shape_21.setTransform(625.5,337.5);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#000000").ss(4,1).p("AAAl7IAAL3");
	this.shape_22.setTransform(582,355);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AgSA5QgJgEgEgIQgCgFgBgGIgBgOIAAgnIABgOQABgGACgFQAEgIAJgFQAHgEALAAQAIAAAHADQAHACAFAGQAEAFADAHQACAHABAIIAAAgIgwAAIAAARQAAAFADAEQAEADAEAAQAHAAACgEQACgEABgFIAZAAQgBAIgCAHQgDAHgEAEQgGAFgGADQgHADgIAAQgLAAgHgFgAgIghQgDADAAAHIAAAOIAXAAIAAgOQAAgHgDgDQgDgEgGAAQgEAAgEAEg");
	this.shape_23.setTransform(513.3,299.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgTA5QgHgEgFgIQgCgFgCgGIgBgOIAAgnIABgOQACgGACgFQAFgIAHgFQAJgEAKAAQAJAAAGADQAHACAFAGQAEAFADAHQACAHAAAIIgYAAQAAgHgEgDQgDgEgFAAQgFAAgDAEQgDADAAAHIAAAxQAAAFADAEQADADAFAAQAHAAACgEQACgEABgFIAYAAQAAAIgCAHQgDAHgFAEQgEAFgHADQgGADgJAAQgKAAgJgFg");
	this.shape_24.setTransform(504.8,299.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AAMA9IAAhTQAAgHgDgDQgDgEgGAAQgGAAgDAEQgCAFAAAHIAABRIgZAAIAAh4IAZAAIAAALIAAAAQAEgFAGgEQAEgEAIABIAJABQAFACAEADQADADACAHQADAFAAAIIAABcg");
	this.shape_25.setTransform(495.4,299.8);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgTA9QgEgBgEgEQgEgDgDgHQgCgGAAgKQAAgKABgIQACgIAEgEQAFgFAHgDQAHgCALAAIAFAAIAHAAIAAgNQAAgGgCgEQgEgEgGAAQgEAAgEADQgEADgCAGIgXAAQABgIADgGQACgHAFgFQAFgFAGgDQAHgCAIAAQAHAAAHACQAHACAFAFQAEAFAEAHQADAHAAAKIAABUIgZAAIAAgNIgBAAQgFAHgFAEQgDADgJAAgAgGALQgGAEAAAKQAAAHAEAEQACAFAGAAQAGAAAEgFQADgEAAgHIAAgRIgHAAQgHAAgFADg");
	this.shape_26.setTransform(486.2,299.9);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgKA9Igfh5IAZAAIAQBNIAAAAIARhNIAZAAIgfB5g");
	this.shape_27.setTransform(477.8,299.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AggBLQgCgEgBgGIgBgQIAAgvIABgSQABgHACgDQADgFAFgDQAFgDAJAAQAHAAAFADQAGADAEAGIAAAAIAAg8IAZAAIAACqIgZAAIAAgLIgEAFIgFAEIgFACIgIABQgPAAgHgLgAgJgJQgCAFAAAEIAAAtQAAAIADAEQADAFAFAAQAFAAAEgEQADgFAAgGIAAgvQAAgFgDgEQgEgEgFAAQgGAAgDAEg");
	this.shape_28.setTransform(468.9,297.5);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AgTA9QgEgBgEgEQgEgDgDgHQgCgGAAgKQAAgKABgIQACgIAFgEQAEgFAHgDQAIgCAKAAIAGAAIAGAAIAAgNQAAgGgDgEQgDgEgGAAQgEAAgEADQgEADgCAGIgXAAQAAgIADgGQADgHAFgFQAFgFAGgDQAHgCAIAAQAHAAAHACQAHACAFAFQAEAFAEAHQACAHABAKIAABUIgZAAIAAgNIgBAAQgFAHgFAEQgDADgJAAgAgHALQgFAEAAAKQAAAHAEAEQACAFAGAAQAGAAAEgFQADgEAAgHIAAgRIgHAAQgHAAgGADg");
	this.shape_29.setTransform(459.8,299.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#0084C7").s().p("AoCCvIAAldIQFAAIi5CoIC5C1g");
	this.shape_30.setTransform(494.5,299.5);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f().s("#000000").ss(4,1).p("AAAl7IAAL3");
	this.shape_31.setTransform(451,317);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AgTA5QgHgEgFgIQgDgFgBgGIgBgOIAAgnIABgOQABgGADgFQAFgIAHgFQAJgEAKAAQAIAAAHADQAHACAEAGQAFAFADAHQACAHAAAIIAAAgIgvAAIAAARQAAAFADAEQADADAFAAQAHAAACgEQACgEABgFIAYAAQAAAIgCAHQgDAHgFAEQgEAFgHADQgHADgIAAQgKAAgJgFgAgIghQgDADAAAHIAAAOIAXAAIAAgOQAAgHgEgDQgDgEgFAAQgFAAgDAEg");
	this.shape_32.setTransform(387.7,367.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FFFFFF").s().p("AADBVQgGgBgEgEQgFgEgDgHQgCgGAAgLIAAiJIAXAAIAACHQAAAHADADQACACAHABIAAAXQgIAAgHgBg");
	this.shape_33.setTransform(381.5,365.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FFFFFF").s().p("AgCBTIgJgJIAAALIgZAAIAAiqIAZAAIAAA8IAAAAQAEgGAGgDQAFgDAHAAQAJAAAFADQAFADADAFQACADABAHIABASIAAAvIgBAQQgBAGgDAEQgDAFgFADQgFADgIAAQgJAAgEgDgAgIgJQgDAEAAAFIAAAvQAAAGADAFQAEAEAEAAQAGAAADgFQADgEAAgIIAAgtQAAgEgCgFQgDgEgHAAQgEAAgEAEg");
	this.shape_34.setTransform(374.2,365.5);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FFFFFF").s().p("AgTA9QgFgBgDgEQgEgDgDgHQgDgGABgKQgBgKACgIQACgIAFgEQAEgFAHgDQAHgCALAAIAGAAIAGAAIAAgNQAAgGgDgEQgCgEgIAAQgDAAgEADQgEADgBAGIgYAAQAAgIADgGQADgHAFgFQAFgFAGgDQAHgCAHAAQAJAAAGACQAHACAEAFQAFAFAEAHQACAHAAAKIAABUIgYAAIAAgNIgBAAQgEAHgGAEQgDADgJAAgAgHALQgFAEAAAKQAAAHADAEQAEAFAFAAQAGAAADgFQAEgEAAgHIAAgRIgHAAQgHAAgGADg");
	this.shape_35.setTransform(365,367.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FFFFFF").s().p("AAQBPQgJAAgFgCQgFgDgDgEQgEgEgBgFIgCgKIAAhJIgMAAIAAgTIAMAAIAAglIAYAAIAAAlIAPAAIAAATIgPAAIAABCIABAHQAAAAAAABQAAABABAAQAAABAAAAQABAAAAABIAFABIAHAAIAAAXg");
	this.shape_36.setTransform(358.2,366);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FFFFFF").s().p("AgNA8QgGgDgFgEQgFgFgDgHQgDgGgBgKIAYAAQAAAGADAEQADAEAGAAQAFAAAEgDQAEgEAAgGQAAgFgCgEQgDgDgHgDIgLgEQgLgFgHgHQgDgEgCgGQgBgFAAgGQAAgIADgGQADgHAEgEQAFgFAHgDQAGgCAHAAQAHAAAGACQAHADAEAFQAFAEADAHQACAHAAAHIgXAAQAAgGgEgDQgDgDgFAAQgFAAgDADQgDAEAAAFIABAHQACADAHACIANAGQAHADAFADQAFADACAFQAGAJAAAKQAAAIgDAHQgDAGgFAFQgFAFgGADQgHADgIAAQgGAAgHgCg");
	this.shape_37.setTransform(351.6,367.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#0084C7").s().p("AoCCvIAAldIQFAAIi5CoIC5C1g");
	this.shape_38.setTransform(386.5,367.5);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f().s("#000000").ss(4,1).p("AAAl7IAAL3");
	this.shape_39.setTransform(343,385);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FFFFFF").s().p("AgSA5QgJgEgEgIQgDgFgBgGIgBgOIAAgnIABgOQABgGADgFQAEgIAJgFQAIgEAKAAQAIAAAHADQAHACAEAGQAFAFADAHQADAHgBAIIAAAgIgvAAIAAARQAAAFADAEQADADAFAAQAHAAACgEQADgEAAgFIAYAAQABAIgDAHQgDAHgFAEQgEAFgHADQgHADgIAAQgKAAgIgFgAgIghQgDADAAAHIAAAOIAXAAIAAgOQAAgHgEgDQgDgEgFAAQgFAAgDAEg");
	this.shape_40.setTransform(228.8,361.9);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FFFFFF").s().p("AghA9IAAgUIAnhPIgkAAIAAgWIBAAAIAAAUIgnBOIAnAAIAAAXg");
	this.shape_41.setTransform(220.7,361.9);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FFFFFF").s().p("AgLBVIAAh4IAXAAIAAB4gAgLg8IAAgZIAXAAIAAAZg");
	this.shape_42.setTransform(214.7,359.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FFFFFF").s().p("AAQBPQgJAAgFgCQgFgDgDgEQgEgEgBgFIgCgKIAAhJIgMAAIAAgTIAMAAIAAglIAYAAIAAAlIAPAAIAAATIgPAAIAABCIABAHQAAAAAAABQAAABABAAQAAABAAAAQABAAAAABIAFABIAHAAIAAAXg");
	this.shape_43.setTransform(210,360);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FFFFFF").s().p("AgTA5QgHgEgFgIQgDgFgBgGIgBgOIAAgnIABgOQABgGADgFQAFgIAHgFQAJgEAKAAQAIAAAHADQAHACAEAGQAFAFADAHQACAHAAAIIAAAgIgvAAIAAARQAAAFADAEQADADAFAAQAHAAACgEQACgEABgFIAYAAQAAAIgCAHQgDAHgFAEQgEAFgHADQgHADgIAAQgKAAgJgFgAgIghQgDADAAAHIAAAOIAXAAIAAgOQAAgHgEgDQgDgEgFAAQgFAAgDAEg");
	this.shape_44.setTransform(203.2,361.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FFFFFF").s().p("AAMA9IAAhTQAAgHgDgDQgDgEgGAAQgGAAgDAEQgCAFAAAHIAABRIgZAAIAAh4IAZAAIAAALIAAAAQAEgFAGgEQAEgEAIABIAJABQAFABAEAEQADADACAGQADAGAAAIIAABcg");
	this.shape_45.setTransform(194.1,361.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FFFFFF").s().p("AgTA5QgHgEgFgIQgDgFAAgGIgBgOIAAgnIABgOQAAgGADgFQAFgIAHgFQAIgEALAAQALAAAJAEQAHAFAFAIQACAFACAGIABAOIAAAnIgBAOQgCAGgCAFQgFAIgHAEQgJAFgLAAQgLAAgIgFgAgIghQgDADAAAHIAAAvQAAAHADADQADAEAFAAQAGAAADgEQADgDAAgHIAAgvQAAgHgDgDQgDgEgGAAQgFAAgDAEg");
	this.shape_46.setTransform(185.1,361.9);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FFFFFF").s().p("AAkA9IAAhTQAAgHgDgDQgDgEgGAAQgGAAgDAEQgCAFAAAHIAABRIgYAAIAAhTQAAgHgDgDQgDgEgGAAQgHAAgDAEQgCAFgBAHIAABRIgYAAIAAh4IAYAAIAAALIABAAQAEgFAFgEQAGgEAIABQAIgBAEAEIAJAJQAEgFAGgEQAFgEAKABIAJABQAFABAEAEQAEADADAGQACAGAAAIIAABcg");
	this.shape_47.setTransform(173.5,361.8);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#0084C7").s().p("AoCCvIAAldIQFAAIi5CoIC5C1g");
	this.shape_48.setTransform(205.5,361.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f().s("#000000").ss(4,1).p("AAAl7IAAL3");
	this.shape_49.setTransform(162,379);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FFFFFF").s().p("AgkBWIAAipIAZAAIAAALIAJgJQAEgEAJAAQAIAAAFADQAFADADAGQADADABAHIABAQIAAAuIgBATQgBAFgCAEQgDAFgFADQgFADgJAAQgHABgFgEQgGgDgEgGIAAAAIAAA8gAgIg4QgDAEAAAHIAAAuQAAAFADAEQAEAEAEAAQAHAAADgFQACgEAAgFIAAgsQAAgHgDgFQgDgEgGgBQgEAAgEAFg");
	this.shape_50.setTransform(69.3,448.3);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FFFFFF").s().p("AgUA8QgEgBgEgEQgDgEgDgFQgCgGAAgIIAAhcIAZAAIAABSQAAAHADAEQADAEAFAAQAHAAACgFQADgDAAgJIAAhQIAZAAIAAB4IgZAAIAAgLIAAAAQgEAFgGAEQgEAEgIgBQgFAAgFgBg");
	this.shape_51.setTransform(60,446);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FFFFFF").s().p("AgjAMIAAgXIBHAAIAAAXg");
	this.shape_52.setTransform(50.7,445.3);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FFFFFF").s().p("AAQBPQgJAAgFgCQgFgDgDgEQgEgEgBgFIgCgKIAAhJIgMAAIAAgTIAMAAIAAglIAYAAIAAAlIAPAAIAAATIgPAAIAABCIABAHQAAAAAAABQAAAAABABQAAAAAAABQABAAAAABIAFABIAHAAIAAAXg");
	this.shape_53.setTransform(43.5,444);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FFFFFF").s().p("AgcA+IAAh5IAZAAIAAANQAGgHAIgEQAHgDALgBIAAAbQgEgCgEAAIgJABIgIAEQgDAEgCAFQgCAFAAAIIAABHg");
	this.shape_54.setTransform(38.4,445.8);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FFFFFF").s().p("AgTA9QgEgBgEgEQgEgDgDgHQgCgGAAgKQAAgKABgIQACgIAFgEQAEgFAHgDQAIgCAKAAIAGAAIAGAAIAAgNQAAgGgDgEQgDgEgGAAQgEAAgEADQgEADgCAGIgXAAQAAgIADgGQADgHAFgFQAFgFAGgDQAHgCAIAAQAHAAAHACQAHACAFAFQAEAFAEAHQADAHAAAKIAABUIgZAAIAAgNIgBAAQgFAHgFAEQgDADgJAAgAgHALQgFAEAAAKQAAAHAEAEQACAFAGAAQAGAAAEgFQADgEAAgHIAAgRIgHAAQgHAAgGADg");
	this.shape_55.setTransform(30,445.9);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FFFFFF").s().p("AAQBPQgJAAgFgCQgFgDgDgEQgEgEgBgFIgCgKIAAhJIgMAAIAAgTIAMAAIAAglIAYAAIAAAlIAPAAIAAATIgPAAIAABCIABAHQAAAAAAABQAAAAABABQAAAAAAABQABAAAAABIAFABIAHAAIAAAXg");
	this.shape_56.setTransform(23.2,444);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FFFFFF").s().p("AgNA8QgGgDgFgEQgFgFgDgHQgDgGgBgKIAYAAQAAAGADAEQADAEAGAAQAFAAAEgDQAEgEAAgGQAAgFgCgEQgDgDgHgDIgLgEQgLgFgHgHQgDgEgCgGQgBgFAAgGQAAgIADgGQADgHAEgEQAFgFAHgDQAGgCAHAAQAHAAAGACQAHADAEAFQAFAEADAHQACAHAAAHIgXAAQAAgGgEgDQgDgDgFAAQgFAAgDADQgDAEAAAFIABAHQACADAHACIANAGQAHADAFADQAFADACAFQAGAJAAAKQAAAIgDAHQgDAGgFAFQgFAFgGADQgHADgIAAQgGAAgHgCg");
	this.shape_57.setTransform(16.6,445.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#0084C7").s().p("AoCCvIAAldIQFAAIi5CoIC5C1g");
	this.shape_58.setTransform(51.5,445.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f().s("#000000").ss(4,1).p("AAAl7IAAL3");
	this.shape_59.setTransform(8,463);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#0084C7").s().p("Ay8QgQgEAAgDgDQgDgDABgEQAAgEADgDIAPgNQADgDAFAAQAEABACADQADADAAAEQgBAEgDADIgPANQgDADgDAAIgBgBgAxwPgQgEgBgDgDQgDgDABgEQAAgEADgDIAPgNQADgDAEAAQAFABACADQADADAAAEQgBAEgDADIgPANQgDADgDAAIgBAAgAwkOfQgEgBgDgDQgDgDABgEQAAgEADgDIAPgNQADgDAFAAQAEABACADQADADAAAEQgBAEgDADIgPANQgDADgDAAIgBAAgAvYNdQgEAAgDgDQgDgDABgEQAAgEADgDIAPgNQADgDAEABQAFAAACADQADADAAAEQgBAEgDADIgPANQgDADgDAAIgBgBgAuMMcQgEAAgDgDQgDgDABgEQAAgEADgDIAPgNQADgDAEABQAFAAACADQADADAAAEQgBAEgDADIgPANQgDADgDAAIgBgBgAtALcQgEgBgDgDQgDgDABgEQAAgEADgDIAPgNQADgDAFAAQAEABACADQADADAAAEQgBAEgDADIgPANQgDADgDAAIgBAAgAr1KbQgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgAqoJaQgFgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAAAgApeIYQgEAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBgBgAoSHXQgFAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAgBgAnHGXQgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgAl8FWQgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgAkwEUQgFAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAgBgAjmDTQgEAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBgBgAibCTQgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgAhPBSQgFgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAAAgAgFARQgEgBgCgDQgDgDAAgEQABgEADgCIAOgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDACIgPANQgCADgEAAIgBAAgABFgwQgEAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBgBgACRhxQgFAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAgBgADcixQgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgAEnjyQgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgAFzk0QgFAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAgBgAG9l1QgEAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBgBgAIIm1QgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgAJUn2QgFgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAAAgAKeo3QgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgALpp5QgEAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBgBgAM1q6QgFAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAgBgAOAr6QgEgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBAAgAPMs7QgFgBgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAAAgAQXt9QgFAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAgBgARhu+QgEAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEABQAEAAADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIgBgBgAStv/QgFAAgCgDQgDgDAAgEQABgEADgDIAPgNQADgDAEAAQAEABADADQADADgBAEQAAAEgDADIgPANQgDADgEAAIAAgBg");
	this.shape_60.setTransform(876.5,194.9);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#0084C7").s().p("Ag1A2QgWgWAAggQAAgeAWgXQAXgWAeAAQAfAAAXAWQAWAXAAAeQAAAggWAWQgXAWgfAAQgeAAgXgWg");
	this.shape_61.setTransform(580.8,401.2);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#0084C7").s().p("Ag0A2QgXgXAAgfQAAgfAXgVQAWgXAeAAQAfAAAXAXQAWAVAAAfQAAAfgWAXQgXAWgfAAQgeAAgWgWg");
	this.shape_62.setTransform(450.8,362.2);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#0084C7").s().p("Ag0A2QgXgXAAgfQAAgfAXgWQAWgWAeAAQAfAAAXAWQAWAWAAAfQAAAfgWAXQgXAWgfAAQgeAAgWgWg");
	this.shape_63.setTransform(342.8,431.2);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#0084C7").s().p("Ag0A2QgXgXAAgfQAAgeAXgXQAWgWAeAAQAfAAAXAWQAWAXAAAeQAAAfgWAXQgXAWgfAAQgeAAgWgWg");
	this.shape_64.setTransform(161.8,423.2);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#0084C7").s().p("Ag1A2QgWgWAAggQAAgeAWgWQAXgXAeAAQAfAAAXAXQAWAWAAAeQAAAggWAWQgXAWgfAAQgeAAgXgWg");
	this.shape_65.setTransform(7.8,508.1);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f().s("#0084C7").p("Eg5jAQRIYCtSIcUBVIQuqzIUYGIIZrv5");
	this.shape_66.setTransform(376.6,403.6);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#0084C7").s().p("A0XCzQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAzbCrQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAyfCjQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAxjCbQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAwnCTQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAvrCLQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAuvCDQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAtzB7QgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAs3BzQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAr7BrQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAq/BjQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAqDBbQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgApHBTQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAoLBLQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAnPBDQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAmTA7QgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAlXAzQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAkbArQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAjgAjQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAijAcQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgCQAGgBAFADQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAhoAUQgEgEgBgGQgBgHAEgDQAEgFAGgBIAUgCQAHAAAEAEQAFADABAGQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAgrANQgFgEgBgGQgBgFAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEAAAGQABAFgDAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAAPAFQgEgEgBgFQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAEQgDAEgHABIgUACIgBAAQgFAAgFgDgABMgBQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUACIgCAAQgFAAgEgCgACIgJQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgADDgRQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAEAgYQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAE7ggQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAF4gnQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAGzgvQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAHwg2QgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAIsg+QgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAJnhGQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAKkhNQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgALfhVQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAMchcQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgANXhkQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAOUhrQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAPPhzQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgAQMh6QgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgARIiCQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgASDiKQgEgEgBgGQgBgHAEgEQAEgFAGgBIAUgCQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgUACIgBAAQgFAAgFgDgATAiRQgFgEgBgGQgBgGAEgFQAEgFAGgBIAUgDQAGgBAFAEQAFAEABAGQABAGgEAFQgEAFgGABIgUADIgCAAQgFAAgEgDgAT7iZQgEgEgBgGQgBgHAEgEQAEgFAGgBIAKgBQAHAAAEAEQAFADABAHQAAAGgEAFQgDAEgHABIgKABIgBAAQgFAAgFgDg");
	this.shape_67.setTransform(874.5,282.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.シンボル3, new cjs.Rectangle(0,-2,1105,517.7), null);


(lib.シンボル2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(5,0,1).p("AwUlDIMJAUIDmAFIR1AcIgPJ1IxmgcIjmgFIsYgUIkBlAgAQoqJIxNAuIjmAKIswAiIgap2INKgjIDmgKIQzgsIEWEvgAQcT/IxBAAIjmAAIs+AAIAAp2IM+AAIDmAAIRBAAIEJE7g");
	this.shape.setTransform(131.7,142.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#000000").ss(3).p("Ahy8QIAAjEIDlAAIAAC6ABzykIAAEyAhyt4IAAkiABzj8IAAE9AhyBBIAAlCABzK3IAAUeIjlAAIAA0e");
	this.shape_1.setTransform(116.5,200.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("Ai1R8IAAlWIAxAAIAAAXIATgSQAKgIASAAQAPAAALAGQAKAGAHAMQAEAHACANIAACjQgBAMgFAIQgGAKgKAGQgKAGgRABQgPgBgLgGQgLgHgJgLIgBAAIAAB4gAh9NcQgHAJAAAOIAABeQAAALAHAIQAHAIALgBQAOABAFgJQAFgJAAgMIAAhZQAAgPgGgIQgGgKgMAAQgLAAgHAIgAF5QFQgEgIgCgMIgBijQACgNAFgHQAGgKAKgGQAJgHASAAQAPAAALAHQALAGAIANIABAAIAAh5IAxAAIAAFVIgxAAIAAgVIgJAJIgJAJIgMAFIgQACQgeAAgNgYgAGnNdQgFAJAAALIAABaQAAAOAGAJQAFAJANAAQALABAHgJQAGgJAAgNIAAhfQAAgLgHgIQgHgHgKAAQgOAAgFAJgAAgQZQgJgCgIgHQgIgHgFgNQgEgMAAgUQAAgVACgPQAEgQAIgKQAJgLAPgFQAPgFAXAAIAXACIAAgaQAAgNgFgIQgFgIgOAAQgJAAgIAGQgIAHgDAMIguAAQABgRAEgNQAGgNAKgKQAJgKAOgFQANgGARAAQAPAAAOAFQANAFAKAJQAKAKAGAPQAFAOAAATIAACpIgxAAIAAgYIAAAAQgKANgKAIQgKAHgSAAgAA5O3QgLAIAAATQAAAOAGAJQAHAIAMAAQANAAAHgIQAGgIAAgPIAAghIgNgBQgQgBgLAIgAEYQZIAAioQAAgOgGgHQgHgIgLAAQgOAAgFAJQgGAJAAAQIAACjIgxAAIAAjzIAxAAIAAAXIABAAQAIgLALgHQALgIARAAQAJAAAJADQAJAEAHAHQAHAHAEAMQAFAKAAARIAAC6gAj3QZIghhKIghBKIg0AAIA8h7Ig5h4IA0AAIAeBFIAehFIA0AAIg6B4IA9B7gAoCQZIAAlVICSAAIAAAuIhhAAIAABkIBUAAIAAAuIhUAAIAABlIBhAAIAAAwgAjSBhQgNgGgKgKQgJgLgGgOQgFgPAAgSQAAgbANgVQANgWAUgRQgMgTgJgTQgIgUAAgVQAAgOAEgMQAEgMAJgJQAIgJAMgFQANgGARAAQAPABAMAEQAMAFAJAJQAIAJAEALQAEALAAAMQAAAMgDAMQgEALgIALQgOAWgUATIAtBGIAagjIAeAVIgkAuIAlA7IgxAAIgRgbQgOAPgQAHQgPAIgUgBQgOAAgNgFgAjLgHQgHAMAAAQQAAARAKANQAKANAQAAQALAAAKgHQAKgHAGgGIgvhLQgLAMgIAMgAi6iiQgHAJAAALQAAALAFANIAOAZIATgWIAHgPQADgHAAgKQAAgJgGgHQgHgIgLAAQgLAAgGAJgABoBkIgOhKIhDAAIgOBKIgwAAIBLlVIApAAIBMFVgAAggTIAxAAIgYh8IgBAAgAlWBkIAAjPIgBAAIgxCRIgYAAIgwiRIgBAAIAADPIgxAAIAAlVIAvAAIA/C0IABAAIA+i0IAwAAIAAFVgAisslQgPgFgLgKQgMgLgHgPQgIgQAAgVIAAi0QAAgVAIgQQAHgQAMgKQALgKAPgGQAPgFAOAAQAPAAAPAFQAPAGALAKQAMAKAHAQQAHAQAAAVIAAC0QAAAVgHAQQgHAPgMALQgLAKgPAFQgPAFgPABQgOgBgPgFgAimxCQgKAIAAATIAAC0QAAARAKAJQAKAJANgBQAOABAKgJQAKgJAAgRIAAi0QAAgTgKgIQgKgJgOABQgNgBgKAJgAmhsiIAAlWIBKAAQAUAAAPAFQAQAFAMANQANAOAEASQAFASAAAfQAAAWgCAQQgDARgJAOQgKARgRAJQgRAJgbAAIgZAAIAACGgAlwvWIAYAAQAPAAAIgEQAIgFAEgHQAEgHAAgLIAAguQAAgLgEgIQgDgIgIgFQgIgEgOAAIgaAAgAoBsiIAAlWIAxAAIAAFWg");
	this.shape_2.setTransform(162.3,149.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#D2F2FF").s().p("AjWDXQhahYAAh/QAAh+BahZQBZhZB9AAQB+AABZBZQBaBZAAB+QAAB/haBYQhZBah+AAQh9AAhZhagAhohoQgsArAAA9QAAA+AsArQArAsA9ABQA+gBAsgsQAsgrAAg+QAAg9gsgrQgsgtg+ABQg9gBgrAtg");
	this.shape_3.setTransform(61.7,238.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00B7FF").s().p("AjWDYQhahaAAh+QAAh+BahYQBZhaB9AAQB+AABZBaQBaBYAAB+QAAB+haBaQhZBZh+AAQh9AAhZhZgAhohoQgsArAAA9QAAA+AsArQArAtA9gBQA+ABAsgtQAsgrAAg+QAAg9gsgrQgsgsg+gBQg9ABgrAsg");
	this.shape_4.setTransform(61.7,142.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#0084C7").s().p("AjWDYQhahaAAh+QAAh+BahYQBZhaB9AAQB+AABZBaQBaBYAAB+QAAB+haBaQhZBZh+AAQh9AAhZhZgAhohpQgsAsAAA9QAAA+AsArQArAtA9gBQA+ABAsgtQAsgrAAg+QAAg9gsgsQgsgsg+AAQg9AAgrAsg");
	this.shape_5.setTransform(61.7,52.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AkLfVIAA0eIDmAAIAAUegAglK3IjmAAIs+AAIAAp2IM+AAIDmAAIjmAAIAAlCIsYgUIkBlBIEQk1IMJATIDmAGIjmgGIAAkiIswAiIgap1INKgjIDmgKIjmAKIAAjEIDmAAIAAC6IQzgtIEWEwIj8FFIxNAuIjmAKIDmgKIAAEyIR1AcIgPJ1IxmgbIjmgFIDmAFIAAE9IRBAAIEJE7IkJE7gAuTCiQhaBZAAB/QAAB/BaBYQBZBaB+AAQB/AABZhaQBZhYAAh/QAAh/hZhZQhZhZh/AAQh+AAhZBZgAB8J8IAxAAIAAh5IABAAQAIAMALAHQAMAGAOAAQASAAAJgGQAKgHAHgJQAEgIACgMIgBikQgCgMgEgIQgGgLgLgHQgKgFgPAAQgTgBgJAIIgTASIAAgWIgxAAgAK6EpQgKAGgGAKQgFAHgBANIAACjQACANAFAIQAMAXAeAAIAQgCIAMgFIAKgIIAJgKIAAAWIAxAAIAAlVIgxAAIAAB4IgBAAQgIgMgLgHQgMgHgOABQgSgBgKAHgAFWGNQgPAGgJAKQgJAKgDAQQgEAPAAAVQAAAUAFANQAFAMAIAIQAIAGAJADIASADQASAAAKgHQAJgIAKgNIABAAIAAAZIAxAAIAAiqQAAgSgGgPQgGgOgJgKQgKgJgOgGQgNgEgPAAQgRAAgOAFQgNAGgKAKQgJAJgGANQgGAOgBARIAwAAQACgMAIgHQAIgGAKgBQANABAGAIQAFAIAAAMIAAAaIgXgBQgXAAgPAEgAJDFcQAGAHAAANIAACpIAxAAIAAi7QAAgQgEgLQgEgLgIgIQgHgHgJgDQgJgDgJAAQgRgBgLAIQgKAHgIAMIgBAAIAAgXIgxAAIAADzIAxAAIAAijQAAgRAFgJQAFgJAOAAQAMAAAGAJgABtIZIg9h8IA6h3IgzAAIgfBFIgdhFIg0AAIA5B3Ig8B8IA0AAIAghKIAhBKIA0AAgAjRIZICSAAIAAgxIhhAAIAAhlIBVAAIAAguIhVAAIAAhkIBhAAIAAgtIiSAAgAuTscQhaBYAAB/QAAB+BaBaQBZBZB+AAQB/AABZhZQBZhaAAh+QAAh/hZhYQhZhah/AAQh+AAhZBagABqrMQgNAEgIAJQgIAKgEALQgFAMAAAOQAAAWAJAUQAIASANATQgUASgOAVQgNAVAAAdQAAARAFAPQAGAOAKALQAJALANAFQANAGAPAAQATAAAQgHQAQgHAOgPIAQAaIAxAAIglg6IAlgvIgfgWIgaAkIgthHQAUgSAPgWQAHgLAEgMQAEgLAAgMQAAgMgFgMQgEgLgIgIQgIgJgMgFQgMgFgQAAQgRAAgMAGgAHLmcIhNlWIgoAAIhNFWIAxAAIAOhKIBDAAIAPBKIAxAAgAglpsIAADQIAwAAIAAlWIguAAIg/C1IgBAAIg/i1IgvAAIAAFWIAxAAIAAjQIABAAIAwCTIAZAAIAwiTgAuT6gQhaBYAAB/QAAB+BaBaQBZBZB+AAQB/AABZhZQBZhaAAh+QAAh/hZhYQhZhah/AAQh+AAhZBagACE53QgOAGgMAKQgMALgHAPQgHAQAAAVIAAC0QAAAWAHAPQAHAQAMAKQAMAKAOAGQAPAFAPAAQAPAAAOgFQAPgGAMgKQAMgKAHgQQAHgPAAgWIAAi0QAAgVgHgQQgHgPgMgLQgMgKgPgGQgOgEgPAAQgPAAgPAEgAhv0jIAxAAIAAiGIAZAAQAbABAQgKQARgJAKgRQAIgOADgQQADgQAAgWQAAgfgFgTQgFgRgMgOQgNgNgOgFQgQgGgUAAIhJAAgAjP0jIAxAAIAAlWIgxAAgAslHjQgsgrAAg+QAAg+AsgrQArgtA+ABQA+gBAsAtQAsArAAA+QAAA+gsArQgsAsg+ABQg+gBgrgsgAFmHoQgHgJAAgNQAAgUALgIQAMgHAQAAIANABIAAAiQAAAOgHAJQgHAHgMAAQgMABgHgJgALZHiQgGgJAAgPIAAhaQAAgLAFgJQAFgJAOAAQALABAHAHQAHAIAAALIAABeQAAAOgHAJQgHAIgLAAQgMAAgGgJgAC0HjQgHgHAAgLIAAhfQAAgNAHgJQAGgIALgBQANABAGAJQAFAJAAAPIAABZQAAALgFAKQgFAIgOAAQgKAAgHgIgAslncQgsgrAAg+QAAg+AsgrQArgsA+gBQA+ABAsAsQAsArAAA+QAAA+gsArQgsAtg+gBQg+ABgrgtgABpnNQgLgMAAgRQAAgRAIgNQAHgMALgLIAvBLQgFAGgKAIQgKAGgLAAQgQAAgKgNgAFRoUIAYh8IABAAIAYB8gAB1p3QgFgNAAgLQAAgLAGgJQAHgIALgBQALAAAGAIQAHAHAAAKQAAAJgDAIIgHAOIgUAXgAsl1gQgsgrAAg+QAAg+AsgsQArgsA+AAQA+AAAsAsQAsAsAAA+QAAA+gsArQgsAtg+gBQg+ABgrgtgACL1ZQgKgJAAgSIAAi0QAAgSAKgJQAKgIANAAQAOAAAJAIQAKAJAAASIAAC0QAAASgKAJQgJAIgOAAQgNAAgKgIgAg+3XIAAh0IAZAAQAPAAAHAFQAIAFAEAHQADAJAAAKIAAAuQAAALgEAHQgDAIgJAFQgIADgPAAg");
	this.shape_6.setTransform(131.7,200.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.シンボル2, new cjs.Rectangle(-2.5,-1.5,268.5,404), null);


(lib.シンボル1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// レイヤー 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#231815").ss(5,1).p("EBgQAJ9IzBipIy5vTIr8hmIqGn8IksEzIpurBIkLGMImEhVItgOsIoQjNIpQKsIx+iCIz5CtIwpl4IuwF7I5VT/MDj2AAAg");
	this.shape.setTransform(729.1,153);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AmAGbIMBtJIieNeg");
	this.shape_1.setTransform(1306.8,263);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#231815").ss(5).p("A4sJvIO3suIKXmkIDjClIHaBLINLPi");
	this.shape_2.setTransform(542.9,243.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("ApwBsIInxYIB+BnII8VHIssIrg");
	this.shape_3.setTransform(1108.8,203.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AqTg0IOjl1IGENTg");
	this.shape_4.setTransform(229.4,184.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AjOg3IEslZIBxMhg");
	this.shape_5.setTransform(952.1,79.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#0084C7").s().p("AoeiwILRn9IBHG6IElESIrkKPg");
	this.shape_6.setTransform(749.1,237.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#00B7FF").s().p("ArWCmINflLIJOFLg");
	this.shape_7.setTransform(457.6,289.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AoPBGIN5q9ICmTvg");
	this.shape_8.setTransform(127.2,242.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#006CC4").s().p("Av/GJISAsRIN/MRg");
	this.shape_9.setTransform(282.5,266.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#0084C7").s().p("AAeg1IqwpCIUlHeIx/MRg");
	this.shape_10.setTransform(229.4,242.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#0084C7").s().p("ACTCbIjii+IoviyIR9BtICBE+g");
	this.shape_11.setTransform(554.3,183.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#0084C7").s().p("AkGowIINDMIjnOVg");
	this.shape_12.setTransform(691.6,163.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#00B7FF").s().p("AkEgiIIJkOIjZJhg");
	this.shape_13.setTransform(999,123.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#0084C7").s().p("ACXF+Ivar7IaHL7g");
	this.shape_14.setTransform(1006.3,265.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#0084C7").s().p("AotrAIRbONIoIH0g");
	this.shape_15.setTransform(1167.5,178.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#00B7FF").s().p("Al2DFIELyIIDaD2IirMQIGzOAg");
	this.shape_16.setTransform(1052.5,207.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#00B7FF").s().p("AzlitMAnLgGiIqmFxIu2Mug");
	this.shape_17.setTransform(420.8,244.9);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#00B7FF").s().p("AjZG5IA+nJIF1pHIjcSvg");
	this.shape_18.setTransform(842.8,246.1);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AhXFfIhanLIBhkzIECM/g");
	this.shape_19.setTransform(623.2,250.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("Al1EYILstnInCMFID/Gag");
	this.shape_20.setTransform(508.8,244.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAYBYImUCWIHEnwIhEDxIF5EUg");
	this.shape_21.setTransform(804.4,280.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#00B7FF").s().p("AmsD7IGrumIBwBoIjgLSIIeIdg");
	this.shape_22.setTransform(821.7,117.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#0084C7").s().p("AghpoIDiClImCQsg");
	this.shape_23.setTransform(549.7,244.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#006CC4").s().p("Ak9mQIJ7ITIoJEOg");
	this.shape_24.setTransform(993.3,79.9);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#006CC4").s().p("AtHFNILuqYIBZHLINIDNg");
	this.shape_25.setTransform(614.3,272.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#006CC4").s().p("A8VKPIXI0dIHCIMIahMRg");
	this.shape_26.setTransform(910.8,240.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#0084C7").s().p("AoSmpIQlF5IqiHag");
	this.shape_27.setTransform(309.7,184.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#0084C7").s().p("ApOJSIhO6uIU5XbIsoLeg");
	this.shape_28.setTransform(936.4,115.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#006CC4").s().p("AnFG/Ij4jhIuZhdIF4kAIbJk/IRsN9g");
	this.shape_29.setTransform(1296,261.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#006CC4").s().p("AjOiBIGcA1IiCDOg");
	this.shape_30.setTransform(825.2,46.7);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FFFFFF").s().p("AmRANINhuuICqCbIoiSrIrRH9g");
	this.shape_31.setTransform(758.2,126.8);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFFFFF").s().p("AlOGGIDgrTIFtpVIBQbXIh/Bug");
	this.shape_32.setTransform(843.9,93.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#D2F2FF").s().p("EhP6ADxIOwl4IQoF4IT6itIR9CCIJQqsIIQDNINgusIGFBVIELmMIJuLBIErkzIKGH8IL9BmIS3PSITPCoIAAOKMjH/AAAIqOAFg");
	this.shape_33.setTransform(672.7,153.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.シンボル1, new cjs.Rectangle(-7.1,-3,1472.6,312.2), null);


// stage content:
(lib.mountain = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// mountain.svg
	this.instance = new lib.シンボル1();
	this.instance.parent = this;
	this.instance.setTransform(727.6,683.9,1,1,0,0,0,728.8,153.2);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:729.1,regY:153,x:728.1,y:664,alpha:0.043},0).wait(1).to({x:728.2,y:646.1,alpha:0.087},0).wait(1).to({x:728.3,y:629.8,alpha:0.13},0).wait(1).to({x:728.4,y:615,alpha:0.174},0).wait(1).to({x:728.5,y:601.8,alpha:0.217},0).wait(1).to({y:589.9,alpha:0.261},0).wait(1).to({x:728.6,y:579.3,alpha:0.304},0).wait(1).to({x:728.7,y:570,alpha:0.348},0).wait(1).to({y:561.9,alpha:0.391},0).wait(1).to({x:728.8,y:554.8,alpha:0.435},0).wait(1).to({y:548.7,alpha:0.478},0).wait(1).to({y:543.6,alpha:0.522},0).wait(1).to({y:539.3,alpha:0.565},0).wait(1).to({x:728.9,y:535.8,alpha:0.609},0).wait(1).to({y:533,alpha:0.652},0).wait(1).to({y:530.8,alpha:0.696},0).wait(1).to({y:529.2,alpha:0.739},0).wait(1).to({x:729,y:528,alpha:0.783},0).wait(1).to({y:527.2,alpha:0.826},0).wait(1).to({x:728.9,y:526.8,alpha:0.87},0).wait(1).to({y:526.5,alpha:0.913},0).wait(1).to({y:526.4,alpha:0.957},0).wait(1).to({alpha:1},0).wait(1));

	// board
	this.instance_1 = new lib.シンボル2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(883.1,636.7,1,1,0,0,0,131.7,200.4);
	this.instance_1.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regY:200.5,x:883.4,y:481.3},0).wait(1).to({x:883.6,y:405.9},0).wait(1).to({x:883.7,y:383},0).wait(1).to({y:362.3},0).wait(1).to({x:883.8,y:343.6,alpha:0.053},0).wait(1).to({y:326.9,alpha:0.105},0).wait(1).to({x:883.9,y:312.1,alpha:0.158},0).wait(1).to({y:299,alpha:0.211},0).wait(1).to({y:287.6,alpha:0.263},0).wait(1).to({y:277.6,alpha:0.316},0).wait(1).to({x:884,y:269.1,alpha:0.368},0).wait(1).to({y:261.9,alpha:0.421},0).wait(1).to({y:255.9,alpha:0.474},0).wait(1).to({y:251,alpha:0.526},0).wait(1).to({y:247,alpha:0.579},0).wait(1).to({y:244,alpha:0.632},0).wait(1).to({y:241.7,alpha:0.684},0).wait(1).to({y:240,alpha:0.737},0).wait(1).to({y:238.9,alpha:0.789},0).wait(1).to({y:238.2,alpha:0.842},0).wait(1).to({y:237.9,alpha:0.895},0).wait(1).to({y:237.8,alpha:0.947},0).wait(1).to({x:884.1,alpha:1},0).wait(1));

	// flag
	this.instance_2 = new lib.シンボル3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(680.2,568.7,1,1,0,0,0,552.2,257.7);
	this.instance_2.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regX:552.5,regY:256.9,x:680.5,y:567.9},0).wait(1).to({y:545.2},0).wait(1).to({x:680.7,y:508.9},0).wait(1).to({x:680.8,y:476.1},0).wait(1).to({x:680.9,y:446.6,alpha:0.053},0).wait(1).to({x:681,y:420.2,alpha:0.105},0).wait(1).to({y:396.7,alpha:0.158},0).wait(1).to({x:681.1,y:375.9,alpha:0.211},0).wait(1).to({x:681.2,y:357.8,alpha:0.263},0).wait(1).to({y:342.1,alpha:0.316},0).wait(1).to({x:681.3,y:328.6,alpha:0.368},0).wait(1).to({y:317.2,alpha:0.421},0).wait(1).to({x:681.4,y:307.7,alpha:0.474},0).wait(1).to({y:299.9,alpha:0.526},0).wait(1).to({y:293.6,alpha:0.579},0).wait(1).to({y:288.8,alpha:0.632},0).wait(1).to({y:285.1,alpha:0.684},0).wait(1).to({y:282.5,alpha:0.737},0).wait(1).to({y:280.7,alpha:0.789},0).wait(1).to({y:279.7,alpha:0.842},0).wait(1).to({y:279.1,alpha:0.895},0).wait(1).to({y:278.9,alpha:0.947},0).wait(1).to({x:681.5,alpha:1},0).wait(1));

	// moji
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F4F4F4").s().p("AjJWFIAAyrIos5eIGtAAIFFRjIAHAAIFGxjIGsAAIorZeIAASrg");
	this.shape.setTransform(1182.1,200);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F4F4F4").s().p("ApaWFMAAAgsJIS1AAIAAF9IshAAIAAM+IK6AAIAAF8Iq6AAIAAM9IMhAAIAAGVg");
	this.shape_1.setTransform(1037.6,200);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#F4F4F4").s().p("ApbWFMAAAgsJIGVAAMAAAAl0IMhAAIAAGVg");
	this.shape_2.setTransform(890.8,200);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#F4F4F4").s().p("AFFWFIpq6iIgHAAIAAaiImVAAMAAAgsJIGFAAIJiamIAIAAIAA6mIGVAAMAAAAsJg");
	this.shape_3.setTransform(723.3,200);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#F4F4F4").s().p("AjKWFMAAAgsJIGUAAMAAAAsJg");
	this.shape_4.setTransform(594.8,200);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#F4F4F4").s().p("AFHWFInLzmIjaGZIAANNImVAAMAAAgsJIGVAAIAAUKIAIAAIJO0KIGVAAIooRrIKPaeg");
	this.shape_5.setTransform(479.9,200);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#F4F4F4").s().p("AAMWcQh2AAhzgfQh3gfhghOQhlhUg3h5Qg+h/AAjCIAA4TQgCiEAyh+QAuh1BYhZQBchaB1guQCBg1CKADQCGgCB8A0QB0AvBbBbQBbBfAtB4QAzCDgBCNIAACeImVAAIAAiHQABg4gRg1QgRg1gigsQhDhYhzAAQiXAAgwBeQgyBcAACQIAAWjQgBB8A2BSQA1BUCLAAQAsgBApgNQAugPAlgfQApgkAVgzQAahDgDhIIAAiLIGVAAIAACvQAACCgyB4QgwB2hZBaQhZBbhzA0Qh2A1iCAAIgEAAg");
	this.shape_6.setTransform(305.8,200);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#F4F4F4").s().p("AH8WFIAA61IgIAAImRS9IjJAAImNy9IgIAAIAAa1ImVAAMAAAgsJIGFAAIIIXXIAHAAIIE3XIGJAAMAAAAsJg");
	this.shape_7.setTransform(113.4,200);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(24));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(721.7,396.4,1472.6,783.4);
// library properties:
lib.properties = {
	width: 1460,
	height: 680,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;