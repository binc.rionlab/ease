<?php
/**
 * The template for displaying Tag pages
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<section id="primary" class="site-content lower-site-content blog-site-content tag-site-content">
		<div id="content" role="main">
		
		<?php if(is_tag( array( 'class_aftereffect01', 'class_aftereffect02', 'class_aftereffect03', 'class_aftereffect04', 'class_aftereffect05', 'class_aftereffect06', 'class_aftereffect07', 'class_aftereffect08', 'class_aftereffect09', 'class_aftereffect10', 'class_aftereffect11', 'class_aftereffect12', 'class_aftereffect13', 'class_aftereffect14', 'class_aftereffect_die', 'class_aftereffect_other', 'traumatism_parts_head', 'traumatism_parts_face', 'traumatism_parts_nerve', 'traumatism_parts_upper_limb', 'traumatism_parts_lower_limb', 'traumatism_parts_neck_chest_back_waist_pelvis', 'traumatism_parts_property_damage' ) ) ) { ?>
			
			<?php get_template_part('other_articles_traffic'); ?>
			
		<?php } ?>
		
		<?php if(is_tag( array( 'type_of_accident_caught', 'type_of_accident_fall', 'type_of_accident_heart', 'type_of_accident_staff', 'type_of_accident_fire', 'type_of_accident_other', 'industry_food', 'industry_estate', 'industry_service', 'industry_maker', 'industry_other') ) ) { ?>
			
			<?php get_template_part('other_articles_industrial'); ?>
			
		<?php } ?>
		
		<?php if(is_tag( array( 'trouble_client', 'trouble_staff', 'trouble_money', 'trouble_customer') ) ) { ?>
			
			<?php get_template_part('other_articles_enterprise'); ?>
			
		<?php } ?>

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( '%s', 'twentytwelve' ), '<span>' . single_tag_title( '', false ) . '</span>' ); ?></h1>

			<?php if ( tag_description() ) : // Show an optional tag description ?>
				<div class="archive-meta"><?php echo tag_description(); ?></div>
			<?php endif; ?>
			</header><!-- .archive-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the post format-specific template for the content. If you want to
				 * this in a child theme then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

			endwhile;

			twentytwelve_content_nav( 'nav-below' );
			?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div><!-- #content -->
	</section><!-- #primary -->

	<?php get_sidebar(); ?>

<?php get_footer(); ?>