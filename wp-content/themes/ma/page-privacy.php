<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */


get_header('page'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>
<a href="javascript:void(0);" style="width: 100%;text-align: center;pointer-events: none;"></a>




			<?php while ( have_posts() ) : the_post(); ?>

					<?php the_content(); ?>
						
			<?php endwhile; // end of the loop. ?>




<?php get_footer(); ?>
