<?php
/**
 * The template for displaying Category pages
 *
 * Used to display archive-type pages for posts in a category.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header('sec'); ?>
<?php get_template_part('inc/category-nav'); ?>
  <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/materialize.min.js"></script>
  <script>
    $(document).ready(function(){$('.sidenav').sidenav();});
  </script>
  <section id="box-container">
<?php if (SwpmMemberUtils::is_member_logged_in()) : //Simple Membershipでログインしているかを判断する
	$member_level = SwpmMemberUtils::get_logged_in_members_level(); //会員レベルの判定
		if ( $member_level == 6) : ?>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make015/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/015.png">
          <img class="container--ribbon container--ribbon__small" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg">
      </div>

    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make001/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/001.png">
          <img class="container--ribbon container--ribbon__large" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg">
      </div>

    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make022/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/022.png">
          <img class="container--ribbon container--ribbon__small" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg">
      </div>

    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make048/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/048.png">
          <img class="container--ribbon container--ribbon__small" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg">
      </div>

    </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/018.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/016.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/030.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/049.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/040.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/005.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/032.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/045.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/044.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/012.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/025.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/033.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/019.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/047.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/011.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/014.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/052.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/038.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/054.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/029.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/053.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/039.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/043.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/007.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/042.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/036.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/034.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/020.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/031.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/024.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/027.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/059.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/035.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/013.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/041.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/057.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/002.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/051.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/017.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/023.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/009.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/055.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/006.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/003.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/010.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/056.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/008.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/046.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/058.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/037.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/004.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/021.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/050.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/028.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/026.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
			<?php else : ?>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make015/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/015.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make001/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/001.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make022/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/022.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make048/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/048.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make018/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/018.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make016/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/016.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make030/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/030.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make049/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/049.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make040/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/040.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make005/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/005.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make032/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/032.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make045/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/045.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make044/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/044.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make012/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/012.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make025/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/025.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make033/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/033.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make019/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/019.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make047/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/047.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make011/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/011.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make014/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/014.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make052/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/052.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make038/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/038.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make054/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/054.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make029/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/029.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make053/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/053.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make039/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/039.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make043/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/043.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make007/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/007.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make042/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/042.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make036/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/036.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make034/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/034.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make020/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/020.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make031/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/031.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make024/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/024.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make027/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/027.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make059/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/059.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make035/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/035.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make013/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/013.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make041/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/041.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make057/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/057.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make002/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/002.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make051/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/051.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make017/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/017.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make023/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/023.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make009/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/009.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make055/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/055.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make006/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/006.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make003/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/003.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make010/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/010.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make056/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/056.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make008/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/008.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make046/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/046.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make058/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/058.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make037/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/037.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make004/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/004.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make021/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/021.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make050/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/050.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make028/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/028.png"></div>
    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make026/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/026.png"></div>
    </a>
			<?php endif; ?>
<?php else : ?>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make015/">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/015.png">
          <img class="container--ribbon container--ribbon__small" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg">
      </div>

    </a>
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>02/make001/">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/001.png">
          <img class="container--ribbon container--ribbon__large" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ribbon.svg">
      </div>

    </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/022.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/048.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/018.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/016.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/030.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/049.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/040.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/005.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/032.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/045.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/044.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/012.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/025.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/033.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/019.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/047.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/011.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/014.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/052.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/038.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/054.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/029.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/053.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/039.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/043.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/007.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/042.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/036.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/034.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/020.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/031.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/024.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/027.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/059.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/035.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/013.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/041.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/057.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/002.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/051.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/017.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/023.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/009.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/055.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/006.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/003.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/010.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/056.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/008.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/046.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/058.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/037.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/004.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/021.png">
        <div class="container--box container--box__black container--box__large"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/050.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/028.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>membership-join">
      <div class="item box-size1"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/02img/make/026.png">
        <div class="container--box container--box__black container--box__small"></div>
        <div class="container--filter container--filter__black"></div>
      </div>
      </a>
<?php endif; ?>
  </section>
<?php get_footer('make'); ?>
